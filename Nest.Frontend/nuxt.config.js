export default {
  mode: "spa",
  head: {
    title: "NEST - Centro de inovação do turismo",
    meta: [
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: "Our mission is to promote innovation and technology adoption in the sector’s value chain, supporting the development of new business ideas, projects experimentation and capacitate entrepreneurs in the transition to the digital economy."
      },
      {
        hid: "og:title",
        name: "og:title",
        content: "NEST - Centro de inovação do turismo"
      },
      {
        hid: "og:image",
        name: "og:image",
        content: "https://nestportugal.pt/Services/services/media/1074/20.jpg"
      },
      {
        hid: "og:description",
        name: "og:description",
        content: "Our mission is to promote innovation and technology adoption in the sector’s value chain, supporting the development of new business ideas, projects experimentation and capacitate entrepreneurs in the transition to the digital economy."
      },
      {
        hid: "og:url",
        name: "og:url",
        content: "https://nestportugal.pt/"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      },
      {
        rel: "stylesheet",
        href: "https://use.typekit.net/wkg6fvh.css"
      }
    ],
    htmlAttrs: {
      lang: "en"
    }
    // script: [{
    //   src: '../../libs/fb-sdk.js'
    // }]
  },
  /*
   ** Customize the progress-bar color
   */
  // loading: '~/components/loading.vue',
  // loading: false,
  /*
   ** Global CSS
   */
  css: ["~/assets/scss/main.scss"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{ src: "~/plugins/aos", ssr: false }],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
  ],

  layout: "default",
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    [
      "@nuxtjs/google-analytics",
      {
        id: "UA-152976328-1"
      }
    ],
    // '@nuxtjs/sitemap',
    "@nuxtjs/robots",
    'vue-social-sharing/nuxt'
  ],

  // sitemap: {
  //   path: '/sitemap.xml',
  //   cacheTime: 1000 * 60 * 60 * 2,
  //   trailingSlash: true,
  //   hostname: 'http://www.nestportugal.pt',
  //   gzip: true,
  //   routes: async () => {
  //     const { data } = await axios.get('http://localhost:1949/umbraco/api/WebsiteContent/GetWebsiteData')
  //     // const { data } = await axios(window.location.origin + '/Services/umbraco/api/WebsiteContent/GetWebsiteData')
  //     return data.RoutePaths
  //     // return data.map(user => `/users/${user.username}`)
  //   }
  // },
  robots: {
    UserAgent: "*",
    Disallow: "",
    sitemap: "/Services/sitemap/"
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { }
  },
  router: {
    middleware: "getInitialData"
  },
  // Transition pages
  pageTransition: {
    name: "page"
  }

}
