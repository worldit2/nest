import AOS from "aos"
import "~/assets/css/aos.css"
export default ({ app }) => {
  app.AOS = new AOS.init() // or any other options you need
}
