# 1. Nest

This project is split in two into single entities where the frontend and backend are uncoupled an run in single instances in the IIS web server.
The frontend is a single page web application meaning that whatever page you access in the web browser it will be redirected to root page of the website.

Technologies used in the frontend:
* Vue
* Nuxt

IDE for frontend:
* Visual Studio Code

Technologies used in the backend:
* ASP .net
* Umbraco CMS

IDE for backend:
* Visual Studio

## 1.1 Configure local host for development and testing

This is one of the methods found to test and develop the nest project in the local machine. The project run in the IIS in two instances, one for the frontend, and one for the backend.

### 1.1.1 Configure Frontend

To run the this project in the local machine you have to first compile a version of the frontend.
Open Visual Studio code frontend project in the folder Nest.Frontend. Then open a console terminal and run the following commands:

npm install

When you excute the following command it will be created a folder with the name "dist" in the Nest.Frontend folder.

Copy the file web.config which is located in the folder in the docs folder to the "dist" folder.

### 1.1.2 Configure Backend

For running and testing the web application in your local machine you need to configure the IIS.
It is recomended that you use the IIS express for running and testing although you can use the normal IIS. The steps bellow are for configuration of IIS express in your local machine. This configuration will setup visual studio so that evertime you run the application it will run two instances. One of the instances will point to the folder \Nest.Frontend\dist\ and the other one will point to the backend project located in \Nest.Web\ .
To do so you have to access the file \project\ .vs\config\applicationhost.config and change a few lines of code.

## 1.2 Endpoint testing