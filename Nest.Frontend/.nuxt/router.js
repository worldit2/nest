import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _081d8984 = () => interopDefault(import('..\\pages\\about-us\\index.vue' /* webpackChunkName: "pages/about-us/index" */))
const _093333ec = () => interopDefault(import('..\\pages\\challenges\\index.vue' /* webpackChunkName: "pages/challenges/index" */))
const _60b6748e = () => interopDefault(import('..\\pages\\contacts.vue' /* webpackChunkName: "pages/contacts" */))
const _66d7ceba = () => interopDefault(import('..\\pages\\events\\index.vue' /* webpackChunkName: "pages/events/index" */))
const _662db62b = () => interopDefault(import('..\\pages\\join-us.vue' /* webpackChunkName: "pages/join-us" */))
const _3d839a70 = () => interopDefault(import('..\\pages\\newsletter-subscriptions.vue' /* webpackChunkName: "pages/newsletter-subscriptions" */))
const _b7796d5c = () => interopDefault(import('..\\pages\\portfolio.vue' /* webpackChunkName: "pages/portfolio" */))
const _6cf7541a = () => interopDefault(import('..\\pages\\resources\\index.vue' /* webpackChunkName: "pages/resources/index" */))
const _ba7d6cee = () => interopDefault(import('..\\pages\\about-us\\our-purpose.vue' /* webpackChunkName: "pages/about-us/our-purpose" */))
const _3766a73a = () => interopDefault(import('..\\pages\\challenges\\_challenge.vue' /* webpackChunkName: "pages/challenges/_challenge" */))
const _4527139a = () => interopDefault(import('..\\pages\\events\\_event.vue' /* webpackChunkName: "pages/events/_event" */))
const _efd51ee4 = () => interopDefault(import('..\\pages\\resources\\_resource.vue' /* webpackChunkName: "pages/resources/_resource" */))
const _391920dc = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about-us",
    component: _081d8984,
    name: "about-us"
  }, {
    path: "/challenges",
    component: _093333ec,
    name: "challenges"
  }, {
    path: "/contacts",
    component: _60b6748e,
    name: "contacts"
  }, {
    path: "/events",
    component: _66d7ceba,
    name: "events"
  }, {
    path: "/join-us",
    component: _662db62b,
    name: "join-us"
  }, {
    path: "/newsletter-subscriptions",
    component: _3d839a70,
    name: "newsletter-subscriptions"
  }, {
    path: "/portfolio",
    component: _b7796d5c,
    name: "portfolio"
  }, {
    path: "/resources",
    component: _6cf7541a,
    name: "resources"
  }, {
    path: "/about-us/our-purpose",
    component: _ba7d6cee,
    name: "about-us-our-purpose"
  }, {
    path: "/challenges/:challenge",
    component: _3766a73a,
    name: "challenges-challenge"
  }, {
    path: "/events/:event",
    component: _4527139a,
    name: "events-event"
  }, {
    path: "/resources/:resource",
    component: _efd51ee4,
    name: "resources-resource"
  }, {
    path: "/",
    component: _391920dc,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
