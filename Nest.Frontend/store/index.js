import Axios from "axios"

export const state = () => ({
  // baseUrl: 'https://formacaojsf6.azurewebsites.net/',
  hasData: false,
  isLoading: false,
  modalOpened: false,
  modalStartupOpened: false,
  modalStartupInfo: "",
  transition: false,
  index: {},
})

export const getters = {
  getDetails(state) {
    return ({ details, area }) => state[area].find((item) => item.UrlName === details)
  },
}

export const mutations = {
  addToStore(state, { obj, target }) {
    for (const [key, value] of Object.entries(obj)) {
      const keyTrimmed = key.split("_")[0]
      if (state[target].hasOwnProperty(keyTrimmed)) {
        state[target][keyTrimmed].push(value)
      } else {
        state[target][keyTrimmed] = [value]
      }
    }
  },
  cleanStore(state) {
    for (const [key, value] of Object.entries(state)) {
      const keyTrimmed = key.split("_")[0]
      const detailsPage = value.challengeDetailsPage || value.ourPurposePage || value.eventDetailsPage || value.resourceDetailsPage

      if (detailsPage) {
        state[keyTrimmed + "Details"] = detailsPage
        delete value.websitePage
        state[keyTrimmed + "Details"].forEach(function (elem) {
          for (const [chave, obj] of Object.entries(elem.PageElements)) {
            const chavetrimmed = chave.split("_")[0]
            if (elem.hasOwnProperty(chavetrimmed)) {
              elem[chavetrimmed].push(obj)
            } else {
              elem[chavetrimmed] = [obj]
            }
            delete elem.PageElements
            delete elem.WebsitePages
          }
        })
      }
    }
  },
  setDataTrue(state) {
    state.hasData = true
  },
  startLoading(state) {
    state.isLoading = true
  },
  endLoading(state) {
    state.isLoading = false
  },
  toggleModal(state) {
    state.modalOpened = !state.modalOpened
  },
  toggleModalStartups(state) {
    state.modalStartupOpened = !state.modalStartupOpened
    if (document.querySelector("body").style.overflow == "hidden") {
      document.querySelector("body").style.overflow = "visible"
    } else {
      document.querySelector("body").style.overflow = "hidden"
    }
  },
  changeModalInfo(state, payload) {
    state.modalStartupInfo = payload
  },
  toggleTransition(state, payload) {
    state.transition = payload
  },
}

export const actions = {
  
  async getInitialData({ state, commit }) {
      const response = await Axios(window.location.origin + "/Services/umbraco/api/WebsiteContent/GetWebsiteData"),
        startResponse = await Axios(window.location.origin + "/Services/umbraco/api/Startups/StartupSearch?lang=en-US")
        // const response = await Axios("https://localhost:1951/Services/umbraco/api/WebsiteContent/GetWebsiteData"),
        // startResponse = await Axios("https://localhost:1951/Services/umbraco/api/Startups/StartupSearch?lang=en-US")
  
  
    const results = response.data,
      Startups = startResponse.data.Startups

    //Add startups to store
    state.Startups = Startups

    // Add index to the store
    commit("addToStore", {
      obj: results.PageElements,
      target: "index",
    })

    state.index.PageDescription = results.SeoDescription
    state.index.PageTitle = results.SeoTitle
    state.index.PageName = results.Name

    // All the other pages added to store
    for (const [key, value] of Object.entries(results.WebsitePages)) {
      const page = key.split("_")[0]
      state[page] = {}

      state[page].PageDescription = value.PageDefinitionDescription
      state[page].PageImage = value.PageDefinitionImage
      state[page].PageTitle = value.PageDefinitionTitle
      state[page].PageColor = value.StyleColorTheme.ColorName

      commit("addToStore", {
        obj: value.PageElements,
        target: page,
      })

      commit("addToStore", {
        obj: value.WebsitePages,
        target: page,
      })
    }

    commit("setDataTrue")
    commit("cleanStore")
    setTimeout(() => {
      commit("endLoading")
    }, 4000)
  },
}
