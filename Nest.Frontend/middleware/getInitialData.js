export default function({ store, redirect }, carryOn) {
  setTimeout(() => {
    carryOn()
  }, 900)
  if (!store.state.hasData) {
    store.commit("startLoading")
    store.dispatch("getInitialData")
  } else {
    store.commit("toggleTransition", true)
    setTimeout(() => {
      store.commit("toggleTransition", false)
    }, 1850)
  }
}
