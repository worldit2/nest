// dargains & borges - 13/12/18
// requires
const { series, parallel, src, dest, watch } = require('gulp');
const wrap = require('gulp-wrap');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const sassGlob = require('gulp-sass-glob');
const babel = require('gulp-babel');
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');
const cssnano = require('gulp-cssnano');
const browser = require('browser-sync');
const concat = require('gulp-concat');
const partials = require('gulp-inject-partials');
const inject = require('gulp-inject-string');
const autoprefixer = require('gulp-autoprefixer');
const index = require('gulp-index');
const fs = require('fs');
const clean = require('gulp-clean');
const wait = require('gulp-wait');
const image = require('gulp-image');
const argv = require('yargs').argv;
const namespace = 'F6';

const mainDev = new String(`document.querySelectorAll('[data-control]').forEach(element => {
  const data = element.dataset;
  const control = data.control;
  if (!document.getElementById(\`script\${control}\`)) {
    const script = document.createElement("script");
    script.id = \`script\${control}\`;
    script.src = \`/scripts/core/\${control}.js\`;
    document.body.appendChild(script);
    script.onload = function() {
      const obj = new ${namespace}[control];
      obj.init(element, data);
    }
  } else {
    setTimeout(() => {
      const obj = new ${namespace}[control];
      obj.init(element, data);
    }, 1000)
  }
})`);
const mainProd = new String(`document.querySelectorAll('[data-control]').forEach(element => {
  const data = element.dataset;
  const control = data.control;
  const obj = new ${namespace}[control];
  obj.init(element, data);
})`);

// builders
const buildModule = file => {
  return new Promise(resolve => {
    return src(file)
      .pipe(partials({
        removeTags: true
      }))
      .pipe(wrap({
        src: 'layout.html'
      }))
      .pipe(rename({
        dirname: ''
      }))
      .pipe(dest('../html/modules'))
      .on('end', resolve)
      .pipe(browser.reload({
        stream: true
      }))
  });
}
const buildAllModules = () => {
  return src('modules/**/*.html')
    .pipe(partials({
      removeTags: true
    }))
    .pipe(wrap({
      src: 'layout.html'
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(dest('../html/modules'));
}
const buildComponent = file => {
  return new Promise(resolve => {
    return src(file)
      .pipe(partials({
        removeTags: true
      }))
      .pipe(wrap({
        src: 'layout.html'
      }))
      .pipe(rename({
        dirname: ''
      }))
      .pipe(dest('../html/components'))
      .on('end', resolve)
      .pipe(browser.reload({
        stream: true
      }));
  });
}
const buildAllTemplates = () => {
  return src('templates/*.html')
    .pipe(partials({
      removeTags: true
    }))
    .pipe(wrap({
      src: 'layout.html'
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(dest('../html/templates'));
}
const buildTemplate = () => {
  return new Promise(resolve => {
    return src('templates/*.html')
      .pipe(partials({
        removeTags: true
      }))
      .pipe(wrap({
        src: 'layout.html'
      }))
      .pipe(rename({
        dirname: ''
      }))
      .pipe(dest('../html/templates'))
      .on('end', resolve)
      .pipe(browser.reload({
        stream: true
      }));
  });
}
const buildAllComponents = () => {
  return src('components/**/*.html')
    .pipe(partials({
      removeTags: true
    }))
    .pipe(wrap({
      src: 'layout.html'
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(dest('../html/components'));
}
const buildCSS = () => {
  return src('scss/main.scss')
    .pipe(sassGlob())
    .pipe(wait(1500))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      gulpbrowsers: ['last 2 versions']
    }))
    .pipe(cssnano())
    .pipe(dest('../css'))
    .pipe(browser.reload({
      stream: true
    }));
};
const buildJS = file => {
  return src(file)
    .pipe(plumber())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(dest('../scripts/core'))
    .pipe(browser.reload({
      stream: true,
      once: true
    }));
};
const buildAllJS = () => {
  return src(['modules/**/*.js', 'components/**/*.js'])
    .pipe(plumber())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(dest('../scripts/core'))
    .pipe(browser.reload({
      stream: true,
      once: true
    }));
};
const buildMainJS = isProd => {
  return src('main.js')
    .pipe(plumber())
    .pipe(isProd ? inject.append(mainProd) : inject.append(mainDev))
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(dest('../scripts'))
    .pipe(browser.reload({
      stream: true,
      once: true
    }));
};
const buildIndex = () => {
  console.log('building index...');
  return src('../html/**/*.html').pipe(index({
    // written out before index contents
    'prepend-to-output': () => `<head>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1, initial-scale = 1.0, shrink-to-fit=no"><link rel="shortcut icon" href="/images/favicon.ico" /><link rel="stylesheet" href="index.css"></head><body class="indexFile">`,
    'append-to-output': () => `</body>`,
    'title': `${namespace} Index`,
    'title-template': (title) => `<h1 class="index__title">${title}</h1>`,
    'pathDepth': 2,
    'section-template': (sectionContent) => `<section class="index__section">${sectionContent}</section>`,
    'section-heading-template': (heading) => `<h2 class="index__section-heading">${heading.split('html/')[1]}</h2>`,
    'list-template': (listContent) => `<ul class="index__list">${listContent}</ul>`,
    'item-template': (filepath, filename) => `<li class="index__item"><a class="index__item-link" href="${filepath}/${filename}">${filename.split('.html')[0]}</a></li>`,
    'relativePath': '../',
    'outputFile': './index.html',
  }))
    .pipe(dest('../'));
};

// watchers
const watchers = () => {
  // modules watcher
  watch('modules/**/*.html').on('change', file => {
    buildModule(file).then(() => {
      buildIndex();
      buildTemplate();
    });
  });
  // components watcher
  watch('components/**/*.html').on('change', file => {
    buildComponent(file).then(() => {
      buildIndex();
      buildTemplate();
    });
  });
  // templates watcher
  watch('templates/*.html').on('change', () => {
    buildTemplate().then(buildIndex);
  });
  // scss watcher
  watch(['scss/*.scss', 'scss/**/*.scss', 'modules/**/*.scss', 'components/**/*.scss']).on('change', () => {
    buildCSS();
  });
  // module js watcher
  watch(['modules/**/*.js', 'components/**/*.js']).on('change', file => {
    buildJS(file);
  });
  // support js watcher
  watch('main.js').on('change', () => {
    buildMainJS();
  });
};

// support
const browserSync = () => {
  return browser.init(null, {
    server: {
      baseDir: "../",
      index: "index.html"
    },
    port: 3000
  });
};
const optimizeImages = () => {
  return src('../images/*')
    .pipe(image({
      optipng: true
    }))
    .pipe(dest('../images'));
};


// create new modules/components
const htmlJSTemplate = name => `<section class="${name}" data-control="${name}">
  ${name}
</section>`;
const htmlNOJSTemplate = name => `<section class="${name}">
  ${name}
</section>`;
const cssTemplate = name => `.${name} {

}`;
const jsTemplate = name => `${namespace}.${name} = () => {
  return {
    init(element, data) {
      const view = this;
      view.el = element;
      view.data = data;
      view.variables();
      view.events();
    },
    variables() {
      const view = this;
    },
    events() {
      const view = this;
    },
  }
};`;

const newModule = cb => {
  const name = argv.name;
  if (typeof (name) !== 'string') {
    console.log('Por favor especifique o nome no formato --name nome');
    cb();
    return;
  }
  const filePath = `modules/${name}/`;
  const htmlTemplate = !argv.nojs ? htmlJSTemplate(name) : htmlNOJSTemplate(name);
  fs.writeFile(`${name}.html`, htmlTemplate, () => {
    new Promise(resolve => {
      return src(`${name}.html`).pipe(dest(filePath)).on('end', resolve);
    }).then(() => {
      buildModule(`${filePath}${name}.html`).then(buildIndex);
      src(`${name}.html`).pipe(clean());
    })
  });
  fs.writeFile(`${name}.scss`, cssTemplate(name), () => {
    new Promise(resolve => {
      return src(`${name}.scss`).pipe(dest(filePath)).on('end', resolve);
    }).then(() => {
      buildCSS(`${filePath}${name}.scss`);
      src(`${name}.scss`).pipe(clean());
    })
  });
  if (!argv.nojs) {
    fs.writeFile(`${name}.js`, jsTemplate(name), () => {
      new Promise(resolve => {
        return src(`${name}.js`).pipe(dest(filePath)).on('end', resolve);
      }).then(() => {
        buildJS(`${filePath}${name}.js`);
        src(`${name}.js`).pipe(clean());
      })
    });
  }
  cb();
}
const newComponent = cb => {
  const name = argv.name;
  if (typeof (name) !== 'string') {
    console.log('Por favor especifique o nome no formato --name nome');
    cb();
    return;
  }
  const filePath = `components/${name}/`
  fs.writeFile(`${name}.html`, htmlNOJSTemplate(name), () => {
    new Promise(resolve => {
      return src(`${name}.html`).pipe(dest(filePath)).on('end', resolve);
    }).then(() => {
      buildComponent(`${filePath}${name}.html`);
      src(`${name}.html`).pipe(clean());
    })
  });
  fs.writeFile(`${name}.scss`, cssTemplate(name), () => {
    new Promise(resolve => {
      return src(`${name}.scss`).pipe(dest(filePath)).on('end', resolve);
    }).then(() => {
      buildCSS(`${filePath}${name}.scss`);
      src(`${name}.scss`).pipe(clean());
    })
  });
  cb();
}
const production = () => {
  return src(['../scripts/main.js', '../scripts/core/*.js'])
    .pipe(concat('bundle.js'))
    .pipe(uglify())
    .pipe(dest('../scripts'));
}

exports.default = parallel(watchers, buildIndex, browserSync);
exports.newModule = newModule;
exports.newComponent = newComponent;
exports.optimizeImages = optimizeImages;
exports.production = series(optimizeImages, series(buildMainJS.bind(null, true), production));
exports.firstRun = series(parallel(buildCSS, buildMainJS, buildAllJS, buildAllComponents, buildAllModules, buildAllTemplates), buildIndex);
