F6.moduleA = () => {
  return {
    init(element, data) {
      const view = this;
      view.el = element;
      view.data = data;
      view.variables();
      view.events();
      console.log(element,data);
    },
    variables() {
      const view = this;
    },
    events() {
      const view = this;
    },
  }
};
