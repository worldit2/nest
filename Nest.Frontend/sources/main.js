// nodeList forEach polyfill
window.NodeList&&!NodeList.prototype.forEach&&(NodeList.prototype.forEach=function(o,t){t=t||window;for(var i=0;i<this.length;i++)o.call(t,this[i],i,this)});

const F6 = {
  main() {
    return {
      init() {
        const view = this;
        view.variables();
        view.events();
      },
      variables() {
        const view = this;
      },
      events() {
        const view = this;
      },
    }
  }
};

F6.main().init();
