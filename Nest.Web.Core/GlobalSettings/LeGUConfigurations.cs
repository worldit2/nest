﻿using System.Configuration;
using System.Linq;

namespace LeGU.Web.MP.Core.GlobalSettings
{
    /// <summary>
    /// Class derivation of ConfigurationSection to Implement Custom Section to contain application configurations.
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationSection" />
    public class LeGUConfigurations : ConfigurationSection
    {
        [ConfigurationProperty("globalSettings")]
        public GlobalSettingsCollection GlobalSettings { get { return this["globalSettings"] as GlobalSettingsCollection; } }

        [ConfigurationProperty("googleAPISSettings")]
        public GoogleAPISSettingsCollection GoogleAPISSettings { get { return this["googleAPISSettings"] as GoogleAPISSettingsCollection; } }

        [ConfigurationProperty("partialViewsSettings")]
        public PartialViewsSettingsCollection PartialViewsSettings { get { return this["partialViewsSettings"] as PartialViewsSettingsCollection; } }

        [ConfigurationProperty("socialShareUrls")]
        public SocialShareUrlsCollection SocialShareUrls { get { return this["socialShareUrls"] as SocialShareUrlsCollection; } }

        [ConfigurationProperty("contentSecurityPolicySettings")]
        public ContentSecurityPolicySettingsCollection ContentSecurityPolicySettings { get { return this["contentSecurityPolicySettings"] as ContentSecurityPolicySettingsCollection; } }
    }

    #region Global Settings
    public class GlobalSettingsCollection : ConfigurationElementCollection
    {
        public GlobalSettings this[int index]
        {
            get { return base.BaseGet(index) as GlobalSettings; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new GlobalSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((GlobalSettings)element).Name;
        }

        public GlobalSettings Get(string name)
        {
            return this.Cast<GlobalSettings>().FirstOrDefault(method => method.Name.Equals(name));
        }
    }

    public class GlobalSettings : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name { get { return this["name"] as string; } }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value { get { return this["value"] as string; } }
    }
    #endregion Global Settings

    #region Google APIS Settings
    public class GoogleAPISSettingsCollection : ConfigurationElementCollection
    {
        public GoogleAPISSettings this[int index]
        {
            get { return base.BaseGet(index) as GoogleAPISSettings; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new GoogleAPISSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((GoogleAPISSettings)element).Name;
        }

        public GoogleAPISSettings Get(string name)
        {
            return this.Cast<GoogleAPISSettings>().FirstOrDefault(method => method.Name.Equals(name));
        }
    }

    public class GoogleAPISSettings : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name { get { return this["name"] as string; } }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value { get { return this["value"] as string; } }
    }
    #endregion Google APIS Settings

    #region Partial Views Settings
    public class PartialViewsSettingsCollection : ConfigurationElementCollection
    {
        public PartialViewsSettings this[int index]
        {
            get { return base.BaseGet(index) as PartialViewsSettings; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new PartialViewsSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PartialViewsSettings)element).Name;
        }

        public PartialViewsSettings Get(string key)
        {
            return this.Cast<PartialViewsSettings>().FirstOrDefault(method => method.Name.Equals(key));
        }
    }

    public class PartialViewsSettings : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name { get { return this["name"] as string; } }

        [ConfigurationProperty("cachedSeconds", IsRequired = true)]
        public int? CachedSeconds { get { return this["cachedSeconds"] as int?; } }

        [ConfigurationProperty("cacheByPage", IsRequired = true)]
        public bool? CacheByPage { get { return this["cacheByPage"] as bool?; } }
    }
    #endregion

    #region Social Share Urls
    public class SocialShareUrlsCollection : ConfigurationElementCollection
    {
        public SocialShareUrls this[int index]
        {
            get { return base.BaseGet(index) as SocialShareUrls; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new SocialShareUrls();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SocialShareUrls)element).Name;
        }

        public SocialShareUrls Get(string name)
        {
            return this.Cast<SocialShareUrls>().FirstOrDefault(method => method.Name.Equals(name));
        }
    }
    public class SocialShareUrls : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name { get { return this["name"] as string; } }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value { get { return this["value"] as string; } }
        [ConfigurationProperty("icon", IsRequired = true)]
        public string Icon { get { return this["icon"] as string; } }
    }
    #endregion

    #region Content Security Policy Settings
    public class ContentSecurityPolicySettingsCollection : ConfigurationElementCollection
    {
        public ContentSecurityPolicySettings this[int index]
        {
            get { return base.BaseGet(index) as ContentSecurityPolicySettings; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ContentSecurityPolicySettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ContentSecurityPolicySettings)element).Name;
        }

        public ContentSecurityPolicySettings Get(string name)
        {
            return this.Cast<ContentSecurityPolicySettings>().FirstOrDefault(method => method.Name.Equals(name));
        }
    }

    public class ContentSecurityPolicySettings : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name { get { return this["name"] as string; } }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value { get { return this["value"] as string; } }
    }
    #endregion Global Settings
}
