﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Linq;

namespace Nest.Web.Core.GlobalSettings
{
    //Use this for all configurations files.
    //Does not need to the be just the Nest.config
    public class SettingsCollection : ConfigurationElementCollection
    {
        public ConfigElem this[int index]
        {
            get { return base.BaseGet(index) as ConfigElem; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemoveAt(index);
                this.BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ConfigElem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConfigElem)element).Name;
        }

        public T Get<T>(string key)
        {
            var appSetting = this.Cast<ConfigElem>().SingleOrDefault(method => method.Name.Equals(key)).Value;

            if (appSetting != null)
            {
                try
                {
                    var converter = TypeDescriptor.GetConverter(typeof(T));
                    return (T)(converter.ConvertFromInvariantString(appSetting));
                }
                catch (Exception e)
                {
                    var error = $"Error while getting the value from key \"{key}\": {e}";
                    throw new ConfigurationErrorsException(error);
                }
            }
            else
            {
                var error = $"Key \"{key}\" does not have a value or does not exits in configuration file.";
                throw new ConfigurationErrorsException(error);
            }
        }
    }

    public class ConfigElem : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)(base["name"]); }
            set { base["name"] = value; }
        }
        [ConfigurationProperty("value", IsKey = false, IsRequired = true)]
        public string Value
        {
            get { return (string)(base["value"]); }
            set { base["value"] = value; }
        }
    }
}
