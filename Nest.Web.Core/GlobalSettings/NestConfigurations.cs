﻿using System.Configuration;

namespace Nest.Web.Core.GlobalSettings
{
    public class NestConfigurations : ConfigurationSection
    {
        [ConfigurationProperty("NodeRules", IsRequired = true)]
        public SettingsCollection NodeRules
        {
            get { return (SettingsCollection)this["NodeRules"]; }
        }

        //Example
        //Instead of the NodeRules in the file add OtherSettingsCollections
        //[ConfigurationProperty("OtherSettingsCollections", IsRequired = true)]
        //public SettingsCollection NodeRules
        //{
        //    get { return (SettingsCollection)this["OtherSettingsCollections"]; }
        //}
    }
}