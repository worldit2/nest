﻿using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace LeGU.Web.Core.Converters
{
    [PropertyValueType(typeof(string))]
    [PropertyValueCache(PropertyCacheValue.Source, PropertyCacheLevel.Content)]
    [PropertyValueCache(PropertyCacheValue.Object, PropertyCacheLevel.ContentCache)]
    [PropertyValueCache(PropertyCacheValue.XPath, PropertyCacheLevel.Content)]
    public class InputCharLimitEditorPropertyConverter : IPropertyValueConverter
    {
        public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source != null)
            {
                return source.ToString();
            }

            return string.Empty;
        }

        public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {

            if(source == null)
            {
                return string.Empty;
            }

            return source.ToString();
        }

        public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
        {
            return source.ToString();
        }

        public bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("Input.CharLimitEditor");
        }
    }
}