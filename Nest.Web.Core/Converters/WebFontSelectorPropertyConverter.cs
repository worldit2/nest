﻿using LeGU.Web.Core.Objects.Models;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace LeGU.Web.Core.Converters
{
    [PropertyValueType(typeof(IEnumerable<WebFont>))]
    [PropertyValueCache(PropertyCacheValue.Source, PropertyCacheLevel.Content)]
    [PropertyValueCache(PropertyCacheValue.Object, PropertyCacheLevel.ContentCache)]
    [PropertyValueCache(PropertyCacheValue.XPath, PropertyCacheLevel.Content)]
    public class WebFontSelectorPropertyConverter : IPropertyValueConverter
    {
        public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();

            List<RawFont> deserealizedFonts = scriptSerializer.Deserialize<List<RawFont>>(source.ToString());

            return deserealizedFonts;
        }

        public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            List<RawFont> deserealizedFonts = (List<RawFont>)source;
            List<WebFont> webFonts = new List<WebFont>();

            foreach (RawFont font in deserealizedFonts)
            {
                WebFont webFont;
                if (font.fontType.Equals("web", StringComparison.InvariantCultureIgnoreCase))
                {
                    webFont = new WebFont
                    {
                        FontType = WebFontType.WEB,
                        AltFont = font.fontAltFont,
                        LetterSpacing = font.letterSpacing,
                        FontCSS = font.fontCss,
                        FontFamily = font.fontName,
                        FontVariant = string.Empty
                    };
                }
                else
                {
                    webFont = new WebFont
                    {
                        FontType = WebFontType.GOOGLE,
                        AltFont = font.fontAltFont,
                        LetterSpacing = font.letterSpacing,
                        FontFamily = font.fontName,
                        FontCSS = string.Format("http://fonts.googleapis.com/css?family={0}", font.fontName),
                        FontVariant = font.fontVariant
                    };
                }

                webFonts.Add(webFont);
            }

            return webFonts;
        }

        public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
        {
            return source.ToString();
        }

        public bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("WebFontSelector");
        }
    }

    public class RawFont
    {
        public string fontCss { get; set; }
        public string fontType { get; set; }
        public string fontName { get; set; }
        public string fontAltFont { get; set; }
        public string letterSpacing { get; set; }
        public string fontVariant { get; set; }
        public override bool Equals(object obj)
        {
            if (!obj.GetType().Equals(typeof(RawFont)))
            {
                return false;
            }

            RawFont objFont = (RawFont)obj;

            if (!string.IsNullOrEmpty(fontCss) && !string.IsNullOrEmpty(objFont.fontCss)
                && (string.IsNullOrEmpty(fontCss) && !string.IsNullOrEmpty(objFont.fontCss)
                || !string.IsNullOrEmpty(fontCss) && string.IsNullOrEmpty(objFont.fontCss)
                || !fontCss.Equals(objFont.fontCss)))
                return false;

            if (!string.IsNullOrEmpty(fontType) && !string.IsNullOrEmpty(objFont.fontType)
                && (string.IsNullOrEmpty(fontType) && !string.IsNullOrEmpty(objFont.fontType)
                || !string.IsNullOrEmpty(fontType) && string.IsNullOrEmpty(objFont.fontType)
                || !fontType.Equals(objFont.fontType)))
                return false;

            if (!string.IsNullOrEmpty(fontName) && !string.IsNullOrEmpty(objFont.fontName)
                && (string.IsNullOrEmpty(fontName) && !string.IsNullOrEmpty(objFont.fontName)
                || !string.IsNullOrEmpty(fontName) && string.IsNullOrEmpty(objFont.fontName)
                || !fontName.Equals(objFont.fontName)))
                return false;

            if (!string.IsNullOrEmpty(fontAltFont) && !string.IsNullOrEmpty(objFont.fontAltFont)
                && (string.IsNullOrEmpty(fontAltFont) && !string.IsNullOrEmpty(objFont.fontAltFont)
                || !string.IsNullOrEmpty(fontAltFont) && string.IsNullOrEmpty(objFont.fontAltFont)
                || !fontAltFont.Equals(objFont.fontAltFont)))
                return false;

            if (!string.IsNullOrEmpty(letterSpacing) && !string.IsNullOrEmpty(objFont.letterSpacing)
                && (string.IsNullOrEmpty(letterSpacing) && !string.IsNullOrEmpty(objFont.letterSpacing)
                || !string.IsNullOrEmpty(letterSpacing) && string.IsNullOrEmpty(objFont.letterSpacing)
                || !letterSpacing.Equals(objFont.letterSpacing)))
                return false;

            if (!string.IsNullOrEmpty(fontVariant) && !string.IsNullOrEmpty(objFont.fontVariant)
                && (string.IsNullOrEmpty(fontVariant) && !string.IsNullOrEmpty(objFont.fontVariant)
                || !string.IsNullOrEmpty(fontVariant) && string.IsNullOrEmpty(objFont.fontVariant)
                || !fontVariant.Equals(objFont.fontVariant)))
                return false;

            return true;
        }
        public override int GetHashCode()
        {
            var hashCode = 509274795;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(fontType);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(fontName);
            return hashCode;
        }
    }
}