﻿using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace LeGU.Web.Core.Converters
{
    [PropertyValueType(typeof(string))]
    [PropertyValueCache(PropertyCacheValue.Source, PropertyCacheLevel.Content)]
    [PropertyValueCache(PropertyCacheValue.Object, PropertyCacheLevel.ContentCache)]
    [PropertyValueCache(PropertyCacheValue.XPath, PropertyCacheLevel.Content)]
    public class SimpleColorPickerPropertyConverter : IPropertyValueConverter
    {
        public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source != null)
            {
                return source.ToString();
            }

            return source;
        }

        public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {

            if(source == null)
            {
                return string.Empty;
            }

            string s = source.ToString();

            if (string.IsNullOrEmpty(s) ||( s.Length != 3 && s.Length != 6))
            {
                return string.Empty;
            }

            if (s.Length == 3)
            {
                s = string.Format("{0}{0}{1}{1}{2}{2}", s[0], s[1], s[2]);
            }

            return s;
        }

        public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
        {
            return source.ToString();
        }

        public bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("SimpleColorPicker");
        }
    }
}