﻿using LeGU.Web.Core.Objects.Models;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace LeGU.Web.Core.Converters
{
    [PropertyValueType(typeof(TableEditor))]
    [PropertyValueCache(PropertyCacheValue.Source, PropertyCacheLevel.Content)]
    [PropertyValueCache(PropertyCacheValue.Object, PropertyCacheLevel.ContentCache)]
    [PropertyValueCache(PropertyCacheValue.XPath, PropertyCacheLevel.Content)]
    public class TableEditorPropertyConverter : IPropertyValueConverter
    {
        public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source != null)
            {
                JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();

                RawTable deserealizedTable = scriptSerializer.Deserialize<RawTable>(source.ToString());

                return deserealizedTable;
            }

            return source;
        }

        public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            if(source != null)
            {
                RawTable rawTable = (RawTable)source;

                List<List<TableEditorCell>> tableRows = new List<List<TableEditorCell>>();

                foreach(List<CellItem> rawTableRow in rawTable.tableContent)
                {
                    List<TableEditorCell> tableRow = new List<TableEditorCell>();

                    foreach(CellItem item in rawTableRow)
                    {
                        tableRow.Add(new TableEditorCell() { Content = new HtmlString(item.value), ContentType = item.type ?? string.Empty } );
                    }

                    tableRows.Add(tableRow);
                }

                return new TableEditor()
                {
                    FirstRowAsHeader = rawTable.firstRowAsHeader,
                    FirstColAsHeader = rawTable.firstColAsHeader,
                    Rows = tableRows
                };
            }

            return source;
        }

        public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
        {
            return source.ToString();
        }

        public bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("TableEditor");
        }
    }
    class RawTable
    {
        public bool firstRowAsHeader { get; set; }
        public bool firstColAsHeader { get; set; }
        public List<List<CellItem>> tableContent { get; set; }
    }
    class CellItem
    {
        public string value { get; set; }
        public bool isHtml { get; set; }
        public string type { get; set; }
    }
}