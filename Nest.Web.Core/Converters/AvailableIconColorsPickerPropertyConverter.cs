﻿using LeGU.Web.Core.Objects.Models;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;

namespace LeGU.Web.Core.Converters
{
    [PropertyValueType(typeof(IEnumerable<AvailableColorsModel>))]
    [PropertyValueCache(PropertyCacheValue.Source, PropertyCacheLevel.Content)]
    [PropertyValueCache(PropertyCacheValue.Object, PropertyCacheLevel.ContentCache)]
    [PropertyValueCache(PropertyCacheValue.XPath, PropertyCacheLevel.Content)]
    public class AvailableIconColorsPickerPropertyConverter : IPropertyValueConverter
    {
        public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source != null)
            {
                JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();

                List<RawAvailableColor> deserealizedColors = scriptSerializer.Deserialize<List<RawAvailableColor>>(source.ToString());

                return deserealizedColors;
            }

            return source;
        }

        public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            List<AvailableColorsModel> returnList = new List<AvailableColorsModel>();

            if (source != null)
            {
                List<RawAvailableColor> deserealizedColors = (List<RawAvailableColor>)source;

                foreach (RawAvailableColor color in deserealizedColors)
                {
                    returnList.Add(new AvailableColorsModel()
                    {
                        ColorHexCode = color.ColorHexCode,
                        UseDarkFont = color.UseDarkFont
                    });
                }
            }

            return returnList;
        }

        public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
        {
            return source.ToString();
        }

        public bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("AvailableIconColorsPicker");
        }
    }
    class RawAvailableColor
    {
        public string ColorHexCode { get; set; }
        public bool UseDarkFont { get; set; }
        public bool toggleColor { get; set; }
    }
}