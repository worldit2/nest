﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web;

namespace LeGU.Web.Core.Converters
{
    [PropertyValueType(typeof(IEnumerable<IPublishedContent>))]
    [PropertyValueCache(PropertyCacheValue.Source, PropertyCacheLevel.Content)]
    [PropertyValueCache(PropertyCacheValue.Object, PropertyCacheLevel.ContentCache)]
    [PropertyValueCache(PropertyCacheValue.XPath, PropertyCacheLevel.Content)]
    public class UploadGridPickerPropertyConverter : IPropertyValueConverter
    {
        public object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source != null)
            {
                JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();

                List<RawGridUpload> deserealizedUdi = scriptSerializer.Deserialize<List<RawGridUpload>>(source.ToString());

                return deserealizedUdi;
            }

            return source;
        }

        public object ConvertSourceToObject(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source != null)
            {
                List<RawGridUpload> udis = (List<RawGridUpload>)source;

                List<IPublishedContent> mediaContents = new List<IPublishedContent>();

                UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

                Udi udi = null;
                foreach(RawGridUpload raw in udis)
                {
                    if (Udi.TryParse(raw.udi, out udi))
                    {
                        var media = helper.TypedMedia(udi);
                        if (media != null)
                        {
                            mediaContents.Add(media);
                        }
                    }
                }

                return mediaContents;
            }

            return source;
        }

        public object ConvertSourceToXPath(PublishedPropertyType propertyType, object source, bool preview)
        {
            return source.ToString();
        }

        public bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("UploadGrid");
        }
    }

    class RawGridUpload
    {
        public string udi { get; set; }
        public string path { get; set; }
    }
}