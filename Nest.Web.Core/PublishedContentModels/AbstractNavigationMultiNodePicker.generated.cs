//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace LeGU.Web.Core.PublishedContentModels
{
	// Mixin content Type 1134 with alias "abstractNavigationMultiNodePicker"
	/// <summary>Abstract Navigation - Multi Node Picker</summary>
	public partial interface IAbstractNavigationMultiNodePicker : IPublishedContent
	{
		/// <summary>Transparent Theme</summary>
		bool NavigationMenuAllowNavigationTheme { get; }

		/// <summary>Menu Builder</summary>
		IEnumerable<IPublishedContent> NavigationMenuMenuBuilder { get; }

		/// <summary>Mobile Animation</summary>
		string NavigationMenuNaviagtionAnimation { get; }

		/// <summary>Menu Alignment</summary>
		string NavigationMenuNavigationAlignment { get; }

		/// <summary>Mobile Animation Full</summary>
		bool NavigationMenuNavigationFullMobile { get; }

		/// <summary>Logo Alignment</summary>
		string NavigationMenuNavigationLogoAlignment { get; }

		/// <summary>Navigation Sticky</summary>
		bool NavigationMenuNavigationSticky { get; }
	}

	/// <summary>Abstract Navigation - Multi Node Picker</summary>
	[PublishedContentModel("abstractNavigationMultiNodePicker")]
	public partial class AbstractNavigationMultiNodePicker : PublishedContentModel, IAbstractNavigationMultiNodePicker
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "abstractNavigationMultiNodePicker";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public AbstractNavigationMultiNodePicker(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<AbstractNavigationMultiNodePicker, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Transparent Theme: If this property is enabled and the {b}first{/b} module of a page has and uses the 'Fullscreen' property, the menu will be transparent
		///</summary>
		[ImplementPropertyType("navigationMenuAllowNavigationTheme")]
		public bool NavigationMenuAllowNavigationTheme
		{
			get { return GetNavigationMenuAllowNavigationTheme(this); }
		}

		/// <summary>Static getter for Transparent Theme</summary>
		public static bool GetNavigationMenuAllowNavigationTheme(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<bool>("navigationMenuAllowNavigationTheme"); }

		///<summary>
		/// Menu Builder: This links will be displayed in the menu up to 7 items
		///</summary>
		[ImplementPropertyType("navigationMenuMenuBuilder")]
		public IEnumerable<IPublishedContent> NavigationMenuMenuBuilder
		{
			get { return GetNavigationMenuMenuBuilder(this); }
		}

		/// <summary>Static getter for Menu Builder</summary>
		public static IEnumerable<IPublishedContent> GetNavigationMenuMenuBuilder(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<IEnumerable<IPublishedContent>>("navigationMenuMenuBuilder"); }

		///<summary>
		/// Mobile Animation: How the menu behaves when it is to be displayed in devices below 768px wide Default animation is 'Slide right'
		///</summary>
		[ImplementPropertyType("navigationMenuNaviagtionAnimation")]
		public string NavigationMenuNaviagtionAnimation
		{
			get { return GetNavigationMenuNaviagtionAnimation(this); }
		}

		/// <summary>Static getter for Mobile Animation</summary>
		public static string GetNavigationMenuNaviagtionAnimation(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<string>("navigationMenuNaviagtionAnimation"); }

		///<summary>
		/// Menu Alignment: How the menu should be aligned. Default value is 'Right'
		///</summary>
		[ImplementPropertyType("navigationMenuNavigationAlignment")]
		public string NavigationMenuNavigationAlignment
		{
			get { return GetNavigationMenuNavigationAlignment(this); }
		}

		/// <summary>Static getter for Menu Alignment</summary>
		public static string GetNavigationMenuNavigationAlignment(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<string>("navigationMenuNavigationAlignment"); }

		///<summary>
		/// Mobile Animation Full: How the menu behaves when it is displayed in devices below 768px wide. By default it covers 50% of the screen. If set to true, it will cover 100%.
		///</summary>
		[ImplementPropertyType("navigationMenuNavigationFullMobile")]
		public bool NavigationMenuNavigationFullMobile
		{
			get { return GetNavigationMenuNavigationFullMobile(this); }
		}

		/// <summary>Static getter for Mobile Animation Full</summary>
		public static bool GetNavigationMenuNavigationFullMobile(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<bool>("navigationMenuNavigationFullMobile"); }

		///<summary>
		/// Logo Alignment: {b}If, and only if,{/b} the logo is the only element in the menu, you can choose how to align it here. Default value is Left
		///</summary>
		[ImplementPropertyType("navigationMenuNavigationLogoAlignment")]
		public string NavigationMenuNavigationLogoAlignment
		{
			get { return GetNavigationMenuNavigationLogoAlignment(this); }
		}

		/// <summary>Static getter for Logo Alignment</summary>
		public static string GetNavigationMenuNavigationLogoAlignment(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<string>("navigationMenuNavigationLogoAlignment"); }

		///<summary>
		/// Navigation Sticky: If enabled, the menu will be sticked to the top of the page.
		///</summary>
		[ImplementPropertyType("navigationMenuNavigationSticky")]
		public bool NavigationMenuNavigationSticky
		{
			get { return GetNavigationMenuNavigationSticky(this); }
		}

		/// <summary>Static getter for Navigation Sticky</summary>
		public static bool GetNavigationMenuNavigationSticky(IAbstractNavigationMultiNodePicker that) { return that.GetPropertyValue<bool>("navigationMenuNavigationSticky"); }
	}
}
