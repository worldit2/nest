//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace LeGU.Web.Core.PublishedContentModels
{
	/// <summary>Social Item</summary>
	[PublishedContentModel("socialItem")]
	public partial class SocialItem : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "socialItem";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public SocialItem(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<SocialItem, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Social Icon: The social network's icon
		///</summary>
		[ImplementPropertyType("contentSocialIcon")]
		public object ContentSocialIcon
		{
			get { return this.GetPropertyValue("contentSocialIcon"); }
		}

		///<summary>
		/// Social Link: Define here the link to a Social Page.
		///</summary>
		[ImplementPropertyType("contentSocialLink")]
		public string ContentSocialLink
		{
			get { return this.GetPropertyValue<string>("contentSocialLink"); }
		}

		///<summary>
		/// Social Name: The social network's name (it will be used if no icon is chosen)
		///</summary>
		[ImplementPropertyType("contentSocialName")]
		public string ContentSocialName
		{
			get { return this.GetPropertyValue<string>("contentSocialName"); }
		}
	}
}
