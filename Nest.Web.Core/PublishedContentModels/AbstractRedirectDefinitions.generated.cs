//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace LeGU.Web.Core.PublishedContentModels
{
	// Mixin content Type 1143 with alias "abstractRedirectDefinitions"
	/// <summary>Abstract Redirect Definitions</summary>
	public partial interface IAbstractRedirectDefinitions : IPublishedContent
	{
		/// <summary>Destination</summary>
		Umbraco.Web.Models.Link RedirectDestination { get; }
	}

	/// <summary>Abstract Redirect Definitions</summary>
	[PublishedContentModel("abstractRedirectDefinitions")]
	public partial class AbstractRedirectDefinitions : PublishedContentModel, IAbstractRedirectDefinitions
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "abstractRedirectDefinitions";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public AbstractRedirectDefinitions(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<AbstractRedirectDefinitions, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Destination: Define the page (internal or external) to be redirected to
		///</summary>
		[ImplementPropertyType("redirectDestination")]
		public Umbraco.Web.Models.Link RedirectDestination
		{
			get { return GetRedirectDestination(this); }
		}

		/// <summary>Static getter for Destination</summary>
		public static Umbraco.Web.Models.Link GetRedirectDestination(IAbstractRedirectDefinitions that) { return that.GetPropertyValue<Umbraco.Web.Models.Link>("redirectDestination"); }
	}
}
