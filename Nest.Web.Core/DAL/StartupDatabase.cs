﻿using System;
using Umbraco.Core.Persistence;

namespace Nest.Web.Core.DAL
{
    [TableName("Startup")]
    [ExplicitColumns]
    public class StartupDatabase
    {
        [Column("Id")]
        public Guid Id { get; set; }

        [Column("StartupName")]
        public string StartupName { get; set; }

        [Column("WebsiteName")]
        public string WebsiteName { get; set; }

        [Column("Email")]
        public string Email { get; set; }

        [Column("ContactNumber")]
        public string ContactNumber { get; set; }

        [Column("BusinessType")]
        public string BusinessType { get; set; }

        [Column("Categories")]
        public string Categories { get; set; }

        [Column("StartupDescription")]
        public string StartupDescription { get; set; }

        [Column("IsStartupUpdate")]
        public bool IsStartupUpdate { get; set; }

        [Column("ExistingStartupName")]
        public string ExistingStartupName { get; set; }

        [Column("Authorize")]
        public bool Authorize { get; set; }

        [Column("DateCreatedUTC")]
        public DateTime DateCreatedUTC { get; set; }

        [Column("DateUpdatedUTC")]
        public DateTime DateUpdatedUTC { get; set; }

        [Column("IsValidated")]
        public bool? IsValidated { get; set; }

        [Column("NodeId")]
        public int? NodeId { get; set; }

        [Column("IsoCode")]
        public string IsoCode { get; set; }

        [Ignore]
        public StartupFileDatabase File { get; set; }
    }
}
