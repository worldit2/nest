﻿using Nest.Web.Core.Managers;
using System;
using System.Linq;
using System.Web;
using UIOMatic.Attributes;
using UIOMatic.Enums;
using Umbraco.Core.Persistence;

namespace Nest.Web.Core.DAL
{
    [UIOMatic("Startup", "Startup Submissions", "Startup Submission",
        FolderIcon = "icon-lightbulb color-yellow",
        ItemIcon = "icon-lightbulb color-yellow",
        ReadOnly = true,
        SortColumn = "DateCreatedUTC",
        //Order = 1,
        RenderType = UIOMaticRenderType.List,
        //ShowOnSummaryDashboard = true
        ListViewActions = new[] { typeof(ExportAction) }
    )]
    [TableName("StartupSubmitInfo")]
    [ExplicitColumns]
    public class StartupSubmitInfo
    {
        [Column("Id")]
        public Guid Id { get; set; }

        [UIOMaticField(Name = "Startup Name", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField(Name = "Startup Name")]
        [Column("StartupName")]
        public string StartupName { get; set; }

        [UIOMaticField(Name = "Startup Description", View = UIOMatic.Constants.FieldEditors.Label)]
        //[UIOMaticListViewField(Name = "Startup Description")]
        [Column("StartupDescription")]
        public string StartupDescription { get; set; }

        [UIOMaticField(Name = "Website", View = UIOMatic.Constants.FieldEditors.Label)]
        //[UIOMaticListViewField(Name = "Website")]
        [Column("WebsiteName")]
        public string WebsiteName { get; set; }

        [UIOMaticField(Name = "Email", View = UIOMatic.Constants.FieldEditors.Label)]
        //[UIOMaticListViewField]
        [Column("Email")]
        public string Email { get; set; }

        [UIOMaticField(Name = "Contact Number", View = UIOMatic.Constants.FieldEditors.Label)]
        //[UIOMaticListViewField(Name = "Contact Number")]
        [Column("ContactNumber")]
        public string ContactNumber { get; set; }

        [UIOMaticField(Name = "Business Type", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField(Name = "Business Type")]
        [Column("BusinessType")]
        public string BusinessType { get; set; }

        [UIOMaticField(Name = "Categories", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField]
        [Column("Categories")]
        public string Categories { get; set; }

        [UIOMaticField(Name = "Other Categories", Description = "Other categories submited", View = UIOMatic.Constants.FieldEditors.Label)]
        [Column("StartupCategories")]
        public string OtherCategories { get; set; }

        [Column("IsStartupUpdate")]
        public bool IsStartupUpdate_Ex { get; set; }

        [UIOMaticField(Name = "Startup Update", Description = "It is a startup update", View = UIOMatic.Constants.FieldEditors.Label)]
        [UIOMaticListViewField(Name = "Startup Update")]
        [Ignore]
        public string IsStartupUpdate_
        {
            get { return IsStartupUpdate_Ex ? "Yes" : "No"; }
        }

        [Column("ExistingStartupName")]
        public string ExistingStartupName { get; set; }

        [Ignore]
        public bool IsValidated
        {
            get { return NodeId_Ex.HasValue ? new StartupManager().CheckIfStartupIsValidated(NodeId_Ex.Value) : false; }
        }


        [UIOMaticField(Name = "Existing Startup Name", Description = "Name of the startup, for updating existing startup.", View = UIOMatic.Constants.FieldEditors.Label)]
        //[UIOMaticListViewField(Name = "Existing Startup Name")]
        [Ignore]
        public string ExistingStartupNameUIOMaticPro_Ex
        {
            get { return !string.IsNullOrWhiteSpace(ExistingStartupName) ? ExistingStartupName : ""; }
        }

        //[UIOMaticField(Name = "Authorize", Description = "Consent authorization.", View = UIOMatic.Constants.FieldEditors.Label)]
        [Column("Authorize")]
        public bool Authorize { get; set; }


        [Column("DateCreatedUTC")]
        public DateTime DateCreatedUTC_Ex { get; set; }

        [UIOMaticField(Name = "Submission Date UTC", Description = "Submission Date of the startup info.", View = UIOMatic.Constants.FieldEditors.Label)]
        [Ignore]
        public string DateCreatedUTC_
        {
            get { return DateCreatedUTC_Ex.ToString("yyyy-MM-dd HH:mm:ss"); }
        }

        [Column("DateUpdatedUTC")]
        public DateTime DateUpdatedUTC_Ex { get; set; }

        [Column("NodeId")]
        public int? NodeId_Ex { get; set; }

        [UIOMaticListViewField(Name = "Validate Startup", View = "~/App_Plugins/UIOMatic/backoffice/CustomField/ButtonValidation.html")]
        [Ignore]
        public StartupButtonInfo ValidateStartup_Ex
        {
            get { return new StartupButtonInfo(Id, NodeId_Ex.HasValue ? new StartupManager().CheckIfStartupIsValidated(NodeId_Ex.Value) : false); }
        }

        [Column("FilePath")]
        public string FilePath { get; set; }


        [UIOMaticField(Description = "Image submited by the startup", View = "~/App_Plugins/UIOMatic/backoffice/CustomField/StartupImageViewer.html")]
        [Ignore]
        public string Image
        {
            get
            {
                return FilePath != null ? $@"{HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)}/services/StartupImages/{FilePath.Split('\\').Last()}" : null;
            }
        }

        public class StartupButtonInfo
        {
            public StartupButtonInfo(Guid id, bool startupIsValid)
            {
                Id = id;
                StartupIsValid = startupIsValid;
            }
            public Guid Id { get; private set; }
            public bool StartupIsValid { get; private set; }
        }

        public override string ToString()
        {
            return StartupName;
        }
    }

    [UIOMaticAction("export", "Export", "~/App_Plugins/UIOMatic/backoffice/Export/export.html", Icon = "icon-document-dashed-line")]
    public class ExportAction { }
}
