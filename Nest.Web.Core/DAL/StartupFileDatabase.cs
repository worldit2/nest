﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Nest.Web.Core.DAL
{
    [TableName("StartupFile")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    public class StartupFileDatabase
    {
        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Column("StartupId")]
        public Guid StartupId { get; set; }
        [Column("FilePath")]
        public string FilePath { get; set; }
    }
}
