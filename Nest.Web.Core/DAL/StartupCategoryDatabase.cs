﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Nest.Web.Core.DAL
{
    [TableName("StartupCategory")]
    [PrimaryKey("Id", autoIncrement = true)]
    [ExplicitColumns]
    public class StartupCategoryDatabase
    {
        [Column("Id")]
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Column("StartupId")]
        public Guid StartupId { get; set; }
        [Column("CategoryName")]
        public string CategoryName { get; set; }
    }
}
