﻿using System.Collections.Generic;
using System.Web;

namespace LeGU.Web.Core.Objects.Models
{
    public class TableEditor
    {
        public List<List<TableEditorCell>> Rows { get; set; }
        public bool FirstRowAsHeader { get; set; }
        public bool FirstColAsHeader { get; set; }
    }
    public class TableEditorCell
    {
        public HtmlString Content { get; set; }
        public string ContentType { get; set; }
    }

}
