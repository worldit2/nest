﻿namespace LeGU.Web.Core.Objects.Models
{
    public class AvailableColorsModel
    {
        public string ColorHexCode { get; set; }
        public bool UseDarkFont { get; set; }
    }
}