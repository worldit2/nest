﻿namespace LeGU.Web.Core.Objects.Models
{
    public class WebFont
    {
        public WebFontType FontType { get; set; }
        public string FontFamily { get; set; }
        public string FontVariant { get; set; }
        public string LetterSpacing { get; set; }
        public string AltFont { get; set; }
        public string FontCSS { get; set; }
    }
    public enum WebFontType
    {
        WEB,
        GOOGLE
    }
}