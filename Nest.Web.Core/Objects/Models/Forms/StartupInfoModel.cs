﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nest.Web.Core.Objects.Models
{
    public class StartupInfoModel : BaseModel
    {
        [Required]
        public string StartupName { get; set; }
        public string WebsiteName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string ContactNumber { get; set; }
        [Required]
        public string BusinessType { get; set; }
        public string Categories { get; set; }        
        public string StartupDescription { get; set; }
        public bool IsStartupUpdate { get; set; }
        public string ExistingStartupName { get; set; }
        [Required]
        public bool Authorize { get; set; }
        public IEnumerable<string> OtherCategories { get; set; }
        [Required]
        public string IsoCode { get; set; }
    }
}
