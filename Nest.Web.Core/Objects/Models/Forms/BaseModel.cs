﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Nest.Web.Core.Objects.Models
{
    public abstract class BaseModel
    {
        public void Validate()
        {
            var context = new ValidationContext(this, null, null);
            var results = new List<ValidationResult>();
            if (!Validator.TryValidateObject(this, context, results, true))
            {
                throw new ValidationException(string.Join(";", results));
            }
        }
    }
}
