﻿using DotSee;
using LeGU.Web.Core.PublishedContentModels;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;

namespace Nest.Web.Core.UmbracoExtensions
{
    public class NestCustomNodeRules
    {
        //Check the node rules the collection was created
        private List<NestRule> Rules = new List<NestRule>();

        private IEnumerable<string> PageElementsNames = new string[] 
        {
            PageElements.ModelTypeAlias,
            ChallengeDetailPageElements.ModelTypeAlias,
            EventDetailPageElements.ModelTypeAlias,
            ResourceDetailsPageElements.ModelTypeAlias,
            AboutUsPageElements.ModelTypeAlias,
            ChallengesPageElements.ModelTypeAlias,
            ContactUsPageElements.ModelTypeAlias,
            EventsPageElements.ModelTypeAlias,
            HomepagePageElements.ModelTypeAlias,
            JoinUsPageElements.ModelTypeAlias,
            NewsletterSubscriptionsPageElements.ModelTypeAlias,
            OurPurposePageElements.ModelTypeAlias,
            PortfolioPageElements.ModelTypeAlias,
            PrivacyPolicyPageElements.ModelTypeAlias,
            ResourcesPageElements.ModelTypeAlias,
            SubscribePageElements.ModelTypeAlias
        };

        
        private IEnumerable<string> WebsitePageNames = new string[] 
        {
            WebsitePage.ModelTypeAlias,
            ChallengeDetailsPage.ModelTypeAlias,
            EventDetailsPage.ModelTypeAlias,
            OurPurposePage.ModelTypeAlias,
            ResourceDetailsPage.ModelTypeAlias,
            AboutUsPage.ModelTypeAlias,
            ChallengesPage.ModelTypeAlias,
            ContactsPage.ModelTypeAlias,
            EventsPage.ModelTypeAlias,
            JoinUsPage.ModelTypeAlias,
            NewsletterSubscriptionsPage.ModelTypeAlias,
            PortfolioPage.ModelTypeAlias,
            PrivacyPolicyPage.ModelTypeAlias,
            ResourcesPage.ModelTypeAlias
        };

        //To preserve the route names in the umbraco
        private IDictionary<string, string> PageRouteNames = new Dictionary<string, string>() {
            { AboutUsPage.ModelTypeAlias, "About Us" },
            { OurPurposePage.ModelTypeAlias, "Our Purpose" },

            { ChallengesPage.ModelTypeAlias, "Challenges" },
            //{ ChallengeDetailsPage.ModelTypeAlias, "" },

            { ResourcesPage.ModelTypeAlias, "Resources" },
            //{ ResourceDetailsPage.ModelTypeAlias, "" },

            { EventsPage.ModelTypeAlias, "Events" },
            //{ EventDetailsPage.ModelTypeAlias, "" },

            { JoinUsPage.ModelTypeAlias, "Join Us" },

            { NewsletterSubscriptionsPage.ModelTypeAlias, "Newsletter Subscriptions" },

            { ContactsPage.ModelTypeAlias, "contacts" },

            { PrivacyPolicyPage.ModelTypeAlias, "Privacy Policy" },

            { PortfolioPage.ModelTypeAlias, "Portfolio" },
        };

        public NestCustomNodeRules()
        {
            #region HomePage
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, HomepagePageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, HighlightSlider.ModelTypeAlias, "1- Highlight Slider"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, ContentMediaDualImage.ModelTypeAlias, "2- Content Media Dual Image"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, InfoIcons.ModelTypeAlias, "4- Info Icons"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "5- Simple Heading"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "6- Related Pages"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "7- CTA Block (1)"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "8- Simple Heading (1)"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "9- Related Pages (1)"));
            Rules.Add(new NestRule(HomepagePageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "10- CTA Block"));
            #endregion

            #region About Us
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, AboutUsPage.ModelTypeAlias, "About Us"));
            Rules.Add(new NestRule(AboutUsPage.ModelTypeAlias, AboutUsPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, ContentMediaBlack.ModelTypeAlias, "2- Content Media Black"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, InfoIcons.ModelTypeAlias, "4- Info Icons"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "5- Simple Heading"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "6- RelatedPages"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "7- CTA Block"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "8- Simple Heading"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "9- Related Pages"));
            Rules.Add(new NestRule(AboutUsPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "10- CTA Block"));
            #endregion

            #region Our purpose
            Rules.Add(new NestRule(AboutUsPage.ModelTypeAlias, OurPurposePage.ModelTypeAlias, "Our Purpose"));
            Rules.Add(new NestRule(OurPurposePage.ModelTypeAlias, OurPurposePageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, WOT.ModelTypeAlias, "2- WOT"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, ContentMediaWhite.ModelTypeAlias, "3- Content Media White"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, ContentMediaDualImage.ModelTypeAlias, "4- Content Media Dual Image"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, InfoIconsGroups.ModelTypeAlias, "5- InfoIconsGroups"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "6- CTablock"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "7- SimpleHeading"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "8- RelatedPages"));
            Rules.Add(new NestRule(OurPurposePageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "9- CTablock"));
            #endregion

            #region Challenges
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, ChallengesPage.ModelTypeAlias, "Challenges Page"));
            Rules.Add(new NestRule(ChallengesPage.ModelTypeAlias, ChallengesPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- MainHeading"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, ContentMediaWhite.ModelTypeAlias, "2- Content Media White"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, InfoIcons.ModelTypeAlias, "4- Info Icons"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "5- Simple Heading"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "6- Related Pages"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "7- Simple Heading"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "8- Related Pages"));
            Rules.Add(new NestRule(ChallengesPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "9- CTA Block"));
            #endregion

            #region Challenge Detail
            Rules.Add(new NestRule(ChallengeDetailsPage.ModelTypeAlias, ChallengeDetailPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, WOT.ModelTypeAlias, "2- WOT"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, InfoIconsGroups.ModelTypeAlias, "3- Sectors"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "4- CTA Block"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "5- Simple Heading"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "6- Related Pages"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "7- CTA Block"));
            Rules.Add(new NestRule(ChallengeDetailPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "8- CTA Block"));
            #endregion

            #region Resources
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, ResourcesPage.ModelTypeAlias, "Resources Page"));
            Rules.Add(new NestRule(ResourcesPage.ModelTypeAlias, ResourcesPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(ResourcesPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(ResourcesPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "2- Related Pages"));
            Rules.Add(new NestRule(ResourcesPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(ResourcesPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "4- Related Pages"));
            Rules.Add(new NestRule(ResourcesPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "5- CTA Block"));
            #endregion

            #region Resource Detail
            Rules.Add(new NestRule(ResourceDetailsPage.ModelTypeAlias, ResourceDetailsPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, ContentMediaBlack.ModelTypeAlias, "2- Content Media Black"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, SimpleMedia.ModelTypeAlias, "4- Simple Media"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "5- Simple Heading"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, InfoIcons.ModelTypeAlias, "6- Info Icons"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, WOT.ModelTypeAlias, "7- WOT"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "8- CTA Block"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "9- Simple Heading"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "10- Related Pages"));
            Rules.Add(new NestRule(ResourceDetailsPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "11- CTA Block"));
            #endregion

            #region Events
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, EventsPage.ModelTypeAlias, "Events Page"));
            Rules.Add(new NestRule(EventsPage.ModelTypeAlias, EventsPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(EventsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(EventsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "2- Simple Heading"));
            Rules.Add(new NestRule(EventsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "3- Related Pages"));
            Rules.Add(new NestRule(EventsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "4- Simple Heading"));
            Rules.Add(new NestRule(EventsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "5- Related Pages"));
            Rules.Add(new NestRule(EventsPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "5- CTA Block"));
            #endregion

            #region Event Details
            Rules.Add(new NestRule(EventDetailsPage.ModelTypeAlias, EventDetailPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(EventDetailPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(EventDetailPageElements.ModelTypeAlias, WOT.ModelTypeAlias, "2- WOT"));
            Rules.Add(new NestRule(EventDetailPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "3- CTA Block"));
            Rules.Add(new NestRule(EventDetailPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "4- Simple Heading"));
            Rules.Add(new NestRule(EventDetailPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "5- Related Pages"));
            Rules.Add(new NestRule(EventDetailPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "6- CTA Block"));
            #endregion

            //TODO PORTFOLIO

            #region Join us
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, JoinUsPage.ModelTypeAlias, "Join Us"));
            Rules.Add(new NestRule(JoinUsPage.ModelTypeAlias, JoinUsPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(JoinUsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            //Rules.Add(new NestRule(JoinUsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "2- Startup Form"));
            Rules.Add(new NestRule(JoinUsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(JoinUsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "4- Related Pages"));
            Rules.Add(new NestRule(JoinUsPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "5- CTA Block"));
            #endregion

            #region Newsletter Subscriptions
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, NewsletterSubscriptionsPage.ModelTypeAlias, "Newsletter Subscriptions"));
            Rules.Add(new NestRule(NewsletterSubscriptionsPage.ModelTypeAlias, NewsletterSubscriptionsPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(NewsletterSubscriptionsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(NewsletterSubscriptionsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "2- Simple Heading"));
            Rules.Add(new NestRule(NewsletterSubscriptionsPageElements.ModelTypeAlias, NewsletterForm.ModelTypeAlias, "3- Newsletter Form"));
            Rules.Add(new NestRule(NewsletterSubscriptionsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "4- Simple Heading"));
            Rules.Add(new NestRule(NewsletterSubscriptionsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "5- Related Pages"));
            #endregion

            #region Contact Us
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, ContactsPage.ModelTypeAlias, "Contact Us"));
            Rules.Add(new NestRule(ContactsPage.ModelTypeAlias, ContactUsPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(ContactUsPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(ContactUsPageElements.ModelTypeAlias, ContentMediaWhite.ModelTypeAlias, "2- Content Media White"));
            Rules.Add(new NestRule(ContactUsPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(ContactUsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "4- Related Pages"));
            Rules.Add(new NestRule(ContactUsPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "5- CTA Block"));
            #endregion

            #region Privacy Policy
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, PrivacyPolicyPage.ModelTypeAlias, "Privacy Policy"));
            Rules.Add(new NestRule(PrivacyPolicyPage.ModelTypeAlias, PrivacyPolicyPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(PrivacyPolicyPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(PrivacyPolicyPageElements.ModelTypeAlias, WOT.ModelTypeAlias, "2- WOT"));
            #endregion

            #region Portfolio
            Rules.Add(new NestRule(MultiPageWebsite.ModelTypeAlias, PortfolioPage.ModelTypeAlias, "Contact Us"));
            Rules.Add(new NestRule(PortfolioPage.ModelTypeAlias, PortfolioPageElements.ModelTypeAlias, "Page Elements"));
            //Page Elements
            Rules.Add(new NestRule(PortfolioPageElements.ModelTypeAlias, MainHeading.ModelTypeAlias, "1- Main Heading"));
            Rules.Add(new NestRule(PortfolioPageElements.ModelTypeAlias, ContentMediaWhite.ModelTypeAlias, "2- Content Media White"));
            Rules.Add(new NestRule(PortfolioPageElements.ModelTypeAlias, SimpleHeading.ModelTypeAlias, "3- Simple Heading"));
            Rules.Add(new NestRule(PortfolioPageElements.ModelTypeAlias, RelatedPages.ModelTypeAlias, "4- Related Pages"));
            Rules.Add(new NestRule(PortfolioPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "5- CTA Block"));
            Rules.Add(new NestRule(PortfolioPageElements.ModelTypeAlias, CTablock.ModelTypeAlias, "6- CTA Block"));
            #endregion
        }

        //Method to check if the page element order is valid
        public bool PageElementsOrderIsValid(IContent contentItem)
        {
            if (contentItem != null)
            {
                //check if the parent node is a page elements aggregatetor
                if (PageElementsNames.Any(a => a == contentItem.Parent().ContentType.Alias))
                {
                    var pageElementsRules = Rules.Where(a => a.CreatedDocTypeAlias == contentItem.Parent().ContentType.Alias).ToList();
                    var umbracoPageElements = contentItem.Parent().Children().ToList();

                    for (int x = 0, y = 0; x < pageElementsRules.Count; x++)
                    {
                        if ((y < umbracoPageElements.Count) && pageElementsRules[x].DocTypeAliasToCreate == umbracoPageElements[y].ContentType.Alias)
                        {
                            y++;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public bool RouteNameIsValid(IContent contentItem)
        {
            if(contentItem != null)
            {
                //check if any of the elements is a websitePage
                if(WebsitePageNames.Any(a => a == contentItem.ContentType.Alias))
                {
                    //if it is check if the name is preserved
                    if(contentItem.Name == PageRouteNames[contentItem.ContentType.Alias])//If the name is the same
                    {
                        return true;
                    }
                    else//If the name is not the same
                    {
                        return false;
                    }
                }
                
                return true;
            }
            else
            {
                return true;
            }
        }

        public List<NestRule> GetPageElementsRules(string pageElementsNodeAllias)
        {
            return Rules.Where(a => a.CreatedDocTypeAlias == pageElementsNodeAllias).ToList();
        }

        public void AutoNodeNestConfigurations()
        {
            var autonode = AutoNode.Instance;
            Rules.ForEach(a => autonode.RegisterRule(new AutoNodeRule(a.CreatedDocTypeAlias, a.DocTypeAliasToCreate, a.NodeName, false, false)));
        }
    }

    public class NestRule
    {
        public NestRule(string createdDocTypeAlias, string docTypeAliasToCreate, string nodeName)
        {
            CreatedDocTypeAlias = createdDocTypeAlias;
            DocTypeAliasToCreate = docTypeAliasToCreate;
            NodeName = nodeName;
        }
        public string CreatedDocTypeAlias { get; }
        public string DocTypeAliasToCreate { get; }
        public string NodeName { get; }
    }
}
