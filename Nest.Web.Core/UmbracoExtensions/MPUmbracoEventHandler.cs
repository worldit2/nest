﻿using DotSee;
using LeGU.Web.Core.PublishedContentModels;
using log4net;
using Nest.Web.Core.GlobalSettings;
using Nest.Web.Core.Managers;
using Nest.Web.Core.UmbracoExtensions;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace LeGU.Web.MP.Core.UmbracoExtensions
{
    /// <summary>
    /// Class derivation on ApplicationEventHandler to override Application/Umbraco Events.
    /// </summary>
    /// <seealso cref="Umbraco.Core.ApplicationEventHandler" />
    public class MPUmbracoEventHandler : ApplicationEventHandler
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(MPUmbracoEventHandler));
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //var _configurations = ConfigurationManager.GetSection("NestConfigurations") as NestConfigurations;
            //var y = _configurations.NodeRules.Get<bool>("CheckPageElementOrder");

            new AutoMapperWebConfiguration().Configure();

            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            base.ApplicationStarting(umbracoApplication, applicationContext);
        }
        /// <summary>
        /// Overrides Event Published on Application Started pipeline moment.
        /// </summary>
        /// <param name="umbracoApplicationBase">The umbraco application base.</param>
        /// <param name="applicationContext">The application context.</param>
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var _configurations = ConfigurationManager.GetSection("NestConfigurations") as NestConfigurations;

            ContentService.Saved += ContentService_Saved;

            //get all the umbraco languages
            #region Clean content cache
            var isoLanguages = applicationContext.Services.LocalizationService.GetAllLanguages().Select(a => a.IsoCode).ToList();

            ContentService.Published += delegate
            {
                foreach (var isoLang in isoLanguages)
                {
                    HttpRuntime.Cache.Remove($"WebsiteData_{isoLang}");
                }
            };
            ContentService.UnPublished += delegate
            {
                foreach (var isoLang in isoLanguages)
                {
                    HttpRuntime.Cache.Remove($"WebsiteData_{isoLang}");
                }
            };
            #endregion

            #region AutoNode Rules Setup
            AutoNode au = AutoNode.Instance;

            //Website Creation
            au.RegisterRule(new AutoNodeRule(SinglePageWebsite.ModelTypeAlias, ConfigurationFolder.ModelTypeAlias, "Website Configurations Folder", false, false));
            au.RegisterRule(new AutoNodeRule(SinglePageWebsite.ModelTypeAlias, PageElements.ModelTypeAlias, "Page Elements", false, true, false));

            au.RegisterRule(new AutoNodeRule(MultiPageWebsite.ModelTypeAlias, ConfigurationFolder.ModelTypeAlias, "Website Configurations Folder", false, false));
            //au.RegisterRule(new AutoNodeRule(MultiPageWebsite.ModelTypeAlias, PageElements.ModelTypeAlias, "Page Elements", false, true, false));

            //Configuration Folder Creation
            au.RegisterRule(new AutoNodeRule(ConfigurationFolder.ModelTypeAlias, GlobalConfigurationSettings.ModelTypeAlias, "Website Configurations", false, true));
            au.RegisterRule(new AutoNodeRule(ConfigurationFolder.ModelTypeAlias, ReusableSectionsFolder.ModelTypeAlias, "Reusable Sections", false, true));

            //New WebsitePage Creation
            au.RegisterRule(new AutoNodeRule(WebsitePage.ModelTypeAlias, PageElements.ModelTypeAlias, "Page Elements", false, true, false));

            //AutoNode Nest Configurations
            new NestCustomNodeRules().AutoNodeNestConfigurations();
            #endregion

            #region To preserve the document order
            //if (_configurations.NodeRules.Get<bool>("CheckPageElementOrder"))
            //{
            //    ContentService.Saving += delegate (IContentService sender, SaveEventArgs<IContent> e) { CheckNodesOrder(e, e.SavedEntities); };
            //    ContentService.Copying += delegate (IContentService sender, CopyEventArgs<IContent> e) { CheckNodeOrder(e, e.Copy); };
            //    ContentService.Moving += delegate (IContentService sender, MoveEventArgs<IContent> e) { CheckNodesOrder(e, e.MoveInfoCollection.Select(a => a.Entity)); };
            //    ContentService.Trashing += delegate (IContentService sender, MoveEventArgs<IContent> e) { CheckNodesOrder(e, e.MoveInfoCollection.Select(a => a.Entity)); };
            //}
            #endregion

            #region Preserve website route names
            //if (_configurations.NodeRules.Get<bool>("CheckPageNames"))
            //{
            //    ContentService.Saving += delegate (IContentService sender, SaveEventArgs<IContent> e) { CheckWebsitePageNames(e, e.SavedEntities); };
            //    ContentService.Copying += delegate (IContentService sender, CopyEventArgs<IContent> e) { CheckWebsitePageName(e, e.Copy); };
            //    ContentService.Moving += delegate (IContentService sender, MoveEventArgs<IContent> e) { CheckWebsitePageNames(e, e.MoveInfoCollection.Select(a => a.Entity)); };
            //    ContentService.Trashing += delegate (IContentService sender, MoveEventArgs<IContent> e) { CheckWebsitePageNames(e, e.MoveInfoCollection.Select(a => a.Entity)); };
            //}
            #endregion

            base.ApplicationStarted(umbracoApplication, applicationContext);
        }

        private void CheckNodesOrder(CancellableEventArgs e, IEnumerable<IContent> modifiedContent)
        {
            if (null != e && null != modifiedContent)
            {
                foreach (var contentItem in modifiedContent)
                {
                    CheckNodeOrder(e, contentItem);
                }
            }
        }

        private void CheckNodeOrder(CancellableEventArgs e, IContent modifiedContent)
        {
            if (!new NestCustomNodeRules().PageElementsOrderIsValid(modifiedContent))
            {
                if (e.CanCancel)
                {
                    e.Cancel = true;
                    e.Messages.Add(new EventMessage("Error", "Cannot save changes due to node roles"));
                }
            }
        }

        private void CheckWebsitePageNames(CancellableEventArgs e, IEnumerable<IContent> modifiedContent)
        {
            if(null != e && null != modifiedContent)
            {
                foreach (var contentItem in modifiedContent)
                {
                    CheckWebsitePageName(e, contentItem);
                }
            }
        }

        private void CheckWebsitePageName(CancellableEventArgs e, IContent modifiedContent)
        {
            if (!new NestCustomNodeRules().RouteNameIsValid(modifiedContent))
            {
                if (e.CanCancel)
                {
                    e.Cancel = true;
                    e.Messages.Add(new EventMessage("Error", "Name of the website page cannot be renamed."));
                }
            }
        }

        private void ContentService_Saved(IContentService sender, SaveEventArgs<IContent> e)
        {
            if (null != e && null != e.SavedEntities)
            {
                foreach (Content _content in e.SavedEntities)
                {
                    switch (_content.ContentType.Alias)
                    {
                        case WebsitePage.ModelTypeAlias:
                        case MultiPageWebsite.ModelTypeAlias:
                        case SinglePageWebsite.ModelTypeAlias:

                            // Rule to preserve Website Configurations Folder at bottom
                            IContent parentContent = _content.Parent() ?? _content;
                            List<IContent> childNodes = parentContent.Children().ToList();
                            int totalChilds = childNodes.Count();

                            if (childNodes.Any(child => child.ContentType.Alias.Equals(ConfigurationFolder.ModelTypeAlias)))
                            {
                                IContent _configFolder = childNodes.FirstOrDefault(child => child.ContentType.Alias.Equals(ConfigurationFolder.ModelTypeAlias));
                                int configFolderPosition = childNodes.FindIndex(child => child.ContentType.Alias.Equals(ConfigurationFolder.ModelTypeAlias));

                                List<IContent> tmpList = new List<IContent>();
                                tmpList.AddRange(childNodes.Where(child => !child.ContentType.Alias.Equals(ConfigurationFolder.ModelTypeAlias)));
                                tmpList.Add(_configFolder);
                                var sortResult = sender.Sort(tmpList, 0, false);
                            }

                            break;
                    }
                }
            }
        }

    }
}