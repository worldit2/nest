﻿using Umbraco.Web.Mvc;

namespace LeGU.Web.MP.Core.UmbracoExtensions
{
    public class CustomPartialsViewEngine : RenderViewEngine
    {
        private static string[] NewPartialsViewFormats = new[] {
            "~/Views/Partials/ModularPlatform/WebsiteContent/Content/Render_{0}.cshtml",
            "~/Views/Partials/ModularPlatform/WebsiteContent/Functional/Render_{0}.cshtml",
            "~/Views/Partials/ModularPlatform/WebsiteContent/Headings/Render_{0}.cshtml",
            "~/Views/Partials/ModularPlatform/WebsiteContent/Featured And Related/Render_{0}.cshtml"
        };

        public CustomPartialsViewEngine()
        {
            base.ViewLocationFormats = new string[] { "~/Views/{0}.cshtml" };
            base.PartialViewLocationFormats = NewPartialsViewFormats;
        }
    }
}
