﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Nest.Web.Core.UmbracoExtensions
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //name: "ResourceNotFound",
            //routeTemplate: "api/{*uri}",
            //defaults: new { controller = "Default", uri = RouteParameter.Optional });
            config.MapHttpAttributeRoutes();
        }
    }
}
