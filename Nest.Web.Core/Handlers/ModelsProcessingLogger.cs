﻿using log4net;
using System.Reflection;
using Umbraco.Core;

namespace LeGU.Web.Core.Handlers
{
    public class ModelsProcessingLogger : ApplicationEventHandler
    {
        public static readonly ILog modelsProcessingLogger =
            LogManager.GetLogger(
                MethodBase.GetCurrentMethod().DeclaringType
            );
    }
}
