﻿using Our.Umbraco.Vorto.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Umbraco.Core.Models.PublishedContent;
namespace Nest.Web.Core.Helpers
{
    public static class ExtensionMethods
    {
        public static T GetExistingVortoValue<T>(this PublishedContentModel contentModel, string propertyName, string lang = null)
        {
            if (contentModel == null || string.IsNullOrEmpty(propertyName))
                return default(T);
            List<string> cultureLanguagesPreferredOrder = new List<string>() { "en-US" };
            //List<string> cultureLanguagesPreferredOrder = new UmbracoTools().GetAllLanguages();
            T objectToReturn = !string.IsNullOrEmpty(lang)
                                    ? contentModel.GetVortoValue<T>(propertyName, lang)
                                    : contentModel.GetVortoValue<T>(propertyName);
            switch (typeof(T).Name)
            {
                case "String":
                    if (objectToReturn != null && !string.IsNullOrEmpty(objectToReturn.ToString()))
                    {
                        return objectToReturn;
                    }
                    foreach (string cultureLang in cultureLanguagesPreferredOrder)
                    {
                        objectToReturn = contentModel.GetVortoValue<T>(propertyName, cultureName: cultureLang);
                        if (objectToReturn != null && !string.IsNullOrEmpty(objectToReturn.ToString()))
                            return objectToReturn;
                    }
                    break;
                case "String[]":
                    if (objectToReturn != null && (objectToReturn as string[]).Count() > 0)
                        return objectToReturn;
                    foreach (string cultureLang in cultureLanguagesPreferredOrder)
                    {
                        objectToReturn = contentModel.GetVortoValue<T>(propertyName, cultureName: cultureLang);
                        if (objectToReturn != null && (objectToReturn as string[]).Count() > 0)
                            return objectToReturn;
                    }
                    break;
                case "Object":
                    if (objectToReturn != null)
                        return objectToReturn;
                    foreach (string cultureLang in cultureLanguagesPreferredOrder)
                    {
                        objectToReturn = contentModel.GetVortoValue<T>(propertyName, cultureName: cultureLang);
                        if (objectToReturn != null)
                            return objectToReturn;
                    }
                    break;
            }
            return default(T);
        }

        public static string GetDateWithSuffixFormat(this DateTime dateTime, CultureInfo cultureInfo, DateTime? dateTimeEnd = null)
        {
            if (cultureInfo.Equals(CultureInfo.CreateSpecificCulture("en-US")))
            {
                if (!dateTimeEnd.HasValue)
                {
                    return $"{dateTime.ToString("dd", cultureInfo)}{GetDaySuffix(dateTime.Day)} {dateTime.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                }
                else//case there is the end date
                {
                    if (dateTime.Date == dateTimeEnd.Value.Date)
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)}{GetDaySuffix(dateTime.Day)} {dateTime.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                    }
                    else if (dateTime.Month == dateTimeEnd.Value.Month)
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)}{GetDaySuffix(dateTime.Day)} - {dateTimeEnd.Value.ToString("dd", cultureInfo)}{GetDaySuffix(dateTimeEnd.Value.Day)} {dateTime.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                    }
                    else if((dateTime.Month != dateTimeEnd.Value.Month) && (dateTime.Year == dateTimeEnd.Value.Year))
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)}{GetDaySuffix(dateTime.Day)} {dateTime.ToString("MMMM", cultureInfo)} - {dateTimeEnd.Value.ToString("dd", cultureInfo)}{GetDaySuffix(dateTimeEnd.Value.Day)} {dateTimeEnd.Value.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                    }
                    else// if(dateTime.Year != dateTimeEnd.Value.Year)
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)}{GetDaySuffix(dateTime.Day)} {dateTime.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)} - {dateTimeEnd.Value.ToString("dd", cultureInfo)}{GetDaySuffix(dateTimeEnd.Value.Day)} {dateTimeEnd.Value.ToString("MMMM", cultureInfo)} {dateTimeEnd.Value.ToString("yyyy", cultureInfo)}";
                    }
                }
            }
            else
            {
                if (!dateTimeEnd.HasValue)
                {
                    return $"{dateTime.ToString("m", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                }
                else
                {
                    if (dateTime.Date == dateTimeEnd.Value.Date)
                    {
                        return $"{dateTime.ToString("m", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                    }
                    //check if same month -> if so add the code above
                    if (dateTime.Month == dateTimeEnd.Value.Month)
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)} - {dateTimeEnd.Value.ToString("dd", cultureInfo)} {dateTime.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                    }
                    else if ((dateTime.Month != dateTimeEnd.Value.Month) && (dateTime.Year == dateTimeEnd.Value.Year))
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)} {dateTime.ToString("MMMM", cultureInfo)} - {dateTimeEnd.Value.ToString("dd", cultureInfo)} {dateTimeEnd.Value.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)}";
                    }
                    else// if(dateTime.Year != dateTimeEnd.Value.Year)
                    {
                        return $"{dateTime.ToString("dd", cultureInfo)} {dateTime.ToString("MMMM", cultureInfo)} {dateTime.ToString("yyyy", cultureInfo)} - {dateTimeEnd.Value.ToString("dd", cultureInfo)} {dateTimeEnd.Value.ToString("MMMM", cultureInfo)} {dateTimeEnd.Value.ToString("yyyy", cultureInfo)}";
                    }
                }
            }
        }

        private static string GetDaySuffix(int dayOfWeek)
        {
            //Get the suffix of the day
            string daySuffix;
            switch (dayOfWeek)
            {
                case 1:
                case 21:
                case 31:
                    daySuffix = "st";
                    break;
                case 2:
                case 22:
                    daySuffix = "nd";
                    break;
                case 3:
                case 23:
                    daySuffix = "rd";
                    break;
                default:
                    daySuffix = "th";
                    break;
            }

            return daySuffix;
        }
    }
}