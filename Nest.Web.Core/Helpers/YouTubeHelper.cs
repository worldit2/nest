﻿using System;

namespace LeGU.Web.MP.Core.Helpers
{
    public class YouTubeHelper
    {
        /// <summary>
        /// Gets the YouTube code that is unique for the video
        /// </summary>
        /// <param name="youtubeUrl"></param>
        /// <returns>The youtube video code or empty, if param is an empty string</returns>
        public static string GetYoutubeCodeFromUrl(string youtubeUrl)
        {
            if (string.IsNullOrEmpty(youtubeUrl))
                return string.Empty;

            if (youtubeUrl.Contains("?v="))
            {
                return youtubeUrl.Split(new[] { "?v=", "&" }, StringSplitOptions.RemoveEmptyEntries)[1];
            }

            string[] splittedUrl = youtubeUrl.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            return splittedUrl[splittedUrl.Length - 1].Split(new[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0];
        }
        /// <summary>
        /// Gets the YouTube max resolution image url for the given video code
        /// </summary>
        /// <param name="videoCode"></param>
        /// <returns>The video's default max resolution image url or empty, if param is an empty string</returns>
        public static string GetYoutubeVideoImage(string videoCode)
        {
            if (string.IsNullOrEmpty(videoCode))
            {
                return string.Empty;
            }

            string youtubeVideoThumbnail = string.Format("https://img.youtube.com/vi/{0}/maxresdefault.jpg", videoCode);

            return youtubeVideoThumbnail;
        }
        public static string GetYoutubeEmbedUrl(string videoCode)
        {
            if (string.IsNullOrEmpty(videoCode))
            {
                return string.Empty;
            }

            return string.Format("https://www.youtube.com/embed/{0}", videoCode);
        }
    }
}
