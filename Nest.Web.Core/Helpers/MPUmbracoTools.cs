﻿using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace LeGU.Web.MP.Core.Helpers
{
    public class MPUmbracoTools
    {
        private UmbracoHelper _umbHelper = new UmbracoHelper(UmbracoContext.Current);

        public string GetDocTypeParentName(IPublishedContent publishedContent)
        {
            IContentTypeService contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
            IContentType pubContentType = contentTypeService.GetContentType(publishedContent.DocumentTypeId);
            var parentContentType = contentTypeService.GetContentTypeContainer(pubContentType.ParentId);

            if (parentContentType == null)
            {
                return string.Empty;
            }

            return parentContentType.Name;
        }

    }
}
