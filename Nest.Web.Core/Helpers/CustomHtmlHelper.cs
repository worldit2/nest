﻿using HtmlAgilityPack;
using System.Web;

namespace LeGU.Web.MP.Core.Helpers
{
    public class CustomHtmlHelper
    {
        public string RemoveAttributesFromImg(IHtmlString input)
        {
            if (input != null)
            {
                string inputString = input.ToString();
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(inputString);
                HtmlNodeCollection imgNodes = htmlDoc.DocumentNode.SelectNodes("//img");

                if (imgNodes != null)
                {
                    foreach (HtmlNode node in imgNodes)
                    {
                        if (node.Attributes.Contains("width"))
                        {
                            node.Attributes.Remove("width");
                        }

                        if (node.Attributes.Contains("height"))
                        {
                            node.Attributes.Remove("height");
                        }

                        if (node.Attributes.Contains("style"))
                        {
                            node.Attributes.Remove("style");
                        }
                    }

                    return htmlDoc.DocumentNode.InnerHtml;
                }
            }

            return input.ToString();
        }

    }
}
