﻿namespace Nest.Web.Core.Domain
{
    public class DColorInfo
    {
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
    }
}
