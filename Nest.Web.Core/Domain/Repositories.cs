﻿using System.Collections.Generic;

namespace Nest.Web.Core.Domain
{
    public class DBusinessItem
    {
        public int NodeId { get; set; }
        public string BusinessType { get; set; }
    }

    public class DStartupCategoryItem
    {
        public int NodeId { get; set; }
        public string StartupCategoryType { get; set; }
    }

    public class DStartupItem
    {
        public DImageInfo ContentIcon { get; set; }

        public string ContentStartupName { get; set; }

        public string ContentDescription { get; set; }

        public List<string> ContentCategories { get; set; }

        public List<string> ContentBusinessType { get; set; }

        //public List<DButtonItem> ContentLink { get; set; }
        public string ContentWebsiteButtonLink { get; set; }
        public string ContentPhoneButtonLink { get; set; }
        public string ContentEmailButtonLink { get; set; }
    }
}
