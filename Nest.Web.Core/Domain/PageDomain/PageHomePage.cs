﻿using LeGU.Web.Core.PublishedContentModels;

namespace Nest.Web.Core.Domain.PageDomain
{
    public class PageHomePage
    {
        public HighlightSlider HighlightSlider_1 { get; set; }
        public ContentMediaDualImage ContentMediaDualImage_2 { get; set; }
        public InfoIcons InfoIcons_3 { get; set; }
        public SimpleHeading SimpleHeading_4 { get; set; }
        public RelatedPages RelatedPages_5 { get; set; }
        public CTablock CTABlock_6 { get; set; }
        public SimpleHeading SimpleHeading_7 { get; set; }
        public RelatedPages RelatedPages_8 { get; set; }
        public CTablock CTABlock_9 { get; set; }
    }
}
