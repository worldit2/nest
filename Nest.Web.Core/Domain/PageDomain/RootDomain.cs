﻿using Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure;

namespace Nest.Web.Core.Domain.PageDomain
{
    public class VueWebsiteData
    {
        public DMultiPageWebsite Index { get; set; }
        public DWebsitePage AboutUs { get; set; }
        public DWebsitePage Portfolio { get; set; }
        public DWebsitePage Challenges { get; set; }
        public DWebsitePage Resources { get; set; }
        public DWebsitePage Events { get; set; }
        public DWebsitePage PrivacyPolicy { get; set; }
        public DWebsitePage Contacts { get; set; }
        public DWebsitePage NewsletterSubscriptions { get; set; }
    }
}
