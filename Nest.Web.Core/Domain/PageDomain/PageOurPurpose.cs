﻿using LeGU.Web.Core.PublishedContentModels;

namespace Nest.Web.Core.Domain.PageDomain
{
    public class PageOurPurpose
    {
        public MainHeading MainHeading { get; set; }
        public WOT WOT { get; set; }
        public ContentMediaWhite ContentMediaWhite { get; set; }
        public ContentMediaDualImage ContentMediaDualImage { get; set; }
        public InfoIconsGroups InfoIconsGroups { get; set; }
        public CTablock CTABlock { get; set; }
    }
}
