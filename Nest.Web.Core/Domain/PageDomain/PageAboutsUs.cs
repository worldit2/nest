﻿using LeGU.Web.Core.PublishedContentModels;

namespace Nest.Web.Core.Domain.PageDomain
{
    public class PageAboutsUs
    {
        public MainHeading MainHeading_1 { get; set; }
        public ContentMediaBlack ContentMediaBlack_2 { get; set; }
        public SimpleHeading SimpleHeading_3 { get; set; }
        public InfoIcons InfoIcons_4 { get; set; }
        public SimpleHeading SimpleHeading_5 { get; set; }
        public CTablock CTablock_6 { get; set; }
        public SimpleHeading SimpleHeading_7 { get; set; }
        public RelatedPages RelatedPages_8 { get; set; }
        public CTablock CTaBlock_9 { get; set; }
    }
}
