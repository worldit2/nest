﻿using LeGU.Web.MP.Core.Helpers;

namespace Nest.Web.Core.Domain
{
    public class DYoutubeVideoInfo
    {
        public DYoutubeVideoInfo(string contentVideo)
        {
            YoutubeVideoCode = YouTubeHelper.GetYoutubeCodeFromUrl(contentVideo);
            YoutubeEmbedUrl = YouTubeHelper.GetYoutubeEmbedUrl(YouTubeHelper.GetYoutubeCodeFromUrl(contentVideo));
            VideoImage = YouTubeHelper.GetYoutubeVideoImage(YouTubeHelper.GetYoutubeCodeFromUrl(contentVideo));
        }

        public string YoutubeVideoCode { get; set; }
        public string YoutubeEmbedUrl { get; set; }
        public string VideoImage { get; set; }
    }
}
