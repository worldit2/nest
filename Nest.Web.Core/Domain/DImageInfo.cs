﻿namespace Nest.Web.Core.Domain
{
    public class DImageInfo
    {
        public string ImageSrc { get; set; }
        public string ImageAlt { get; set; }
        public string ImageTitle { get; set; }
    }
}
