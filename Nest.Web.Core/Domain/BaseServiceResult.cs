﻿using Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain
{
    public class BaseServiceResult
    {
        public bool Success { get; set; }
        public string MessageTitle { get; set; }
        public string Message { get; set; }
        public int InternalStatusCode { get; set; }
        //public List<ValidationResult> Errors { get; set; }
    }

    public class WebsiteDataFullResult: BaseServiceResult
    {
        public DMultiPageWebsite WebsiteData { get; set; }
    }

    public class WebsiteDataByRouteResult : BaseServiceResult
    {
        public DMultiPageWebsite WebsiteData { get; set; }
    }

    public class StartupSearchResult : BaseServiceResult
    {
        public List<DStartupItem> Startups { get; set; } = new List<DStartupItem>();
    }

    public class WebsiteLanguagesResult : BaseServiceResult
    {
        public List<string> IsoLanguages { get; set; } = new List<string>();
    }

    public class EventICSFileResult : BaseServiceResult
    {
        public string ICSXML { get; set; }
    }
}
