﻿using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Functional
{
    public class DStartupForm : DPageElement
    {
        public List<string> BusinessTypes { get; set; }
        public List<string> Categories { get; set; }
    }
}
