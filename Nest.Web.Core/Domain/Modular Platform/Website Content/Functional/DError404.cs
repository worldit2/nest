﻿namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Functional
{
    public class DError404 : DPageElement
    {
        public string ContentTitle { get; set; }
        public string ContentSubtitle { get; set; }
    }
}
