﻿using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Functional
{
    public class DShareBar : DPageElement
    {
        public string ContentText { get; set; }
        public List<DShareBarItem> SocialMediaItems { get; set; } = new List<DShareBarItem>();
    }
}
