﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content
{
    public class DContentMediaBlack : DPageElement
    {
        public string ContentTitleLabel { get; set; }
        public string ContentTitle { get; set; }
        public string ContentSubtitle { get; set; }
        public DImageInfo ContentImage { get; set; }
        public string ContentContent { get; set; }
        public List<DButtonItemStyledPrimarySecondary> ContentButtons { get; set; }
        public DColorInfo StyleColorTheme { get; set; }
    }
}
