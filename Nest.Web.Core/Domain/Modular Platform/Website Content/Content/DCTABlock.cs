﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content
{
    public class DCTablock : DPageElement
    {
        public string ContentTitle { get; set; }
        public string ContentSubtitle { get; set; }
        public List<DButtonItemStyledPrimarySecondary> ContentButtons { get; set; }
        public DColorInfo StyleBackgroundColor { get; set; }
        public DColorInfo StyleColorTheme { get; set; }
        public DImageInfo StyleBackgroundImage { get; set; }
        public DColorInfo StyleFontColor { get; set; }
    }
}
