﻿namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content
{
    public class DSimpleMedia : DPageElement
    {
        public DImageInfo ContentDesktopImage { get; set; }
        public DImageInfo ContentMobileImage { get; set; }
        public DYoutubeVideoInfo ContentVideo { get; set; }
        public string StyleMediaBackgroundSize { get; set; }
        public bool StyleEnableCrop { get; set; }
        public string StyleImageAlignment { get; set; }
        public DColorInfo StyleBackgroundColor { get; set; }
        public DColorInfo StyleColorTheme { get; set; }
    }
}
