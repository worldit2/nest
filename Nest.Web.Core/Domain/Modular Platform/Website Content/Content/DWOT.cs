﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content
{
    public class DWOT : DPageElement
    {
        public DColorInfo StyleColorTheme { get; set; }
        public List<DWOTItem> ContentItems { get; set; } = new List<DWOTItem>();
    }
}
