﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content
{
    public class DInfoIcons : DPageElement
    {
        public List<DInfoIconItem> ContentItems { get; set; }
    }
}
