﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content
{
    public class DInfoIconsGroups : DPageElement
    {
        public string ContentTitle { get; set; }
        public DColorInfo StyleInfoIconColor { get; set; }
        public bool StyleCentered { get; set; }
        public List<DInfoIconGroup> ContentGroups { get; set; }
    }
}
