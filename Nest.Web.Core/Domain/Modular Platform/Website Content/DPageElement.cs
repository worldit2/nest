﻿namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content
{
    public abstract class DPageElement
    {
        public string Name { get; set; }
        public string DocumentTypeAlias { get; set; }
    }
}
