﻿using Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Featured_And_Related
{
    public class DRelatedPages : DPageElement
    {
        //public string ContentTitle { get; set; }
        public List<DWebsitePage> ContentPages { get; set; }
        //Show Images
        //Show Description
    }
}
