﻿namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Headings
{
    public class DSimpleHeading : DPageElement
    {
        public string ContentTitle { get; set; }
        public string ContentSubtitle { get; set; }
        public DColorInfo StyleModuleColorTheme { get; set; }
        public string StyleHeadingIcon { get; set; }
    }
}
