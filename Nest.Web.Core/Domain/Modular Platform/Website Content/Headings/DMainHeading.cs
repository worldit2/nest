﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Headings
{
    public class DMainHeading : DPageElement
    {
        public string ContentTitleLabel { get; set; }
        public string ContentTitle { get; set; }
        public string ContentSubtitle { get; set; }
        public List<DButtonItemStyledPrimarySecondary> ContentButtons { get; set; } = new List<DButtonItemStyledPrimarySecondary>();
        public string ContentRegisterButtonLabel { get; set; }
        public DColorInfo StyleColorTheme { get; set; }
        public DImageInfo StyleBackgroundImage { get; set; }
    }
}
