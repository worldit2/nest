﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Content.Headings
{
    public class DHighlightSlider : DPageElement
    {
        public List<DSliderItem> ContentSliders { get; set; } = new List<DSliderItem>();
    }
}
