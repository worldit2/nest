﻿namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DInfoIconItem
    {
        public DImageInfo ContentIcon { get; set; }
        public string ContentTitle { get; set; }
        public string ContentDescription { get; set; }
    }
}
