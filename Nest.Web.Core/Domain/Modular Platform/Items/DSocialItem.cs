﻿namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DSocialItem
    {
        public string ContentSocialName { get; set; }
        public string ContentSocialLink { get; set; }
        public string ContentSocialIcon { get; set; }
    }
}
