﻿namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DWOTItem
    {
        public string ContentColumnTitle { get; set; }
        public string ContentBody { get; set; }
        public DColorInfo StyleColumnColor { get; set; }
    }
}
