﻿namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DButtonItem
    {
        public string ButtonLink { get; set; }
        public string ButtonTitle { get; set; }
        public string ButtonText { get; set; }
        
    }
}
