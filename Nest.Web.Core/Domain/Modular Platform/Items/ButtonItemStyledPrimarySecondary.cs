﻿namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DButtonItemStyledPrimarySecondary : DButtonItem
    {
        public string ButtonStyle { get; set; }
        public DColorInfo StyleButtonColor { get; set; }
    }
}
