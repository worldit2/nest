﻿using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DSliderItem
    {
        public string SliderTitleLabel { get; set; }
        public string SliderTitle { get; set; }
        public string SliderSubtitle { get; set; }
        public List<DButtonItemStyledPrimarySecondary> SliderButton { get; set; }
        public DImageInfo SliderBackgroundImage { get; set; }
        public DColorInfo SliderColorTheme { get; set; }
    }
}
