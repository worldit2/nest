﻿using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Items
{
    public class DInfoIconGroup
    {
        public string ContentTitle { get; set; }
        public List<DInfoIconItem> ContentItems { get; set; }
    }
}
