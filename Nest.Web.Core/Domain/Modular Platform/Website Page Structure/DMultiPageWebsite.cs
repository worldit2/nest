﻿using Nest.Web.Core.Domain.Modular_Platform.Items;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure
{
    public class DMultiPageWebsite : DAbstractPageWebsite
    {
        
        public List<DBusinessItem> BusinessTypes { get; set; }
        public List<DStartupCategoryItem> StartupCategoryTypes { get; set; }

        public string Name { get; set; }

        public List<DButtonItem> HeaderLinks { get; set; }

        public List<DButtonItem> FooterLinks { get; set; }

        #region Metatags
        //#region Titles
        //public string OverrideTitle { get; set; }
        //public string AltTitleBreadcrumb { get; set; }
        //public string AltTitleInternal { get; set; }
        //public string AltTitleMenu { get; set; }
        //#endregion

        //#region SEO
        public string SeoTitle { get; set; }
        public string SeoDescription { get; set; }
        //public List<string> SeoKeywords { get; set; }
        //public bool SeoNoIndex { get; set; }
        //public bool SeoNoFolow { get; set; }
        //#endregion

        //#region OpenGraph
        //public string OgTitle { get; set; }
        //public string OgDescription { get; set; }
        //public string OgType { get; set; }
        //public string OgImageSrc { get; set; }
        //public string OgImageAlt { get; set; }
        //public float OgImageWidth { get; set; }
        //public float OgImageHeight { get; set; }
        //#endregion

        //#region URLs
        //public string UmbracoUrlName { get; set; }
        //public string UmbracoUrlAlias { get; set; }
        //#endregion
        #endregion

        public IDictionary<string, DPageElement> PageElements { get; set; } = new Dictionary<string, DPageElement>();
        public IDictionary<string, DWebsitePage> WebsitePages { get; set; } = new Dictionary<string, DWebsitePage>();
        public List<string> RoutePaths { get; set; } = new List<string>();
    }
}
