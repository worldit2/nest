﻿using Nest.Web.Core.Domain.Modular_Platform.Website_Content;
using System;
using System.Collections.Generic;

namespace Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure
{
    public class DWebsitePage : DAbstractPageWebsite
    {
        public string PagDetailName { get; set; }
        public string contentDateTextt { get; set; }

        public string Url { get; set; } 
        public string UrlName { get; set; }

        #region Page Definition
        public string PageDefinitionTitle { get; set; }
        public string PageDefinitionDescription { get; set; }
        public DImageInfo PageDefinitionImage { get; set; }
        public DColorInfo StyleColorTheme { get; set; }
        #endregion

        #region Event Info
        //public string RegisterButtonLabel { get; set; } = "Register";
        public string DateText { get; set; }
        public string TimeText { get; set; }
        public string ContentLocation { get; set; }
        public bool ShowAddToCalenderButton { get; set; }
        public string ICSCalenderLink { get; set; }
        public string GmailCalenderLink { get; set; }
        #endregion

        #region Commented Metatags
        //#region Titles
        //public string OverrideTitle { get; set; }
        //public string AltTitleBreadcrumb { get; set; }
        //public string AltTitleInternal { get; set; }
        //public string AltTitleMenu { get; set; }
        //#endregion

        //#region SEO
        //public string SeoTitle { get; set; }
        //public string SeoDescription { get; set; }
        //public List<string> SeoKeywords { get; set; }
        //public bool SeoNoIndex { get; set; }
        //public bool SeoNoFollow { get; set; }
        //#endregion

        //#region OpenGraph
        //public string OgTitle { get; set; }
        //public string OgDescription { get; set; }
        //public string OgType { get; set; }
        //public DImageInfo OgImageSrc { get; set; }
        //public string OgImageWidth { get; set; }
        //public string ogImageHeight { get; set; }
        //#endregion

        //#region Redirect
        //public string RedirectDestination { get; set; }
        //#endregion

        //#region URLs
        //public string UmbracoUrlName { get; set; }
        //public string UmbracoUrlAlias { get; set; }
        //#endregion
        #endregion

        //public DPageElements PageElements { get; set; }
        public IDictionary<string, DPageElement> PageElements { get; set; } = new Dictionary<string, DPageElement>();

        //New Code
        public IDictionary<string, DWebsitePage> WebsitePages { get; set; } = new Dictionary<string, DWebsitePage>();

        public List<string> RoutePaths { get; set; } = new List<string>();
    }
}
