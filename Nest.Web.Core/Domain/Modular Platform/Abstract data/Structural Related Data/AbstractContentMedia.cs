﻿namespace Nest.Web.Core.Domain.Modular_Platform.Abstract_data.Structural_Related_Data
{
    public abstract class AbstractContentMedia
    {
        public string ContentTitle { get; set; }
        public string ContentContent { get; set; }
        //public List<DButtonItem> ContentButtons { get; set; } = new List<DButtonItem>();
    }
}
