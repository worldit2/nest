﻿using Nest.Web.Core.Helpers;
namespace LeGU.Web.Core.PublishedContentModels
{
    public partial class StartupCategoryItem
    {
        public string NameVorto => this.GetExistingVortoValue<string>("contentName");
        public string NameVortoLang(string lang) => this.GetExistingVortoValue<string>("contentName", lang);
    }
}
