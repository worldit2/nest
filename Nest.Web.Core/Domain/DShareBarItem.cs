﻿namespace Nest.Web.Core.Domain
{
    public class DShareBarItem
    {
        public string Url { get; set; }
        public string SocialMediaType { get; set; }
    }
}
