﻿namespace Nest.Web.Core.Domain
{
    public class WebsiteConfigurationsResult : BaseServiceResult
    {
        public WebsiteConfigurations_ WebsiteConfigurations { get; set; }//TODO changes this to your own class
    }

    public class WebsiteConfigurations_
    {
        #region Logo and Site Name
        public string SiteName { get; set; }
        public string SiteDescription { get; set; }
        public bool AddSuffix { get; set; }
        //public string DefaultLogo { get; set; }
        //public string AlternativeLogo { get; set; }
        //public string Favicon { get; set; }
        public bool GenerateAppleTouchIcons { get; set; }
        public bool GenerateMicrosoftTiles { get; set; }
        public string MicrosoftTileBackgroundColor { get; set; }
        #endregion

        #region SEO and OpenGraph
        public string DefaultMetadataTitleSeparator { get; set; }
        public bool UseTitleFromSEOIfOGMetaTitleIsEmpty { get; set; }
        public bool UseDescriptionFromSEOIfOGMetaDescriptionIsEmpty { get; set; }
        //public string defaultOGImage { get; set; }
        public string defaultOGType { get; set; }
        #endregion

        #region Navigation Menu
        #endregion

        #region Navigation Footer
        #endregion

        //Analytics

        #region Social Links
        #endregion

        //scripts

    }
}
