﻿using LeGU.Web.MP.Core.GlobalSettings;
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace LeGU.Web.MP.Core.Objects.CSP
{
    public sealed class NonceFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext) { }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            LeGUConfigurations _configurations = ConfigurationManager.GetSection("LeGUConfigurations") as LeGUConfigurations;

            string _urlAbsolutePath = filterContext.HttpContext.Request.Url.AbsolutePath;

            string _cspTemplateForRequest = _urlAbsolutePath.StartsWith("/umbraco/") || _urlAbsolutePath.StartsWith("/install/") ?
                _configurations.ContentSecurityPolicySettings.Get("CSPTemplateUmbraco").Value :
                _configurations.ContentSecurityPolicySettings.Get("CSPTemplateWebsite").Value;

            var context = filterContext.HttpContext.GetOwinContext();
            var rng = new RNGCryptoServiceProvider();
            var nonceBytes = new byte[32];
            rng.GetBytes(nonceBytes);
            var nonce = Convert.ToBase64String(nonceBytes);
            context.Set("ScriptNonce", nonce);
            context.Response.Headers.Add("Content-Security-Policy",
                new[] { string.Format(_cspTemplateForRequest, nonce) });
        }
    }
}
