﻿using System.Web.Mvc;

namespace LeGU.Web.MP.Core.Objects.CSP
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new NonceFilter());
        }
    }
}
