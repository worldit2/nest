﻿using AutoMapper;
using LeGU.Web.Core.PublishedContentModels;
using log4net;
using Nest.Web.Core.DAL;
using Nest.Web.Core.Domain;
using Nest.Web.Core.Helpers;
using Nest.Web.Core.Objects.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Our.Umbraco.Vorto.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace Nest.Web.Core.Managers
{
    public class StartupManager
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(StartupManager));

        public BaseServiceResult RegisterStartup(StartupInfoModel startupModel)
        {
            BaseServiceResult result = new BaseServiceResult();
            startupModel.Validate();
            StartupDatabase startup;
            StartupCategoryDatabase startupCategory;
            var db = ApplicationContext.Current.DatabaseContext.Database;
            startup = db.Fetch<StartupDatabase>(string.Format("select TOP 1 * from Startup where StartupName='{0}'", startupModel.StartupName)).FirstOrDefault();
            if (startup == null)
            {
                startup = new StartupDatabase();
                startup.Id = Guid.NewGuid();
                startup.StartupName = startupModel.StartupName;
                startup.WebsiteName = startupModel.WebsiteName;
                startup.Email = startupModel.Email;
                startup.ContactNumber = startupModel.ContactNumber;
                startup.BusinessType = startupModel.BusinessType;
                startup.Categories = startupModel.Categories;
                startup.StartupDescription = startupModel.StartupDescription;
                startup.IsStartupUpdate = startupModel.IsStartupUpdate;
                startup.ExistingStartupName = startupModel.ExistingStartupName;
                startup.Authorize = startupModel.Authorize;
                startup.DateCreatedUTC = DateTime.UtcNow;
                startup.DateUpdatedUTC = DateTime.UtcNow;
                startup.IsoCode = startupModel.IsoCode;
                db.Insert(startup);
                if (startupModel.OtherCategories != null && startupModel.OtherCategories.Any())
                {
                    foreach (string category in startupModel.OtherCategories)
                    {
                        startupCategory = new StartupCategoryDatabase();
                        startupCategory.StartupId = startup.Id;
                        startupCategory.CategoryName = category;
                        db.Insert(startupCategory);
                    }
                }
                result.Success = true;
                result.Message = startup.Id.ToString();
            }
            else
            {
                result.Message = "Duplicate Startup Name";
                result.Success = false;
            }
            return result;
        }

        public BaseServiceResult RegisterStartupFile(Guid guid, string filePath)
        {
            BaseServiceResult result = new BaseServiceResult();
            StartupFileDatabase newStartupFile = new StartupFileDatabase();
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var node = db.Fetch<StartupDatabase>(string.Format("select TOP 1 * from StartupFile where FilePath='{0}'", filePath)).FirstOrDefault();
            if (node == null)
            {
                newStartupFile.StartupId = guid;
                newStartupFile.FilePath = filePath;
                db.Insert(newStartupFile);
                result.Success = true;
                result.Message = string.Empty;
            }
            else
            {
                result.Message = "Duplicate File";
                result.Success = false;
            }
            return result;
        }

        public BaseServiceResult SendStartupToUmbracoRepository(Guid id)
        {
            try
            {
                //check if the startup with the respective id exists
                var db = ApplicationContext.Current.DatabaseContext.Database;
                bool startupExists = db.Fetch<StartupDatabase>("WHERE Id=@0", id).Any();

                if (startupExists)
                {
                    //get the startup info from the database
                    var startupSubmitInfo = db.SingleOrDefault<StartupDatabase>("WHERE Id=@0", id);

                    var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

                    //getall the business types from umbraco content
                    var startupRepository = (StartupRepository)umbracoHelper.TypedContentSingleAtXPath("//startupRepository");

                    var businessRepository = (BusinessRepository)umbracoHelper.TypedContentSingleAtXPath("//businessRepository");
                    var businessItems = businessRepository.Children<BusinessItem>();

                    var startupCategoryRepository = (StartupCategoryRepository)umbracoHelper.TypedContentSingleAtXPath("//startupCategoryRepository");
                    var categoryItems = startupCategoryRepository.Children<StartupCategoryItem>();
                    //getall the category types from umbraco content

                    var contentService = ApplicationContext.Current.Services.ContentService;
                    var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
                    var mediaService = ApplicationContext.Current.Services.MediaService;
                    var memberService = ApplicationContext.Current.Services.MediaService;

                    #region insert the image in the umbraco media

                    //check if the startup was a image
                    IMedia mediaImage = null;
                    if (db.Fetch<StartupFileDatabase>("WHERE StartupId = @0", id).Any())
                    {
                        var dbFileInfo = db.Fetch<StartupFileDatabase>("WHERE StartupId = @0", id).First();

                        //check if folder exists
                        IMedia folder;

                        if (mediaService.GetRootMedia().Any(a => a.Name == "StartupLogos"))
                        {
                            folder = mediaService.GetRootMedia().First(a => a.Name == "StartupLogos");
                        }
                        else
                        {
                            //Create folder
                            folder = mediaService.CreateMedia("StartupLogos", Constants.System.Root, Constants.Conventions.MediaTypes.Folder);
                            mediaService.Save(folder);
                        }

                        //Add the image

                        using (Stream stream = System.IO.File.OpenRead(dbFileInfo.FilePath))
                        {
                            // Initialize a new image at the root of the media archive
                            mediaImage = mediaService.CreateMedia(startupSubmitInfo.StartupName, folder/*Constants.System.Root*/, Constants.Conventions.MediaTypes.Image, UmbracoContext.Current.Security.CurrentUser.Id);

                            // Set the property value (Umbraco will handle the underlying magic)
                            mediaImage.SetValue("umbracoFile", Path.GetFileName(dbFileInfo.FilePath), stream);

                            // Save the media
                            mediaService.Save(mediaImage, UmbracoContext.Current.Security.CurrentUser.Id);
                        }
                    }
                    #endregion

                    #region insert the startup in the umbraco content tree
                    var startupItem = contentService.CreateContent(startupSubmitInfo.StartupName, startupRepository.Id, StartupItem.ModelTypeAlias, UmbracoContext.Current.Security.CurrentUser.Id);

                    #region contentBusinessType
                    Guid? umbBusNodeId = null;
                    if (businessItems.Any(a => a.GetExistingVortoValue<string>("contentName", startupSubmitInfo.IsoCode ?? "en-US") == startupSubmitInfo.BusinessType))
                    {
                        umbBusNodeId = businessItems.FirstOrDefault(a => a.GetExistingVortoValue<string>("contentName", startupSubmitInfo.IsoCode ?? "en-US") == startupSubmitInfo.BusinessType).GetKey();
                    }
                    #endregion

                    #region contentCategories
                    //for each categorie in the database check if that same category exists in the umbraco content tree
                    //and retrieve the respective ids of the content tree of the startup categories
                    var umbCatNodeIds = new List<Guid>();
                    foreach (var startupCategorySubmited in startupSubmitInfo.Categories.Split(','))
                    {
                        if (categoryItems.Any(a => a.GetExistingVortoValue<string>("contentName", startupSubmitInfo.IsoCode ?? "en-US") == startupCategorySubmited))
                        {
                            umbCatNodeIds.Add(categoryItems.FirstOrDefault(a => a.GetExistingVortoValue<string>("contentName", startupSubmitInfo.IsoCode ?? "en-US") == startupCategorySubmited).GetKey());
                        }
                    }
                    #endregion


                    if (mediaImage != null)
                    {
                        var locaUdi = Udi.Create(Constants.UdiEntityType.Media, mediaImage.Key);

                        startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentIcon).PropertyTypeAlias, locaUdi.ToString());
                    }

                    //StartupName
                    VortoValue vortoStartupName = new VortoValue();
                    vortoStartupName.Values = new Dictionary<string, object>();
                    vortoStartupName.Values.Add("en-US", (object)startupSubmitInfo.StartupName);
                    startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentStartupName).PropertyTypeAlias, JsonConvert.SerializeObject(vortoStartupName));

                    //StartupCategory
                    VortoValue vortoStartupDescription = new VortoValue();
                    vortoStartupDescription.Values = new Dictionary<string, object>();
                    vortoStartupDescription.Values.Add("en-US", (object)startupSubmitInfo.StartupDescription);
                    startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentDescription).PropertyTypeAlias, JsonConvert.SerializeObject(vortoStartupDescription));

                    if (umbBusNodeId != null)
                    {
                        var categoriesValueToSet = $"umb://document/{umbBusNodeId.ToString().Replace("-", "")}";
                        startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentBusinessType).PropertyTypeAlias, categoriesValueToSet);
                    }

                    if (umbCatNodeIds.Any())
                    {
                        var categoriesValueToSet = string.Join(",", umbCatNodeIds.Select(a => "umb://document/" + a.ToString().Replace("-", "")));
                        startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentCategories).PropertyTypeAlias, categoriesValueToSet);
                    }
                    
                    if(startupSubmitInfo.WebsiteName != null)
                    {
                        if (startupSubmitInfo.WebsiteName.StartsWith("http"))
                        {
                            startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentWebsiteButtonLink).PropertyTypeAlias, startupSubmitInfo.WebsiteName);
                        }
                        else
                        {
                            startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentWebsiteButtonLink).PropertyTypeAlias, $"https://{startupSubmitInfo.WebsiteName}");
                        }
                    }

                    startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentEmailButtonLink).PropertyTypeAlias, startupSubmitInfo.Email);
                    startupItem.SetValue(StartupItem.GetModelPropertyType(a => a.ContentPhoneButtonLink).PropertyTypeAlias, startupSubmitInfo.ContactNumber);

                    var attempt = contentService.SaveAndPublishWithStatus(startupItem, UmbracoContext.Current.Security.CurrentUser.Id);

                    startupSubmitInfo.NodeId = attempt.Result.Entity.Id;

                    ;
                    #endregion

                    //save the node Id in the database
                    db.Update(startupSubmitInfo);

                    return new BaseServiceResult { Success = true, Message = "Startup validated.", InternalStatusCode = 200 };
                }
                else
                {
                    return new BaseServiceResult { Success = false, Message = "Startup with the respective id does not exist.", InternalStatusCode = 400 };
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return new BaseServiceResult { Success = false, Message = "Error while validating startup.", InternalStatusCode = 500 };
            }
        }

        private int GetMediaItemId(string folderName)
        {
            var mediaService = UmbracoContext.Current.Application.Services.MediaService;
            return mediaService.GetRootMedia().FirstOrDefault(m => m.Name.InvariantEquals(folderName)).Id;
        }

        public BaseServiceResult GetStartups(string lang)
        {
            try
            {
                //check if the language asked exists in umbraco
                var languages = ApplicationContext.Current.Services.LocalizationService.GetAllLanguages();
                if (languages.Any(a => a.IsoCode == lang))
                {
                    var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                    var startupRepo = (StartupRepository)umbracoHelper.TypedContentSingleAtXPath("//startupRepository");

                    var umbracoStartupItems = startupRepo.Children<StartupItem>();

                    List<DStartupItem> startups = new List<DStartupItem>();

                    //for each startup
                    foreach (var startup in umbracoStartupItems)
                    {
                        List<string> businessItems;
                        List<string> categoryItems;

                        businessItems = startup.ContentBusinessType.Cast<PublishedContentModel>().Select(a => a.GetExistingVortoValue<string>("contentName", lang)).ToList();
                        categoryItems = startup.ContentCategories.Cast<PublishedContentModel>().Select(a => a.GetExistingVortoValue<string>("contentName", lang)).ToList();

                        startups.Add(new DStartupItem
                        {
                            ContentIcon = Mapper.Map<DImageInfo>(startup.ContentIcon),
                            ContentStartupName = startup.GetExistingVortoValue<string>("contentStartupName", lang),
                            ContentDescription = startup.GetExistingVortoValue<string>("contentDescription", lang),
                            ContentBusinessType = businessItems,
                            ContentCategories = categoryItems,
                            ContentWebsiteButtonLink = startup.ContentWebsiteButtonLink,
                            ContentEmailButtonLink = startup.ContentEmailButtonLink,
                            ContentPhoneButtonLink = startup.ContentPhoneButtonLink
                            //ContentLink = startup.ContentLink.Select(a => Mapper.Map<DButtonItem>(a)).ToList()
                        });
                    }

                    return new StartupSearchResult { Success = true, Message = "Ok", Startups = startups, InternalStatusCode = 200 };
                }
                else
                {
                    return new BaseServiceResult { Success = false, Message = "Language does not exists.", InternalStatusCode = 400 };
                }
            }
            catch (Exception e)
            {
                Log.Error("Error submitting startup", e);
                return new BaseServiceResult { Success = false, Message = "Error while getting the list of startups", InternalStatusCode = 500 };
            }
        }

        public BaseServiceResult CheckIfStartupIsValidated(Guid guid)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var startupExists = db.Fetch<StartupDatabase>("WHERE Id = @0", guid).Any();

            //check if startup with respective id exists
            if (startupExists)
            {
                //check if startup is validated
                var StartupRepoNodeId = db.Fetch<StartupDatabase>("WHERE Id = @0", guid).Single().NodeId;

                //check if node id exists for the respective startup
                if (StartupRepoNodeId.HasValue)
                {
                    var startupRepoItem = ApplicationContext.Current.Services.ContentService.GetById(StartupRepoNodeId.Value);

                    return new BaseServiceResult { Success = true, Message = "Startup is validated and is in the content." };
                }
                else
                {
                    return new BaseServiceResult { Success = false, Message = "Startup was not yet submited to umbraco content." };
                }
            }
            else
            {
                return new BaseServiceResult { Success = false, Message = "Startup with the respective Id does not exist." };
            }
        }

        public bool CheckIfStartupIsValidated(int nodeId)
        {
            var Startup = ApplicationContext.Current.Services.ContentService.GetById(nodeId);
            
            if (Startup != null && !Startup.Trashed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
