﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Umbraco.Core.IO;
using Umbraco.Web;

namespace Nest.Web.Core.Managers
{
    public class UiomaticExport
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(UiomaticExport));

        private const string DefaultSheetName = "Sheet1";

        private const string ReportFolderPath = @"~\Reports";

        public ExportResult CreateExcelDocBytes(string typeAlias)
        {
            try
            {
                var os = new UIOMatic.Services.PetaPocoObjectService();
                var entitiesToExport = os.GetAll(UIOMatic.Helper.GetUIOMaticTypeByAlias(typeAlias));

                var entity = entitiesToExport.First();
                var properties = entity.GetType().GetProperties();
                var entityFields = properties.Where(a=> !a.Name.EndsWith("_Ex")).ToList();

                if (entityFields == null || entityFields.Count() == 0)
                {
                    throw new Exception("no columns to export");
                }
                else
                {
                    var id = Guid.NewGuid();
                    var currentUmbracoUser = UmbracoContext.Current.Security.CurrentUser.Name;
                    var fileName = $"{id}_{typeAlias}_{currentUmbracoUser}.xlsx";
                    var filePath = IOHelper.MapPath($@"{ReportFolderPath}\{fileName}");
                    SpreadsheetDocument document = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook);

                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = DefaultSheetName };

                    sheets.Append(sheet);
                    workbookPart.Workbook.Save();

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

                    #region Header
                    // Constructing header
                    Row row = new Row();

                    foreach (var field in entityFields)
                    {
                        row.Append(ConstructCell(field.Name, CellValues.String));
                    }

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);
                    #endregion

                    #region Rows
                    // Inserting each row
                    foreach (object ent in entitiesToExport)
                    {
                        row = new Row();
                        row.Append(CreateSheetRow(ent, entityFields));
                        sheetData.AppendChild(row);
                    }
                    #endregion
                    workbookPart.Workbook.Save();

                    document.Close();

                    return new ExportResult { data = $"../Reports/{fileName}" };
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Failed to export data to Excel format (returning empty file). REASON: {0}", ex);
                return null;
            }
        }

        public class ExportResult
        {
            public string data { get; set; }
        }

        private static IEnumerable<OpenXmlElement> CreateSheetRow(object ent, IEnumerable<PropertyInfo> entityFields)
        {
            var Result = new List<OpenXmlElement>();

            foreach (var prop in entityFields)
            {
                var value = prop.GetValue(ent);
                Result.Add(ConstructCell(value == null ? string.Empty : value.ToString(), CellValues.String));
            }

            return Result;
        }

        private static Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }
    }
}
