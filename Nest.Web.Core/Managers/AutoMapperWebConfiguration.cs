﻿using AutoMapper;
using LeGU.Web.Core.PublishedContentModels;
using Nest.Web.Core.Domain;
using Nest.Web.Core.Domain.Modular_Platform.Items;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content.Content;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content.Featured_And_Related;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content.Functional;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content.Headings;
using Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure;
using Nest.Web.Core.Helpers;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Web;
using static Umbraco.Core.PropertyEditors.ValueConverters.ColorPickerValueConverter;

namespace Nest.Web.Core.Managers
{
    public class AutoMapperWebConfiguration
    {
        public void Configure()
        {
            string virtualPath = HttpRuntime.AppDomainAppVirtualPath;

            Mapper.CreateMap<MultiPageWebsite, DMultiPageWebsite>();

            #region WebsitePages
            Mapper.CreateMap<WebsitePage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<AboutUsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<OurPurposePage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<ChallengesPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<ResourcesPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<EventsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<NewsletterSubscriptionsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<ContactsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<PrivacyPolicyPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<JoinUsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );
            Mapper.CreateMap<PortfolioPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                );

            #region Details Pages
            Mapper.CreateMap<ChallengeDetailsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                )
                .ForMember(
                    destinationMember => destinationMember.PagDetailName,
                    opts => opts.MapFrom(src => "CHALLENGE")
                );
            Mapper.CreateMap<ResourceDetailsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                )
                .ForMember(
                    destinationMember => destinationMember.PagDetailName,
                    opts => opts.MapFrom(src => "RESOURCE")
                );

            Mapper.CreateMap<EventDetailsPage, DWebsitePage>()
                .ForMember(
                    destinationMember => destinationMember.Url,
                    opts => opts.MapFrom(src => src.Url != null ? ((src.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.Url.Replace(virtualPath, "") : src.Url) : "#")
                )
                .ForMember(
                    destinationMember => destinationMember.PagDetailName,
                    opts => opts.MapFrom(src => "EVENT")
                )
            #region Add to Calender Events
                .ForMember(
                    destinationMember => destinationMember.DateText,
                    opts => opts.MapFrom(src =>
                        src.ContentEventBeginDateTime.GetDateWithSuffixFormat(src.GetCulture(null), (src.ContentEventEndDateTime.Equals(DateTime.MinValue) ? null : src.ContentEventEndDateTime as DateTime?))
                    )
                )
                .ForMember(
                    destinationMember => destinationMember.TimeText,
                    opts => opts.MapFrom(src =>
                        src.ContentEventBeginDateTime.Day == src.ContentEventEndDateTime.Day ? $"{src.ContentEventBeginDateTime.ToString("HH:mm")} - {src.ContentEventEndDateTime.ToString("HH:mm")}" : null
                    )
                )
                .ForMember(
                    destinationMember => destinationMember.ShowAddToCalenderButton,
                    opts => opts.MapFrom(src => src.ContentEventEndDateTime <= DateTime.Now)
                )
                .ForMember(
                    destinationMember => destinationMember.GmailCalenderLink,
                    opts => opts.MapFrom(src =>
                        string.Format(
                                "http://www.google.com/calendar/event?action=TEMPLATE&dates={0}/{1}&text={2}&location={3}&details={4}",
                                src.ContentEventBeginDateTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                                (src.ContentEventEndDateTime != DateTime.MinValue ? src.ContentEventEndDateTime : src.ContentEventBeginDateTime.Date.AddDays(1).AddTicks(-1)).ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                                HttpUtility.UrlEncode(src.AddToCalenderEventSubject),
                                HttpUtility.UrlEncode(src.ContentLocation),
                                src.AddToCalenderEventBodyDescription != null ? HttpUtility.UrlEncode(Regex.Replace((string)src.AddToCalenderEventBodyDescription, @"<(.|\n)*?>", string.Empty)) : ""
                        )
                    )
                )
                .ForMember(
                    destinationMember => destinationMember.ICSCalenderLink,
                    opts => opts.MapFrom(src => $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}{HttpContext.Current.Request.ApplicationPath.TrimEnd('/')}/umbraco/api/Events/GetICSFile?nodeId={src.Id}")
                )
            #endregion

                ;
            #endregion

            #endregion

            Mapper.CreateMap<Image, DImageInfo>()
                .ForMember(
                    destinationMember => destinationMember.ImageSrc,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Url)
                )
                .ForMember(
                    destinationMember => destinationMember.ImageTitle,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Name)
                );

            Mapper.CreateMap<File, DImageInfo>()
                .ForMember(
                    destinationMember => destinationMember.ImageSrc,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.UmbracoFile)
                )
                .ForMember(
                    destinationMember => destinationMember.ImageTitle,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Name)
                );

            Mapper.CreateMap<PickedColor, DColorInfo>()
                .ForMember(
                    destinationMember => destinationMember.ColorName,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Label)
                )
                .ForMember(
                    destinationMember => destinationMember.ColorCode,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Color)
                );



            #region Items

            Mapper.CreateMap<ButtonItem, DButtonItem>()
                .ForMember(
                    destinationMember => destinationMember.ButtonTitle,
                    opts => opts.MapFrom(src => src.ButtonLink.Name)
                )
                .ForMember(
                    destinationMember => destinationMember.ButtonLink,
                    opts => opts.MapFrom(src =>
                    src.ButtonLink != null ? ((src.ButtonLink.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.ButtonLink.Url.Replace(virtualPath, "") : src.ButtonLink.Url) : "#"
                    )
                );
            Mapper.CreateMap<ButtonItemStyledPrimarySecondary, DButtonItemStyledPrimarySecondary>()
                .ForMember(
                    destinationMember => destinationMember.ButtonTitle,
                    opts => opts.MapFrom(src => src.ButtonLink.Name)
                )
                .ForMember(
                    destinationMember => destinationMember.ButtonLink,
                    opts => opts.MapFrom(src =>
                    src.ButtonLink != null ? ((src.ButtonLink.Url.StartsWith(virtualPath) && virtualPath != "/") ? src.ButtonLink.Url.Replace(virtualPath, "") : src.ButtonLink.Url) : "#"
                    )
                );

            Mapper.CreateMap<InfoIconItem, DInfoIconItem>();
            Mapper.CreateMap<InfoIconGroup, DInfoIconGroup>();
            Mapper.CreateMap<SliderItem, DSliderItem>();
            Mapper.CreateMap<SocialItem, DSocialItem>();
            Mapper.CreateMap<WOtitem, DWOTItem>();
            #endregion

            #region Repositories
            Mapper.CreateMap<StartupItem, DStartupItem>()
                .ForMember(
                    destinationMember => destinationMember.ContentStartupName,
                    opt => opt.ResolveUsing(resolver => resolver.Context.Options.Items["lang"])
                    //opt => opt.MapFrom(src => src.GetExistingVortoValue<string>("ContentStartupName", opt.ResolveUsing(resolver => resolver.Context.Options.Items["lang"]))
                );
            Mapper.CreateMap<BusinessItem, DBusinessItem>();
            Mapper.CreateMap<StartupCategoryItem, DStartupCategoryItem>();
            #endregion

            #region Website Content

            #region Content
            Mapper.CreateMap<ContentMediaDualImage, DContentMediaDualImage>();
            Mapper.CreateMap<ContentMediaWhite, DContentMediaWhite>();
            Mapper.CreateMap<ContentMediaBlack, DContentMediaBlack>();
            Mapper.CreateMap<CTablock, DCTablock>();

            Mapper.CreateMap<InfoIcons, DInfoIcons>();
            Mapper.CreateMap<InfoIconsGroups, DInfoIconsGroups>();
            Mapper.CreateMap<SimpleMedia, DSimpleMedia>()
                //TODO 
                //.ForMember(
                //    destinationMember => destinationMember.ContentDesktopImage,
                //    memberOptions => memberOptions
                //    .Condition(src => src.InstanceCache.First())
                //    //.MapFrom(src => destinationMember.ContentDesktopImage != null ?)
                //)
                .ForMember(
                    destinationMember => destinationMember.ContentVideo,
                    memberOptions => memberOptions.MapFrom(src => new DYoutubeVideoInfo(src.ContentVideo))
                )
                ;
            Mapper.CreateMap<WOT, DWOT>();
            #endregion

            #region Featured And Related
            Mapper.CreateMap<RelatedPages, DRelatedPages>();
            #endregion

            #region Functional
            Mapper.CreateMap<Error404, DError404>();
            Mapper.CreateMap<ReusablePlaceholder, DReusablePlaceholder>();
            Mapper.CreateMap<ShareBar, DShareBar>();
            Mapper.CreateMap<NewsletterForm, DNewsletterForm>();
            Mapper.CreateMap<StartupForm, DStartupForm>();
            #endregion

            #region Headings
            Mapper.CreateMap<SimpleHeading, DSimpleHeading>();
            Mapper.CreateMap<HighlightSlider, DHighlightSlider>();
            Mapper.CreateMap<MainHeading, DMainHeading>();
            #endregion

            #endregion
        }
    }

    public class GmailCalenderLinkGenerator
    {
        public string Gen(DateTime startDate, DateTime? endDate, string subject, string location, string bodyDescription)
        {
            //DateTime.MinValue

            var googleCalUrl = string.Format(
                "http://www.google.com/calendar/event?action=TEMPLATE&dates={0}/{1}&text={2}&location={3}&details={4}",
                startDate.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                (endDate ?? startDate).ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                subject,
                bodyDescription
            );

            return googleCalUrl;
        }
    }


}
