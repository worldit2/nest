﻿namespace LeGU.Web.MP.Core.Objects.Models
{
    public class PageMeta
    {
        public string SiteName { get; set; }
        public bool AddSiteNameSuffix { get; set; }
        public string FaviconMediaId { get; set; }
        public bool GenerateAppleTouch { get; set; }
        public bool GenerateMicrosoftTile { get; set; }
        public string MicrosoftTileBackgroundColor { get; set; }
        public string TitleOverride { get; set; }
        public string SEOTitle { get; set; }
        public string SEODescription { get; set; }
        public string SEOKeywords { get; set; }
        public bool SEONoIndex { get; set; }
        public bool SEONoFollow { get; set; }
        public bool UseTitleFromSEOIfOGMetaTitleIsEmpty { get; set; }
        public bool UseDescriptionFromSEOIfOGMetaDescriptionIsEmpty { get; set; }
        public string OGTitle { get; set; }
        public string OGDescription { get; set; }
        public string OGType { get; set; }
        public string OGImageMediaId { get; set; }

        public string OGImageWidth { get; set; }

        public string OGImageHeight { get; set; }
    }
}
