﻿using AutoMapper;
using LeGU.Web.Core.PublishedContentModels;
using LeGU.Web.MP.Core.GlobalSettings;
using log4net;
using Nest.Web.Core.Domain;
using Nest.Web.Core.Domain.Modular_Platform.Items;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content;
using Nest.Web.Core.Domain.Modular_Platform.Website_Content.Functional;
using Nest.Web.Core.Domain.Modular_Platform.Website_Page_Structure;
using Nest.Web.Core.UmbracoExtensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace Nest.Web.Core.Managers
{
    public class ContentManager
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(ContentManager));

        private List<string> PageElementsNames = new List<string> {
            "pageElements",
            "challengeDetailPageElements",
            "eventDetailPageElements",
            "resourceDetailsPageElements",
            "aboutUsPageElements",
            "challengesPageElements",
            "contactUsPageElements",
            "eventsPageElements",
            "homepagePageElements",
            "joinUsPageElements",
            "newsletterSubscriptionsPageElements",
            "ourPurposePageElements",
            "portfolioPageElements",
            "privacyPolicyPageElements",
            "resourcesPageElements",
            "subscribePageElements"
        };

        private List<string> WebsitePageNames = new List<string> {
            "websitePage",
            "challengeDetailsPage",
            "eventDetailsPage",
            "ourPurposePage",
            "resourceDetailsPage",
            "aboutUsPage",
            "challengesPage",
            "contactsPage",
            "eventsPage",
            "joinUsPage",
            "newsletterSubscriptionsPage",
            "portfolioPage",
            "privacyPolicyPage",
            "resourcesPage"
        };

        public BaseServiceResult GetWebsiteLanguages()
        {
            try
            {
                return new WebsiteLanguagesResult
                {
                    Success = true,
                    Message = "Ok",
                    IsoLanguages = ApplicationContext.Current.Services.LocalizationService.GetAllLanguages().Select(a => a.IsoCode).ToList(),
                    InternalStatusCode = 200
                };
            }
            catch (Exception e)
            {
                Log.Error($"Error in GetWebsiteLanguages: {e.ToString()}");
                return new BaseServiceResult { Success = false, Message = "Error while getting the website languages", InternalStatusCode = 500 };
            }
            
        }

        public DAbstractPageWebsite GetElementByRoute(string routeInfo)
        {
            try
            {
                if (routeInfo != "/")
                {
                    var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

                    

                    var configSettings = (MultiPageWebsite)umbracoHelper.TypedContentSingleAtXPath("//multiPageWebsite");
                    var globalConfig = configSettings.Children<ConfigurationFolder>().Single().Children<GlobalConfigurationSettings>().Single();

                    var websitePage = UmbracoContext.Current.ContentCache.GetByRoute(routeInfo) as WebsitePage;

                    return WebsitePage(websitePage, globalConfig, false);
                }
                else
                {
                    return GetWebsitePages(false);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return null; ;
            }
        }

        public BaseServiceResult GetWebsiteAllData(string isoLangCode = null)
        {
            try
            {
                DMultiPageWebsite websiteData;

                if (string.IsNullOrWhiteSpace(isoLangCode))
                {
                    isoLangCode = "en-US";
                }

                var cachedWebsiteData = HttpRuntime.Cache.Get($"WebsiteData_{isoLangCode}");

                if (cachedWebsiteData != null)
                {
                    websiteData = cachedWebsiteData as DMultiPageWebsite;
                }
                else
                {
                    websiteData = GetWebsitePages(isoLangCode : isoLangCode);
                    HttpRuntime.Cache.Insert($"WebsiteData_{isoLangCode}", websiteData);
                }

                return new WebsiteDataFullResult { Success = true, Message = "Ok", WebsiteData = websiteData };
            }
            catch (Exception e)
            {
                Log.Error($"Error in GetWebsiteAllData: {e.ToString()}");
                return new WebsiteDataFullResult { Success = false, Message = $"Error: {e}" };
            }
        }

        private DMultiPageWebsite GetWebsitePages(bool getWebsitePages = true, string isoLangCode = null)
        {
            //List all pages of the current Home Page

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            MultiPageWebsite LangWebsiteContent;

            if (isoLangCode != null)
            {
                //get all the tree lang's available for the respective language

                var contentLang = umbracoHelper.TypedContentAtXPath("//multiPageWebsite").SingleOrDefault(a => a.GetCulture().Name == isoLangCode);

                if (contentLang != null)
                {
                    LangWebsiteContent = (MultiPageWebsite)umbracoHelper.TypedContentSingleAtXPath($"//multiPageWebsite[@id={contentLang.Id}]");
                }
                else
                {
                    throw new Exception("No Content with the respective language.");
                }
            }
            else
            {
                LangWebsiteContent = (MultiPageWebsite)umbracoHelper.TypedContentSingleAtXPath("//multiPageWebsite");
            }

            if(LangWebsiteContent != null)
            {
                //var LangWebsiteContent = (MultiPageWebsite)umbracoHelper.TypedContentSingleAtXPath("//multiPageWebsite");

                var globalConfig = LangWebsiteContent.Children<ConfigurationFolder>().Single().Children<GlobalConfigurationSettings>().Single();



                var multiPageWebsite = Mapper.Map<DMultiPageWebsite>(LangWebsiteContent);

                //Add the root content
                var rootPageElements = GetNodePageElements(LangWebsiteContent).FirstOrDefault();

                if (rootPageElements != null)
                {
                    multiPageWebsite.PageElements = PageElements(rootPageElements, globalConfig, LangWebsiteContent.UrlAbsolute());
                }

                #region Get the Nav and the Footer
                try
                {
                    //Header
                    var headerLinks = globalConfig.NavigationMenuMenuBuilder;

                    if (true)
                    {
                        multiPageWebsite.HeaderLinks = headerLinks.Cast<ButtonItem>()
                        .Where(a => a.ButtonLink != null)
                        .Select(a => Mapper.Map<DButtonItem>(a))
                        .ToList();
                    }

                    //Footer
                    var footerLinks = globalConfig.NavigationMenuMenuBuilder;

                    if (true)
                    {
                        multiPageWebsite.FooterLinks = footerLinks
                        .Cast<ButtonItem>()
                        .Where(a => a.ButtonLink != null)
                        .Select(a => Mapper.Map<DButtonItem>(a))
                        .ToList();
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Error while getting the header and the footer: {e}");
                }
                #endregion

                #region Repositories
                //business type
                var businessRepository = (BusinessRepository)umbracoHelper.TypedContentSingleAtXPath("//businessRepository");
                if (businessRepository != null)
                {
                    var BusinessTypes = businessRepository.Children<BusinessItem>();
                    if (BusinessTypes.Any())
                    {
                        multiPageWebsite.BusinessTypes = BusinessTypes.Select(a => new DBusinessItem { NodeId = a.Id, BusinessType = a.NameVorto }).ToList();
                    }
                }


                //startup type
                var startupCategoryRepository = (StartupCategoryRepository)umbracoHelper.TypedContentSingleAtXPath("//startupCategoryRepository");
                if (startupCategoryRepository != null)
                {
                    var StartupCategoryTypes = startupCategoryRepository.Children<StartupCategoryItem>(); if (StartupCategoryTypes.Any())
                    {
                        multiPageWebsite.StartupCategoryTypes = StartupCategoryTypes.Select(a => new DStartupCategoryItem { NodeId = a.Id, StartupCategoryType = a.NameVorto }).ToList();
                    }
                }

                #endregion

                var rootWebsitePages = GetNodeWebsitePages(LangWebsiteContent);

                if (getWebsitePages)
                {
                    multiPageWebsite.WebsitePages = rootWebsitePages.OrderBy(a => a.SortOrder).Select(a => new KeyValuePair<string, DWebsitePage>($"{a.DocumentTypeAlias}_{a.Id}_{a.SortOrder}", WebsitePage(a, globalConfig))).ToDictionary(a => a.Key, a => a.Value);
                }

                string virtualPath = HttpRuntime.AppDomainAppVirtualPath;

                multiPageWebsite.RoutePaths = rootWebsitePages.Select(a => ((a.Url.StartsWith(virtualPath) && virtualPath != "/") ? a.Url.Replace(virtualPath, "") : a.Url)).ToList();

                return multiPageWebsite;
            }
            else
            {
                throw new Exception("Could not find website content for the respective language.");
            }
        }

        private DWebsitePage WebsitePage(IPublishedContent umbracoWebsitePage, GlobalConfigurationSettings globalConfig, bool getWebsitePages = true)
        {


            var websitePage = Mapper.Map<DWebsitePage>(umbracoWebsitePage);

            //check if there is Page elements for the respective page
            var pageElements = GetNodePageElements(umbracoWebsitePage).FirstOrDefault();

            if (pageElements != null)
            {
                var pageElementsMapped = PageElements(
                    pageElements,
                    globalConfig,
                    umbracoWebsitePage.UrlAbsolute()
                );

                websitePage.PageElements = pageElementsMapped;
            }

            //var websitePages = umbracoWebsitePage.Children<WebsitePage>();
            var websitePages = GetNodeWebsitePages(umbracoWebsitePage);

            //Get the Pages of this respective website
            if (getWebsitePages)
            {
                if (websitePages.Any())
                {
                    websitePage.WebsitePages = websitePages.Select(a => new KeyValuePair<string, DWebsitePage>($"{a.DocumentTypeAlias}_{a.Id}_{a.SortOrder}", WebsitePage(a, globalConfig))).ToDictionary(a => a.Key, a => a.Value);
                }
            }

            //Get the routes of this website page
            string virtualPath = HttpRuntime.AppDomainAppVirtualPath;
            websitePage.RoutePaths = websitePages.Select(a => ((a.Url.StartsWith(virtualPath) && virtualPath != "/") ? a.Url.Replace(virtualPath, "") : a.Url)).ToList();

            return websitePage;
        }

        private IDictionary<string, DPageElement> PageElements(IPublishedContent pageElements, GlobalConfigurationSettings globalConfig, string urlAbsolute)
        {
            var pageElementsMapped = new Dictionary<string, DPageElement>();
            var pageElementsRules = new NestCustomNodeRules().GetPageElementsRules((pageElements.DocumentTypeAlias == "pageElements") ? "homepagePageElements" : pageElements.DocumentTypeAlias);//TODO CHANGE THIS LATER
            var umbracoPageElements = pageElements.Children().ToList();

            //int pageElementsIndex = 0;
            for (int x = 0, y = 0; x < pageElementsRules.Count; x++)
            {
                if ((y < umbracoPageElements.Count) && pageElementsRules[x].DocTypeAliasToCreate == umbracoPageElements[y].DocumentTypeAlias)
                {
                    var pageElement = PageElement(umbracoPageElements[y], globalConfig, urlAbsolute);
                    pageElementsMapped.Add($"{pageElementsRules[x].DocTypeAliasToCreate}_{umbracoPageElements[y].Id}_{x}", pageElement.Value);
                    y++;
                }
                else
                {
                    pageElementsMapped.Add($"{pageElementsRules[x].DocTypeAliasToCreate}_{x}", null);
                }
            }
            return pageElementsMapped;
        }

        private KeyValuePair<string, DPageElement> PageElement(IPublishedContent pageElement, GlobalConfigurationSettings globalConfig, string urlAbsolute)
        {
            try
            {
                DPageElement pageElementMapped = null;

                if (pageElement.GetType() == typeof(ShareBar))
                {
                    pageElementMapped = Mapper.Map<DShareBar>(pageElement);

                    var _configurations = ConfigurationManager.GetSection("LeGUConfigurations") as LeGUConfigurations;

                    ((DShareBar)pageElementMapped).SocialMediaItems = globalConfig.SocialShareTo.Select(a => new DShareBarItem
                    {
                        Url = string.Format(_configurations.SocialShareUrls.Get(a).Value, pageElement.UrlAbsolute()),
                        SocialMediaType = a
                    }).ToList();
                }
                else
                {
                    if (!Mapper.GetAllTypeMaps().Any(map => map.SourceType == pageElement.GetType()))
                    {
                        throw new Exception($"There is no mapping defined for the page element type: {pageElement.DocumentTypeAlias} with node id: {pageElement.Id}");
                    }
                    else if (Mapper.GetAllTypeMaps().Count(map => map.SourceType == pageElement.GetType()) != 1)
                    {
                        throw new Exception($"There is more than one mapping for the page element type: {pageElement.DocumentTypeAlias} with node id: {pageElement.Id}");
                    }
                    else
                    {
                        var destinationType = Mapper.GetAllTypeMaps().SingleOrDefault(map => map.SourceType == pageElement.GetType()).DestinationType;
                        pageElementMapped = Mapper.Map(pageElement, pageElement.GetType(), destinationType) as DPageElement;
                    }
                }
                return new KeyValuePair<string, DPageElement>($"{pageElement.DocumentTypeAlias}", pageElementMapped);
            }
            catch (Exception e)
            {
                throw new Exception($"Error while getting the content for the node id: {pageElement.Id} Error: {e.ToString()}");
            }
        }

        public List<IPublishedContent> GetNodeWebsitePages(IPublishedContent node)
        {
            List<IPublishedContent> websitePages = new List<IPublishedContent>();
            foreach (var docAllias in WebsitePageNames)
            {
                websitePages.AddRange(node.Children(docAllias));
            }

            return websitePages;
        }

        public List<IPublishedContent> GetNodePageElements(IPublishedContent node)
        {
            List<IPublishedContent> pageElements = new List<IPublishedContent>();
            foreach (var docAllias in PageElementsNames)
            {
                pageElements.AddRange(node.Children(docAllias));
            }

            return pageElements;
        }
    }
}
