﻿using LeGU.Web.Core.PublishedContentModels;
using LeGU.Web.MP.Core.Objects.Models;
using System;
using Umbraco.Core.Models;

namespace LeGU.Web.MP.Core.Managers
{
    public partial class ModelsManager
    {
        public PageMeta GetPageMetadata(IPublishedContent currentPage, GlobalConfigurationSettings configSettings)
        {
            string metadataTitleSeparator = configSettings.DefaultMetadataTitleSeparator.ToString();
            PageMeta _pageMetadata = new PageMeta();

            _pageMetadata.SiteName = configSettings.SiteName.ToString();
            _pageMetadata.AddSiteNameSuffix = configSettings.AddSuffix;
            _pageMetadata.FaviconMediaId = configSettings.Favicon.Id.ToString();
            _pageMetadata.GenerateAppleTouch = configSettings.GenerateAppleTouchIcons;
            _pageMetadata.GenerateMicrosoftTile = configSettings.GenerateMicrosoftTiles;
            _pageMetadata.MicrosoftTileBackgroundColor = !String.IsNullOrEmpty(configSettings.MicrosoftTileBackgroundColor.ToString()) ?
                        "#" + configSettings.MicrosoftTileBackgroundColor.ToString() : "#000000";
            _pageMetadata.UseTitleFromSEOIfOGMetaTitleIsEmpty = configSettings.UseTitleFromSeoifOgmetaTitleIsEmpty;
            _pageMetadata.UseDescriptionFromSEOIfOGMetaDescriptionIsEmpty = configSettings.UseDescriptionFromSeoifOgmetaDescriptionIsEmpty;
            _pageMetadata.TitleOverride = !String.IsNullOrEmpty(currentPage.GetProperty("overrideTitle").Value.ToString()) ?
                currentPage.GetProperty("overrideTitle").Value.ToString() : currentPage.Name;
            _pageMetadata.SEOTitle = null != currentPage.GetProperty("seoTitle").Value ? currentPage.GetProperty("seoTitle").Value.ToString() : _pageMetadata.TitleOverride;
            _pageMetadata.SEODescription = null != currentPage.GetProperty("seoDescription").Value ?
                currentPage.GetProperty("seoDescription").Value.ToString() : configSettings.SiteDescription.ToString();

            _pageMetadata.SEOKeywords = String.Empty;

            if (null != currentPage.GetProperty("seoKeywords") && null != currentPage.GetProperty("seoKeywords").Value)
            {
                string[] keywords = currentPage.GetProperty("seoKeywords").Value as string[];

                if (keywords.Length > 0)
                {
                    foreach (string keyword in keywords)
                    {
                        _pageMetadata.SEOKeywords += String.Format("{0},", keyword);
                    }

                    if (!String.IsNullOrEmpty(_pageMetadata.SEOKeywords))
                    {
                        _pageMetadata.SEOKeywords.Remove(_pageMetadata.SEOKeywords.Length - 1);
                    }
                }
            }

            _pageMetadata.SEONoIndex = (bool)currentPage.GetProperty("seoNoIndex").Value;
            _pageMetadata.SEONoFollow = (bool)currentPage.GetProperty("seoNoFollow").Value;
            _pageMetadata.OGTitle = null != currentPage.GetProperty("ogTitle").Value ?
                currentPage.GetProperty("ogTitle").Value.ToString() : String.Empty;
            _pageMetadata.OGDescription = null != currentPage.GetProperty("ogDescription").Value ?
                currentPage.GetProperty("ogDescription").Value.ToString() : String.Empty;
            _pageMetadata.OGType = null != currentPage.GetProperty("ogType").Value ?
                currentPage.GetProperty("ogType").Value.ToString() : configSettings.DefaultOgtype.ToString();
            _pageMetadata.OGImageMediaId = currentPage.GetProperty("ogImage").HasValue ?
                ((IPublishedContent)currentPage.GetProperty("ogImage").Value).Id.ToString() : configSettings.DefaultOgimage.Id.ToString();
            _pageMetadata.OGImageWidth = currentPage.GetProperty("ogImageWidth").HasValue ?
               currentPage.GetProperty("ogImageWidth").Value.ToString() : string.Empty;
            _pageMetadata.OGImageHeight = currentPage.GetProperty("ogImageHeight").HasValue ?
              currentPage.GetProperty("ogImageHeight").Value.ToString() : string.Empty;

            if (String.IsNullOrEmpty(_pageMetadata.SEOTitle))
            {
                _pageMetadata.SEOTitle = _pageMetadata.TitleOverride;
            }

            if (_pageMetadata.AddSiteNameSuffix && !currentPage.DocumentTypeAlias.Equals(SinglePageWebsite.ModelTypeAlias) && !currentPage.DocumentTypeAlias.Equals(MultiPageWebsite.ModelTypeAlias))
            {
                _pageMetadata.SEOTitle = String.Format("{0} {1} {2}", _pageMetadata.SEOTitle, metadataTitleSeparator, _pageMetadata.SiteName);
            }

            if (String.IsNullOrEmpty(_pageMetadata.OGTitle) && _pageMetadata.UseTitleFromSEOIfOGMetaTitleIsEmpty)
            {
                _pageMetadata.OGTitle = _pageMetadata.SEOTitle;
            }

            if (String.IsNullOrEmpty(_pageMetadata.OGDescription) && _pageMetadata.UseDescriptionFromSEOIfOGMetaDescriptionIsEmpty)
            {
                _pageMetadata.OGDescription = _pageMetadata.SEODescription;
            }

            return _pageMetadata;
        }
    }
}
