﻿using LeGU.Web.Core.PublishedContentModels;
using log4net;
using Nest.Web.Core.Domain;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Nest.Web.Core.Managers
{
    public class EventToCalenderManager
    {
        protected static readonly ILog Log = LogManager.GetLogger(typeof(EventToCalenderManager));


        public BaseServiceResult GenFile(int nodeId)
        {
            try
            {
                //check if the node id corresponds to a event node from umbraco
                EventDetailsPage nestEvent = new UmbracoHelper(UmbracoContext.Current).TypedContent(nodeId) as EventDetailsPage;

                if(nestEvent != null)
                {
                    //remove html tags for the event description
                    var eventBody = nestEvent.AddToCalenderEventBodyDescription != null ? Regex.Replace((string)nestEvent.AddToCalenderEventBodyDescription, @"<(.|\n)*?>", string.Empty) : "";

                    var ics = GenerateICS(new ICSParams
                    {
                        Date        = nestEvent.ContentEventBeginDateTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                        EndDate     = nestEvent.ContentEventEndDateTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"),
                        Location    = nestEvent.ContentLocation,
                        Description = eventBody,
                        Summary     = nestEvent.AddToCalenderEventSubject
                    });

                    return new EventICSFileResult
                    {
                        Success = true,
                        Message = "OK",
                        ICSXML = ics
                    };
                }
                else
                {
                    return new BaseServiceResult
                    {
                        Success = false,
                        Message = "Event with the respective id does not exists"
                    };
                }
            }
            catch (Exception e)
            {
                Log.Error($"GenFile: {e.ToString()}");
                return new BaseServiceResult
                {
                    Success = false,
                    Message = "Error while getting the ICS File",
                };
            }
        }

        private string GenerateICS(ICSParams icsParams)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("CALSCALE:GREGORIAN");
            sb.AppendLine("BEGIN:VEVENT");
            sb.AppendLine("DTSTAMP:" + icsParams.Date);
            sb.AppendLine("LOCATION:" + icsParams.Location);
            sb.AppendLine("DESCRIPTION:" + icsParams.Description);
            sb.AppendLine("STATUS:CONFIRMED");
            sb.AppendLine("SUMMARY:" + icsParams.Summary);
            sb.AppendLine("DTSTART:" + icsParams.Date);
            sb.AppendLine("CREATED:" + icsParams.Date);
            if (!string.IsNullOrWhiteSpace(icsParams.EndDate))
            {
                sb.AppendLine("DTEND:" + icsParams.EndDate);
            }
            sb.AppendLine("END:VEVENT");
            sb.AppendLine("END:VCALENDAR");
            return sb.ToString();
        }
        class ICSParams
        {
            public string Date { get; set; }
            public string EndDate { get; set; }
            public string Location { get; set; }
            public string Description { get; set; }
            public string Summary { get; set; }
            public string UID { get; set; }
        }
        private bool IsValidHeadingContentType(IPublishedContent heading)
        {
            return heading != null &&
                heading.GetPropertyValue<DateTime>("contentDate") != null &&
                heading.GetPropertyValue<string>("contentTitle") != null;
        }
    }
}
