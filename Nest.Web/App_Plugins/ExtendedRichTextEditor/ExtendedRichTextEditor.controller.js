﻿/*
	Thanks Codery for the wrapping code!
	http://codery.co.uk/blog/enhance-umbraco-data-types-using-a-wrapper/
*/
angular.module('umbraco')
	.filter('removeHTMLTags', function () { //removeHTMLTags is the filter name
		return function (text) {
			return text ? String(text).replace(/<[^>]+>/gm, '') : ''; // used regular expression
		};
	})
	.controller('ExtendedRichTextController',
	function ($filter, $scope, contentTypeResource, dataTypeResource, tinyMceService) {

		var ignoreWatch = false;
		var thumbUp = "thumb-up", alert = "alert", warning = "stop-hand";
		$scope.limit = $scope.model.config.rteMaxChars.length ? parseInt($scope.model.config.rteMaxChars) : 0;
		$scope.counter = $scope.limit - InputLength($scope.model.value);
		$scope.icon = thumbUp;
		$scope.css = thumbUp;
		
		dataTypeResource.getAll()
			.then(function (data) {
				$scope.RTEdataType = _.filter(data, function (dt) {
					//lets get the default TinyMCE (id is negative)
					return dt.alias === "Umbraco.TinyMCEv3" && dt.id < 0;
				});

                if ($scope.model.value === undefined || $scope.model.value === null) {
                    $scope.model.value = '';
                }

                if ($scope.RTEdataType.length) {

                    contentTypeResource.getPropertyTypeScaffold($scope.RTEdataType[0].id)
                        .then(function (dataType) {
                            // set some missing values
                            dataType.id = $scope.model.id;
                            dataType.alias = $scope.model.alias;
                            dataType.value = $scope.model.value;

                            //inject the default config
                            dataType.config = $scope.model.config;//.editor.toolbar.join();

                            // hide the label of the wrapped data type
                            dataType.hideLabel = true;

                            $scope.wrappedProperty = dataType;
                        });
                }

			});
		
		function CheckChars(newInput, oldInput) {
			var newInputSize = InputLength(newInput);
			var oldInputSize = InputLength(oldInput);

			if (newInputSize === 0) {
				$scope.icon = thumbUp;
				$scope.css = thumbUp;

				$scope.model.value = $scope.wrappedProperty.value;
				$scope.counter = $scope.limit;
			}

			else if (newInputSize > $scope.limit) {
				$scope.icon = warning;
				$scope.css = warning;

				ignoreWatch = true;


				$scope.wrappedProperty.onValueChanged($scope.model.value, newInput);

				$scope.counter = $scope.limit - InputLength($scope.model.value);
			}

			else if (newInputSize === $scope.limit) {
				$scope.icon = warning;
				$scope.css = warning;
				
				$scope.model.value = $scope.wrappedProperty.value;
				$scope.counter = 0;
			}

			else if (newInputSize < $scope.limit / 2) {
				$scope.icon = thumbUp;
				$scope.css = thumbUp;

				$scope.model.value = $scope.wrappedProperty.value;
				$scope.counter = $scope.limit - newInputSize;
			}

			else if (newInputSize > $scope.limit / 2) {
				$scope.icon = alert;
				$scope.css = alert;

				$scope.model.value = $scope.wrappedProperty.value;
				$scope.counter = $scope.limit - newInputSize;
			}

			else {
				$scope.icon = alert;
				$scope.css = alert;

				$scope.model.value = $scope.wrappedProperty.value;
				$scope.counter = $scope.limit - newInputSize;
			}
		}

		function InputLength(input) {
			var cleanedInput = $filter("removeHTMLTags")(input);
			return cleanedInput.length;
		}

		$scope.$watch('wrappedProperty.value',
			function (newVal, oldVal) {
				if (ignoreWatch) {
					ignoreWatch = false;
				}
				else if (newVal !== oldVal) {
					CheckChars(newVal, oldVal);
				}
			});
	});