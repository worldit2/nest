﻿angular.module("umbraco").controller("UIOMatic.ButtonValidation",
    function ($scope, $http) {
        $scope.StartupIsValidated = false;
        
        $scope.ValidateStartup = function (StartupId) {
            $http.get("backoffice/api/UIOMaticCustomActions/ValidateStartup?Id=" + StartupId)
                .then(function (response) {
                    alert(response.data.Message);
                    if (response.data.Success) {
                        $scope.StartupIsValidated = true;
                        console.log("change button color");
                    }
                });

        };
    }
);