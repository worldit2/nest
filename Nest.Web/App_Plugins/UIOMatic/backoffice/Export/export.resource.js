﻿angular.module("umbraco.resources")
	.factory("uioMaticExportResource", function ($http, umbRequestHelper) {
	    return {
	        getExport: function (typeAlias) {
	            return umbRequestHelper.resourcePromise(
                    $http.get("backoffice/api/UIOMaticCustomActions/Export?typeAlias=" + typeAlias),
                    'Failed to generate export'
                );
	        }
	    }
	});