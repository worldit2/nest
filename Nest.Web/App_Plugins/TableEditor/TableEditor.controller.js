﻿angular.module("umbraco").controller("TableEditor.Controller",
	function TableEditor($scope) {

		var IsNullOrEmpty = function (input) {
			if (input === null)
				return true;

			if (input === undefined)
				return true;

			if (input === '')
				return true;

			return false;
		};

		var defaultRowNum = IsNullOrEmpty($scope.model.config.defaultRowNum) || $scope.model.config.defaultRowNum === '0' ? 2 : parseInt($scope.model.config.defaultRowNum);
		var defaultColNum = IsNullOrEmpty($scope.model.config.defaultColNum) || $scope.model.config.defaultColNum === '0' ? 2 : parseInt($scope.model.config.defaultColNum);

		$scope.showAdvance = false;

		$scope.addRow = function () {
			$scope.model.value.tableContent[$scope.model.value.tableContent.length] = [];
			for (var i = 0; i < $scope.model.value.tableContent[0].length; i++) {
				$scope.model.value.tableContent[$scope.model.value.tableContent.length - 1].push({ 'value': '', 'isHtml': false});
			}
		};

		$scope.addColumn = function () {
			for (var i = 0; i < $scope.model.value.tableContent.length; i++) {
				$scope.model.value.tableContent[i].push({ 'value': '', 'isHtml': false});
			}
		};

		$scope.canRemoveColumn = function () {
			return $scope.model.value.tableContent[0].length > defaultColNum;
		};

		$scope.canRemoveRow = function () {
			return $scope.model.value.tableContent.length > defaultRowNum;
		};

		$scope.removeRow = function (index) {
			if (confirm('Are you sure you want to remove the entire row?')) {
				$scope.model.value.tableContent.splice(index, 1);
			}
		};

		$scope.removeColumn = function (index) {
			if (confirm('Are you sure you want to remove the entire column?')) {
				for (var i = 0; i < $scope.model.value.tableContent.length; i++) {
					$scope.model.value.tableContent[i].splice(index, 1);
				}
			}
		};

		$scope.clearAll = function () {
			if (confirm('Are you sure you want to reset the table?')) {
				$scope.model.value.tableContent = getDefaultTable();
			}
		};

		$scope.openIconOverlay = function (elem) {
			$scope.overlay = {
				view: "iconpicker",
				title: "Pick an icon",
				show: true,
				submit: function (model) {
					elem.col.type = "icon";
					elem.col.value = '<i class="icon large ' + model.icon + '"></i>';
					elem.col.isHtml = model.icon !== '' ? true : false;
					this.show = false;
				}
			};
		};

		$scope.openRTEOverlay = function (elem) {
			$scope.overlay = {
				view: "/App_Plugins/TableEditor/dialogs/rtedialog/rte_dialog.html",
				title: "Rich text editor",
				show: true,
				submit: function (model) {
					elem.col.type = "rte";
					elem.col.value = model.rteContent;
					elem.col.isHtml = model.rteContent !== '' ? true : false;
					this.show = false;
				},
				dialogData: { rteContent: elem.col.value }
			};
		};

		var getDefaultTable = function () {
			var defaultTable = [];

			for (var i = 0; i < defaultRowNum; i++) {
				defaultTable[i] = [];
				for (var j = 0; j < defaultColNum; j++) {
					defaultTable[i].push({'value': '', 'isHtml': false});
				}
			}

			return defaultTable;
		};

		if ($scope.model.value === undefined || $scope.model.value === '' || $scope.model.value === null) {
			$scope.model.value = {};
			$scope.model.value.tableContent = getDefaultTable();
		}
		
    });
