﻿angular.module("umbraco").controller("TableEditor.Controller",
	function TableEditor($scope) {

		var IsNullOrEmpty = function (input) {
			if (input === null)
				return true;

			if (input === undefined)
				return true;

			if (input === '')
				return true;

			return false;
		};

		var defaultRowNum = IsNullOrEmpty($scope.model.config.defaultRowNum) || $scope.model.config.defaultRowNum === '0' ? 2 : parseInt($scope.model.config.defaultRowNum);
		var defaultColNum = IsNullOrEmpty($scope.model.config.defaultColNum) || $scope.model.config.defaultColNum === '0' ? 2 : parseInt($scope.model.config.defaultColNum);

		$scope.showAdvance = false;

		$scope.addRow = function () {
			$scope.model.value[$scope.model.value.length] = [];
			for (var i = 0; i < $scope.model.value[0].length; i++) {
				$scope.model.value[$scope.model.value.length - 1].push({'value': ''});
			}
		};

		$scope.addColumn = function () {
			for (var i = 0; i < $scope.model.value.length; i++) {
				$scope.model.value[i].push({'value': ''});
			}
		};

		$scope.canRemoveColumn = function () {
			return $scope.model.value[0].length > defaultColNum;
		};

		$scope.canRemoveRow = function () {
			return $scope.model.value.length > defaultRowNum;
		};

		$scope.removeRow = function (index) {
			if (confirm('Are you sure you want to remove the entire row?')) {
				$scope.model.value.splice(index, 1);
			}
		};

		$scope.removeColumn = function (index) {
			if (confirm('Are you sure you want to remove the entire column?')) {
				for (var i = 0; i < $scope.model.value.length; i++) {
					$scope.model.value[i].splice(index, 1);
				}
			}
		};

		$scope.clearAll = function () {
			if (confirm('Are you sure you want to reset the table?')) {
				$scope.model.value = getDefaultTable();
			}
		};

		$scope.openIconOverlay = function (elem) {
			debugger;
			$scope.overlay = {
				view: "/App_Plugins/Iconator/dialogs/iconator.dialog.html",
				title: "Pick an icon",
				show: true,
				submit: function (model) {
					debugger;
					elem.col.value = '<i class="icon ' + model.icon + '></i>'
					this.show = false;
				}
			};
		};

		var getDefaultTable = function () {
			var defaultTable = [];

			for (var i = 0; i < defaultRowNum; i++) {
				defaultTable[i] = [];
				for (var j = 0; j < defaultColNum; j++) {
					defaultTable[i].push({'value': ''});
				}
			}

			return defaultTable;
		};

		if ($scope.model.value === undefined || $scope.model.value === '' || $scope.model.value === null) {
			$scope.model.value = getDefaultTable();
		}
		
    });
