﻿angular.module("umbraco").controller("RTEDialog.Controller",
	function RTEDialog($scope, contentTypeResource, dataTypeResource) {
		$scope.loaded = false;

		var stripPTags = function (input) {
			return input.replace("<p>", "").replace("</p>", "");
		}

		dataTypeResource.getAll()
			.then(function (data) {
				$scope.RTEdataType = _.filter(data, function (dt) {
					//lets get the default TinyMCE (id is negative)
					return dt.alias === "Umbraco.TinyMCEv3" && dt.id < 0;
				});
				if ($scope.RTEdataType.length) {
					contentTypeResource.getPropertyTypeScaffold($scope.RTEdataType[0].id)
						.then(function (dataType) {
							// set some missing values
							dataType.value = $scope.model.dialogData.rteContent;

							//inject the default config
							dataType.config = {
								"editor": {
									"toolbar": ["bold", "italic"],
									"dimensions":
										{
											"height": 500
										}
								}
							};

							// hide the label of the wrapped data type
							dataType.hideLabel = true;

							$scope.wrappedRTEProperty = dataType;

							$scope.loaded = true;
						});
				}
			});

		$scope.$watch('wrappedRTEProperty.value',
			function (newVal, oldVal) {
				if (newVal !== undefined)
					$scope.model.rteContent = stripPTags(newVal);
			});
    });
