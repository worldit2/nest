﻿angular.module("umbraco").controller("SimpleColorPicker.Controller",
	function SimpleColorPicker($scope) {
		$scope.inputRegex = /(^[0-9a-fA-F]{3}$|^[0-9a-fA-F]{6}$)/;
    });
