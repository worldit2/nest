'use strict';

var LEGU = LEGU || {};

LEGU.SIMPLEMEDIA = function () {
    return {
        init: function init(element, data) {
            var view = this;
            view.el = $(element);
            view.variables();
            view.events();
            setTimeout(function () {
                view.sectionHeight();
            }, 100);
        },

        variables: function variables() {
            var view = this;
            view.imgDesktop = view.el.find('.imgDesktop');
            view.imgMobile = view.el.find('.imgMobile');
            view.video = view.el.find('video');
            view.Window = $(window);
        },

        events: function events() {
            var view = this;
            $(window).on('resize', function () {
                view.sectionHeight();
            });
        },

        sectionHeight: function sectionHeight() {
            var view = this;
            if (view.video.length) view.el.css('height', view.video[0].videoHeight + 'px');else {
                view.Window[0].innerWidth > 768 ? view.el.css('height', view.imgDesktop[0].clientHeight + 'px') : view.el.css('height', view.imgMobile[0].clientHeight + 'px');
            }
        }
    };
};