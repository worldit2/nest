'use strict';

var LEGU = LEGU || {};

LEGU.HIGHLIGHTSLIDER = function () {

  return {
    init: function init(element, data) {
      var view = this;
      view.el = $(element);
      view.variables();
      if (view.slider.length) view.initSlider();
      view.sliderHeight();
    },

    variables: function variables() {
      var view = this;
      view.slider = view.el.find('.slider'), view.sliderBtn = view.slider.find('button'), view.mainNav = view.el.prev(), view.containerWrapper = view.slider.find('.wrapper');
    },
    sliderHeight: function sliderHeight() {
      var view = this;
      view.arrowNavs = view.el.find('.slick-prev, .slick-next');

      if (view.mainNav.data('fixed') === true || view.mainNav.data('theme') === true) {
        view.containerWrapper.css('top', view.el.height() / 2 + 'px');
      } else {
        view.containerWrapper.css('top', '');
      }
    },

    initSlider: function initSlider() {
      var view = this;
      view.speedSlider = view.el[0].dataset.speed, view.loop = view.el[0].dataset.loop, view.vertical = view.el[0].dataset.vertical, view.autoPlaySpeed = view.el[0].dataset.autoplayspeed, view.speedSliderDefault = view.el[0].dataset.autoplayspeed, view.autoPlaySpeedDefault = 2000, view.dots = view.el[0].dataset.dots, view.fade = view.el[0].dataset.fade;

      view.slider.on('init', function (event, slick) {
        var view = this;
        var currentItem = $(view).find('.slick-current');
        view.slideBtn = $(view).find('button'), view.sliderList = $(view).find('.slick-dots');

        if (view.sliderList.length) {
          if (currentItem.data('background') == undefined && currentItem.data('bggradient') === undefined && currentItem.is(':not(".mediaOverlay")') || currentItem.data('background') === 'ColorNEU01' || currentItem.data('background') === 'ColorNEU02' || currentItem.data('background') === 'ColorNEU03') {
            view.sliderList.removeClass('btnWhite');
          } else {
            view.sliderList.addClass('btnWhite');
          }
          view.parentElement.dataset.fade == 'true' ? view.sliderList.css('z-index', '9999') : view.sliderList.css('z-index', '');
        } else {
          if (currentItem.data('background') == undefined && currentItem.data('bggradient') == undefined && currentItem.is(':not(".mediaOverlay")') || currentItem.data('background') == 'ColorNEU01' || currentItem.data('background') == 'ColorNEU02' || currentItem.data('background') == 'ColorNEU03') {
            view.slideBtn.removeClass('btnWhite');
          } else {
            view.slideBtn.addClass('btnWhite');
          }

          view.parentElement.dataset.fade == 'true' ? view.slideBtn.css('z-index', '9999') : view.slideBtn.css('z-index', '');
        }
      });

      view.slider.slick({
        arrows: view.vertical === 'true' || view.dots === 'true' ? false : true,
        dots: view.vertical === 'true' || view.dots === 'true' ? true : false,
        pauseOnHover: false,
        fade: view.fade === 'true' ? true : false,
        vertical: view.vertical === 'true' ? true : false,
        verticalSwiping: view.vertical === 'true' ? true : false,
        autoplay: view.loop === 'true' ? true : false,
        autoplaySpeed: view.autoPlaySpeed === '' ? view.autoPlaySpeedDeafult : view.autoPlaySpeed,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: view.loop === 'true' ? true : false,
        speed: view.speedSlider === '' ? view.speedSliderDeafult : view.speedSlider,
        cssEase: 'ease-in-out',
        responsive: [{
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false
          }
        }]
      }).on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var view = this;
        var currentItem = $(slick.$slides.get(currentSlide)),
            nextItem = $(slick.$slides.get(nextSlide));
        view.slideBtn = $(view).find('button'), view.sliderList = $(view).find('.slick-dots');

        if (view.sliderList.length) {
          if (currentItem.data('background') === undefined && currentItem.data('bggradient') === undefined && currentItem.is(':not(".mediaOverlay")') || currentItem.data('background') === 'ColorNEU01' || currentItem.data('background') === 'ColorNEU02' || currentItem.data('background') === 'ColorNEU03') {
            view.sliderList.removeClass('btnWhite');
          } else {
            view.sliderList.addClass('btnWhite');
          }
        } else {
          if (currentItem.data('background') === undefined && currentItem.data('bggradient') === undefined && currentItem.is(':not(".mediaOverlay")') || currentItem.data('background') === 'ColorNEU01' || currentItem.data('background') === 'ColorNEU02' || currentItem.data('background') === 'ColorNEU03') {
            view.slideBtn.removeClass('btnWhite');
          } else {
            view.slideBtn.addClass('btnWhite');
          }
        }
      }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var nextItem = $(slick.$slides.get(nextSlide));
        if (nextItem.is('.video') && nextItem.find('video').length) {
          $(nextItem).find('video').trigger('load');
        } else {
          $('video').trigger('pause');
        }
      });
    }
  };
};