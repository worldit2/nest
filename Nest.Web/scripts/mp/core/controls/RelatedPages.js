'use strict';

var LEGU = LEGU || {};

LEGU.RELATEDPAGES = function () {

    return {
        init: function init(element, data) {
            var view = this;
            view.el = $(element);
            view.variables();
            view.imgRatio();
            view.adjustRows();
            view.windowResize();
        },

        variables: function variables() {
            var view = this;
            view.img = view.el.find('figure img'), view.imgContainer = view.el.find('figure'), view.row = view.el.find('ul');
        },

        imgRatio: function imgRatio() {
            var view = this;

            view.img.each(function (index, item) {

                view.imgNaturalWidth = item.naturalWidth, view.imgNaturalHeight = item.naturalHeight, view.imgContainerHeight = view.imgContainer[0].offsetHeight, view.imgContainerWidth = view.imgContainer[0].offsetWidth;

                if (view.imgNaturalWidth / view.imgContainerWidth < view.imgNaturalHeight / view.imgContainerHeight) {
                    view.finalWidth = view.imgContainerWidth;
                    view.finalHeight = view.finalWidth / view.imgNaturalWidth * view.imgNaturalHeight;
                } else {
                    view.finalHeight = view.imgContainerHeight;
                    view.finalWidth = view.finalHeight / view.imgNaturalHeight * view.imgNaturalWidth;
                }

                $(item).width(view.finalWidth);
                $(item).height(view.finalHeight);
            });
        },

        adjustRows: function adjustRows() {
            var view = this;

            if (window.innerWidth > 1024) {

                view.row.each(function (index, item) {
                    var row = $(item),
                        columns = row.find('li'),
                        textContainer = columns.find(".title"),
                        textContainerList = [],
                        description = columns.find('p').not('.btn p');

                    $(textContainer).css("height", "auto");
                    textContainer.each(function (index, row) {
                        textContainerList.push(row.getBoundingClientRect().height);
                    });
                    var max = Math.max.apply(Math, textContainerList);
                    textContainer.each(function (index, row) {
                        row.style.height = max + "px";
                    });

                    //description
                    $(description).css("height", "auto");
                    for (var i = 0; i < description.length; i++) {
                        var heights = [],
                            padding = 0;
                        description.each(function (index, row) {
                            if ($(row).index() == i) heights.push(row.getBoundingClientRect().height);
                        });
                        var max = Math.max.apply(Math, heights);
                        description.each(function (index, row) {
                            if ($(row).index() == i) row.style.height = max + "px";
                        });
                    };

                    //columns
                    $(columns).css("height", "auto");
                    for (var i = 0; i < columns.length; i++) {
                        var heights = [],
                            padding = 0;
                        columns.each(function (index, row) {
                            heights.push(row.getBoundingClientRect().height);
                        });
                        var max = Math.max.apply(Math, heights);
                        columns.each(function (index, row) {
                            row.style.height = max + "px";
                        });
                    };
                });
            }
        },

        windowResize: function windowResize() {
            var view = this;
            $(window).on('resize', function () {
                view.imgRatio();
                view.adjustRows();
            });
        }
    };
};