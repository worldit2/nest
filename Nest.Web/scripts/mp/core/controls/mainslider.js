'use strict';

var LEGU = LEGU || {};

LEGU.MAINSLIDER = function () {

  return {
    init: function init(element, data) {
      var view = this;
      view.el = $(element);
      view.variables();
      view.initSlider();
    },

    variables: function variables() {
      var view = this;
      view.slider = view.el.find('.slider'), view.sliderBtn = view.slider.find('button');
    },

    initSlider: function initSlider() {
      var view = this;
      view.speedSlider = view.el[0].dataset.speed, view.loop = view.el[0].dataset.loop, view.vertical = view.el[0].dataset.vertical, view.autoPlaySpeed = view.el[0].dataset.autoplayspeed, view.speedSliderDeafult = 600, view.autoPlaySpeedDeafult = 2000, view.dots = view.el[0].dataset.dots, view.fade = view.el[0].dataset.fade;

      view.slider.on('init', function (event, slick) {
        var view = this;
        var currentItem = $(view).find('.slick-current');
        view.slideBtn = $(view).find('button'), view.sliderList = $(view).find('.slick-dots');

        if (view.sliderList.length) {
          if (currentItem.data('background') == undefined && currentItem.data('bggradient') == undefined && currentItem.is(':not(".imgCover")') || currentItem.data('background') == 'ColorNeutralWhite' || currentItem.data('background') == 'ColorNeutralThin' || currentItem.data('background') == 'ColorNeutralLight') {
            view.sliderList.removeClass('btnWhite');
          } else {
            view.sliderList.addClass('btnWhite');
          }
          view.parentElement.dataset.fade == 'true' ? view.sliderList.css('z-index', '9999') : view.sliderList.css('z-index', '');
        } else {
          if (currentItem.data('background') == undefined && currentItem.data('bggradient') == undefined && currentItem.is(':not(".imgCover")') || currentItem.data('background') == 'ColorNeutralWhite' || currentItem.data('background') == 'ColorNeutralThin' || currentItem.data('background') == 'ColorNeutralLight') {
            view.slideBtn.removeClass('btnWhite');
          } else {
            view.slideBtn.addClass('btnWhite');
          }

          view.parentElement.dataset.fade == 'true' ? view.slideBtn.css('z-index', '9999') : view.slideBtn.css('z-index', '');
        }
      });

      view.slider.slick({
        arrows: view.vertical == 'true' || view.dots == 'true' ? false : true,
        dots: view.vertical == 'true' || view.dots == 'true' ? true : false,
        pauseOnHover: false,
        fade: view.fade == 'true' ? true : false,
        vertical: view.vertical == 'true' ? true : false,
        verticalSwiping: view.vertical == 'true' ? true : false,
        autoplay: view.loop == 'true' ? true : false,
        autoplaySpeed: view.autoPlaySpeed == '' ? view.autoPlaySpeedDeafult : view.autoPlaySpeed,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: view.loop == 'true' ? true : false,
        speed: view.speedSlider == '' ? view.speedSliderDeafult : view.speedSlider,
        cssEase: 'ease-in-out'
      }).on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var view = this;
        var currentItem = $(slick.$slides.get(currentSlide)),
            nextItem = $(slick.$slides.get(nextSlide));
        view.slideBtn = $(view).find('button'), view.sliderList = $(view).find('.slick-dots');

        if (view.sliderList.length) {
          if (currentItem.data('background') == undefined && currentItem.data('bggradient') == undefined && currentItem.is(':not(".imgCover")') || currentItem.data('background') == 'ColorNeutralWhite' || currentItem.data('background') == 'ColorNeutralThin' || currentItem.data('background') == 'ColorNeutralLight') {
            view.sliderList.removeClass('btnWhite');
          } else {
            view.sliderList.addClass('btnWhite');
          }
        } else {
          if (currentItem.data('background') == undefined && currentItem.data('bggradient') == undefined && currentItem.is(':not(".imgCover")') || currentItem.data('background') == 'ColorNeutralWhite' || currentItem.data('background') == 'ColorNeutralThin' || currentItem.data('background') == 'ColorNeutralLight') {
            view.slideBtn.removeClass('btnWhite');
          } else {
            view.slideBtn.addClass('btnWhite');
          }
        }
      });
    }
  };
};