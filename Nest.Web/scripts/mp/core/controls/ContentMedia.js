'use strict';

var LEGU = LEGU || {};

LEGU.CONTENTMEDIA = function () {

    return {
        init: function init(element, data) {
            var view = this;
            view.el = $(element);
            view.variables();
            view.imgRatio();
            view.windowResize();
        },

        variables: function variables() {
            var view = this;
            view.img = view.el.find('figure img'), view.imgContainer = view.el.find('figure'), view.container = view.imgContainer[0].parentNode;
        },

        imgRatio: function imgRatio() {
            var view = this;
            view.imgNaturalWidth = view.img[0].naturalWidth, view.imgNaturalHeight = view.img[0].naturalHeight, view.imgContainerHeight = view.imgContainer[0].offsetHeight, view.imgContainerWidth = view.imgContainer[0].offsetWidth;

            if (view.container.dataset.cropimage === 'true') {

                if (view.imgNaturalWidth / view.imgContainerWidth < view.imgNaturalHeight / view.imgContainerHeight) {
                    view.finalWidth = view.imgContainerWidth;
                    view.finalHeight = view.finalWidth / view.imgNaturalWidth * view.imgNaturalHeight;
                } else {
                    view.finalHeight = view.imgContainerHeight;
                    view.finalWidth = view.finalHeight / view.imgNaturalHeight * view.imgNaturalWidth;
                }

                view.img.width(view.finalWidth);
                view.img.height(view.finalHeight);
            }
        },

        windowResize: function windowResize() {
            var view = this;
            $(window).on('resize', function () {
                view.imgRatio();
            });
        }
    };
};