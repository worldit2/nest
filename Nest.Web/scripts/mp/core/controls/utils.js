"use strict";

var LEGU = LEGU || {};

LEGU.UTILS = function () {
  var isTablet = function isTablet() {
    return $(window).width() <= 1024;
  };
  var isMobile = function isMobile() {
    return $(window).width() <= 768;
  };
  var isMac = function isMac() {
    return navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i);
  };
  var isIe = function isIe() {
    return navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || typeof $.browser !== "undefined" && $.browser.msie == 1;
  };
  var bodyElement = $("body");
  var mainElement = $("main");
  //matches polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (s) {
      var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
      while (--i >= 0 && matches.item(i) !== this) {}
      return i > -1;
    };
  }
  //includes polyfill
  if (!Array.prototype.includes) {
    Array.prototype.includes = function () {
      'use strict';

      return Array.prototype.indexOf.apply(this, arguments) !== -1;
    };
  }
  //nodelist foreach polyfill
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }
  //closest polyfill
  if (!Element.prototype.closest) Element.prototype.closest = function (s) {
    var el = this;
    var ancestor = this;
    if (!document.documentElement.contains(el)) return null;
    do {
      if (ancestor.matches(s)) return ancestor;
      ancestor = ancestor.parentElement;
    } while (ancestor !== null);
    return null;
  };
  function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
    document.body.appendChild(outer);
    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";
    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);
    var widthWithScroll = inner.offsetWidth;
    // remove divs
    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
  }

  return {
    init: function init(element, data) {
      var view = this;
      view.el = $(element);
      view.variables();
      view.events();
      view.setWatchers();
      view.lists.each(function (index, list) {
        view.showFirstElements($(list));
      });
      setTimeout(function () {
        $(".modals section").hide();
      }, 10);
      view.cover.each(function (index, cover) {
        view.mediaCover();
      });
    },
    variables: function variables() {
      var view = this;
      view.yHeight;
      view.lists = $("[data-showMore]");
      view.cover = $('[data-cover]');
      view.slideUp = $('[data-slideUp]');
      view.slideDown = $('[data-slideDown]');
      view.slideUpWatchers = [];
      for (var i = 0; i < view.slideUp.length; i++) {
        view.slideUpWatchers.push(scrollMonitor.create(view.slideUp[i]));
      }
    },

    events: function events() {
      var view = this;
      bodyElement.on("click", ".openOverlay", view.openOverlay.bind(view));
      bodyElement.on("click", ".closeOverlay, .closeButton", view.closeOverlay.bind(view));
      bodyElement.on("click", ".closeOverlayMobile, .closeButtonMobile", view.closeOverlayMobile);
      bodyElement.on("click", ".searchBox", function (event) {
        event.stopPropagation();
      });
      bodyElement.on("click", ".closeNotificationBar, .closeCookie", view.closeNotificationBar); //tirar compare e cookie
      bodyElement.on("click", ".overlayContainer", function (event) {
        event.stopPropagation();
      });
      bodyElement.on("click", "[data-anchor]", view.slideToAnchor);
      bodyElement.on("click", ".openModal", view.openModal.bind(view));
      bodyElement.on("click", ".scrollDown", view.scrollDown.bind(view));
      $(".closeModal").on("click", view.closeModal.bind(view));
    },

    openOverlay: function openOverlay(e) {
      e.preventDefault();
      var view = this,
          target = e.currentTarget;
      view.yHeight = window.pageYOffset;
      if (target.classList.contains("video")) {
        view.videoSrc = target.dataset.video;
        if (!isMobile()) {
          view.adjustMainOnOverlay(true);
          view.injectVideoOverlay(e, view.videoSrc);
          $(".videoOverlay").scrollTop = 0;
          $(".videoOverlay").addClass("show");
        } else window.open(view.videoSrc);
      }
      if (target.classList.contains("search")) {
        view.injectSearchOverlay(isTablet());
        if (!isTablet()) {
          view.adjustMainOnOverlay(true);
        } else {
          $(".menuOverlay").addClass("moved");
          mainElement.addClass("hidden");
        }
        setTimeout(function () {
          $(".searchOverlay").addClass("show");
        }, 10);
        setTimeout(function () {
          $(".searchOverlay").find("input").focus();
        }, 500);
      }
    },

    injectVideoOverlay: function injectVideoOverlay(target, link) {
      var target = target.currentTarget;
      var autoplay = "?autoplay=1";
      target.dataset.source == 'iframe' ? bodyElement.append("<div class=\"videoOverlay overlay closeOverlay\">\n                <div class=\"overlayContainer\">\n                    <div class=\"closeButton\"><i class=\"icon-bas-012\"></i></div>\n                    <iframe width=\"60%\" height=\"60%\" src=\"" + (link + autoplay) + "\" frameborder=\"0\" allow=\"autoplay\" allowfullscreen></iframe>\n                </div>\n            </div>") : bodyElement.append("<div class=\"videoOverlay overlay closeOverlay\">\n                <div class=\"overlayContainer\">\n                    <div class=\"closeButton\"><i class=\"icon-bas-012\"></i></div>\n                    <video id=\"myVideo\" autoplay=\"autoplay\" loop controls=\"\"><source src=\"" + link + "\" type=\"video/mp4\"></video>\n                </div>\n            </div>");
    },
    injectSearchOverlay: function injectSearchOverlay(isMobile) {
      var OverlayClass = isMobile ? "closeOverlayMobile" : "closeOverlay";
      var closeButtonClass = isMobile ? "closeButtonMobile" : "closeButton";
      bodyElement.append("<div class=\"searchOverlay overlay " + PBSOverlayClass + "\">\n          <div class=\"overlayContainer\">\n            <div class=\"" + closeButtonClass + "\"><i class=\"icon-base-close\"></i></div>\n            <div class=\"searchContainer\">\n              <form class=\"formFields\" data-control=\"FORMFIELDS\">\n                <div class=\"textField searchField\">\n                  <i class=\"icon-pbs-s-006\"></i>\n                  <input type=\"text\">\n                  <label for=\"\">Pesquisar</label>\n                  <span>Este campo \xE9 obrigat\xF3rio</span>\n                </div>\n              </form>\n            </div>\n          </div>\n        </div>");
      PBS.FORMFIELDS().init($(".searchOverlay .formFields"));
    },
    closeOverlay: function closeOverlay(e) {
      var view = this;
      e.preventDefault();
      var target = $(e.currentTarget).closest(".overlay");
      view.adjustMainOnOverlay(false);
      if (target.hasClass("videoOverlay")) {
        target.removeClass("show").find("iframe, video").attr("src", "");
        setTimeout(function () {
          $(".videoOverlay").remove();
        }, 500);
      };
      if (target.hasClass("searchOverlay")) {
        target.removeClass("show");
        setTimeout(function () {
          $(".searchOverlay").remove();
        }, 500);
      };
    },
    closeOverlayMobile: function closeOverlayMobile() {
      mainElement.removeClass("hidden");
      $(".searchOverlay").removeClass("show");
      $(".menuOverlay").removeClass("moved");
      setTimeout(function () {
        $(".searchOverlay").remove();
      }, 500);
    },
    adjustMainOnOverlay: function adjustMainOnOverlay(begin) {
      var view = this,
          scale = 0.9,
          newY = 0;
      var x = window.pageYOffset,
          height = $(document).height(),
          a = "",
          b = "",
          c = 3 * height / 20;
      if (begin) {
        newY = view.yHeight;
        bodyElement.css({
          overlfow: "hidden"
        });
        mainElement.css({
          transform: "translate3d(0,0,1px) scale3d(" + scale + "," + scale + ",1) perspective(50px)",
          opacity: "0.3"
        });
      } else {
        newY = view.yHeight;
        bodyElement.css({
          overlfow: "auto"
        });
        mainElement.css({
          transform: "none",
          opacity: "1"
        });
      }
      window.scroll(0, newY);
    },
    openNotificationBar: function openNotificationBar(barInfo) {
      var icon = "<i class=\"icon-base-close closeNotificationBar\"></i>";
      if (barInfo.buttonFunc === "cookie") icon = "<i class=\"icon-pbs-s-024 closeCookie\"></i>";
      var messageTemplate = "\n        <div class=\"notificationBar " + barInfo.type + "\">\n          <div class=\"wrapper\">\n            <p class=\"messageText\">" + barInfo.message + "</p>\n            " + icon + "\n          </div>\n        </div>\n      ";
      mainElement.append(messageTemplate);
      setTimeout(function () {
        $(".notificationBar").addClass("show");
      }, 100);
    },
    closeNotificationBar: function closeNotificationBar(event, barToClose) {
      if (barToClose) {
        barToClose.removeClass("show");
        setTimeout(function () {
          barToClose.remove();
        }, 210);
        return;
      }
      var bar = $(event.currentTarget).closest(".notificationBar");
      if (event.target.classList.contains("closeCookie")) Cookies.set("LEGU_Cookie", true, { expires: 365 });
      bar.removeClass("show");
      setTimeout(function () {
        bar.remove();
      }, 210);
    },
    openModal: function openModal(event) {
      event.preventDefault();
      var modalId = event.currentTarget.dataset.modal,
          modalsContainer = $(".modals"),
          widthNoScroll = bodyElement.width() - getScrollbarWidth();
      if (modalsContainer.find("section[data-modal=\"" + modalId + "\"]").hasClass("modal_programFinder")) modalsContainer.find(".closeModal").addClass("white");

      bodyElement.css("overflow", "hidden");
      modalsContainer.scrollTop = 0;
      modalsContainer.fadeIn("fast").addClass("opened");
      setTimeout(function () {
        modalsContainer.find("section[data-modal=\"" + modalId + "\"]").show().addClass("opened");
        modalsContainer.find(".mask").addClass("solid").css("width", widthNoScroll);
        modalsContainer.find('.closeModal').addClass('visible');
      }, 400);
      setTimeout(function () {
        modalsContainer.find(".mask").removeClass("solid");
      }, 2000);
    },
    closeModal: function closeModal() {
      var view = this,
          modalsContainer = $(".modals");
      bodyElement.css("overflow", "auto");
      modalsContainer.find(".closeModal").removeClass("white visible");
      modalsContainer.find("section").removeClass("opened");
      setTimeout(function () {
        modalsContainer.find("section").hide();
        modalsContainer.fadeOut().removeClass("opened");
      }, 400);
    },
    slideToAnchor: function slideToAnchor(e) {
      var target = e.currentTarget;
      e.preventDefault();
      var destination = target.dataset.anchor,
          navFix = document.querySelector('.mainNav').dataset.fixed;

      $("body, html").animate({
        scrollTop: $("section[data-sectionid=\"" + destination + "\"]").offset().top - (navFix !== "true" ? 0 : 80)
      }, {
        duration: 1500,
        easing: 'swing'
      });
    },

    scrollDown: function scrollDown() {
      var view = this;
      view.scrollBtnNext = document.querySelector('.scrollDown').parentElement.nextElementSibling, view.mainNav = document.querySelector('.mainNav');
      view.offsetTop = view.mainNav.dataset.fixed == "true" ? view.mainNav.dataset.theme == "true" ? $(view.scrollBtnNext).offset().top : $(view.scrollBtnNext).offset().top - 80 : $(view.scrollBtnNext).offset().top;

      $("body, html").animate({

        scrollTop: view.offsetTop
      }, {
        duration: 1000,
        easing: 'swing'
      });
    },

    mediaCover: function mediaCover() {
      var view = this;

      view.cover[0].dataset.cover == "true" ? view.cover.find('.cover').addClass('bgScale ') : view.cover.find('.cover').removeClass('bgScale ');
    },

    setWatchers: function setWatchers() {
      var view = this;
      view.slideUpWatchers.forEach(function (watcher, index) {
        var childs = Array.prototype.slice.call(watcher.watchItem.querySelectorAll('[data-slider]'));

        childs.forEach(function (element, index) {
          var translateValue = element.dataset.translate ? translateValue = element.dataset.translate : translateValue = 40;
          element.style.transform = "translateY(" + translateValue + "px)";
          element.style.transitionDelay = '0s';
        });
        watcher.enterViewport(function () {
          var delay = 0;
          var delayValue = watcher.watchItem.dataset.delay ? parseFloat(watcher.watchItem.dataset.delay) : .3;
          view.slideUp[index].style.opacity = 1;
          childs.forEach(function (element, index) {
            element.style.transform = "none";
            element.style.opacity = 1;
            delay += delayValue;
            element.style.transitionDelay = delay + 's';
          });
        });
        if (view.slideUp[index].dataset.slidedown == 'true') {
          watcher.exitViewport(function () {
            view.slideUp[index].style.opacity = 0;
            childs.forEach(function (element, index) {
              var translateValue = element.dataset.translate ? translateValue = element.dataset.translate : translateValue = 40;
              element.style.transform = "translateY(" + translateValue + "px)";
              element.style.opacity = 0;
              element.style.transitionDelay = '0s';
            });
          });
        }
      });
    },

    resetWatchers: function resetWatchers() {
      var view = this;
      view.slideUpWatchers.forEach(function (watcher) {
        watcher.recalculateLocation();
      });
    }
  };
}();