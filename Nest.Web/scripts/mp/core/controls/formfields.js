"use strict";

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LEGU = LEGU || {};

LEGU.FORMFIELDS = function () {
  return {
    init: function init(element, data) {
      var view = this;
      view.el = $(element);
      view.variables();
      view.events();
      view.initInputs();
      view.initRangesliders();
      view.initAutocompletes();
    },
    variables: function variables() {
      var view = this;
      view.submitButton = view.el.find(".submitButton");
      view.inputs = view.el.find("input[type=text], input[type=email], input[type=password], textarea");
      view.textareas = view.el.find("textarea");
      view.rangeSliders = view.el.find(".rangeSlider");
      view.accordions = view.el.find(".accordionField");
      view.dropdowns = view.el.find(".dropdownField");
      view.radios = view.el.find(".radioContainer");
      view.checkboxes = view.el.find(".checkContainer");
      view.autocompletes = view.el.find(".autocompleteField");
      view.disclaimers = view.el.find(".disclaimer");
      view.searchField = view.el.find(".searchField");
      view.requireds = view.el.find(".required");
      view.resultList = [];
      view.successMessage = view.el.data().success;
      view.errorMessage = view.el.data().error;
      view.incompleteMessage = view.el.data().incomplete;
      view.warningMessage = view.el.data().warning;
    },
    events: function events() {
      var view = this;
      view.el.on("submit", function () {
        return false;
      });
      view.el.find("input, textarea, .dropdownTitle, .dropdownLabel").on("click focus", view.cleanInput);
      view.el.find(".checkField input").on("click", view.checkCheckboxes);
      view.el.find(".listBox li").on("click", view.toggleLockedField.bind(view));
      view.inputs.on("blur", view.checkInput);
      view.textareas.on("input", view.adjustTextarea);
      view.accordions.find(".accordionTitle").on("click", view.toggleAccordion);
      view.dropdowns.find(".dropdownLabel, .dropdownTitle").on("click", view.toggleDropList);
      view.dropdowns.on("click", ".listBox li", view.selectDropdownOption);
      view.autocompletes.on("click", ".autocompletelabel, .autocompleteInput", view.toggleDropList);
      view.autocompletes.on("click", ".listBox li", view.selectDropdownOption);
      view.autocompletes.on("click", "i", view.goToLink);
      view.autocompletes.on("keydown", "input", view.openDropList.bind(view));
      view.searchField.on("keyup", "input", view.goToSearch.bind(view));
      view.searchField.on("click", "i", view.goToSearch.bind(view));
      view.searchField.on("click focus", view.selectSearchText.bind(view));
      view.submitButton.on("click", view.validateForm.bind(view));
      $(window).on("click", view.closeAllDropdowns.bind(view));
    },
    initInputs: function initInputs() {
      this.inputs.each(function (index, item) {
        item.value.length ? item.parentElement.classList.add("filled") : item.parentElement.classList.remove("filled");
      });
    },
    initAutocompletes: function initAutocompletes(auto) {
      var view = this;
      var autocompletes = auto !== undefined ? auto : view.autocompletes;
      autocompletes.each(function (index, element) {
        var listBox = $(element).find(".listBox"),
            options = [];
        listBox.find("li").each(function (index, item) {
          options.push({ text: item.innerText, link: item.dataset.programlink });
        });
        element.addEventListener("change", displayMatches);
        element.addEventListener("keyup", displayMatches);
        function getNewWord(oldWord) {
          var newWord = "";
          for (var i = 0; i < oldWord.length; i++) {
            if (oldWord[i] === "a") newWord += "[aáâàã]";else if (oldWord[i] === "e") newWord += "[eéêè]";else if (oldWord[i] === "i") newWord += "[iéîì]";else if (oldWord[i] === "o") newWord += "[oóôòõ]";else if (oldWord[i] === "u") newWord += "[uúûù]";else if (oldWord[i] === "c") newWord += "[cç]";else newWord += oldWord[i];
          };
          return newWord;
        }

        function findMatches(wordToMatch, options) {
          var newWord = getNewWord(wordToMatch);
          return options.filter(function (option) {
            var regex = new RegExp(newWord, "gi");
            return option.text.match(regex);
          });
        };
        function displayMatches() {
          var input = this.children[1].value;
          var matchArray = findMatches(input, options);
          var html = matchArray.map(function (option) {
            var newWord = getNewWord(input);
            var regex = new RegExp(newWord, "gi");
            var value = option.text.replace(regex, "<em class=\"hl\">" + input + "</em>");
            if (option.link === undefined) {
              return "<li>" + value + "</li>";
            } else {
              return "<li data-programlink=\"" + option.link + "\"><a href=\"" + option.link + "\">" + value + "</a></li>";
            }
          }).join("");
          if (html.length == 0) html = "<li class='notAnOption'>Sem correspondências</li>";
          listBox.html(html);
        };
      });
    },
    initRangesliders: function initRangesliders() {
      var view = this;
      view.rangeSliders.each(function (index, item) {
        var start = parseInt(item.dataset.start),
            infinite = item.dataset.infinite === "true",
            min = parseInt(item.dataset.min),
            max = infinite ? parseInt(item.dataset.max) + 1 : parseInt(item.dataset.max),
            step = parseInt(item.dataset.step);
        noUiSlider.create(item, _defineProperty({
          start: [start],
          connect: true,
          range: {
            'min': min,
            'max': max
          },
          step: step,
          pips: {
            mode: "steps",
            stepped: true,
            density: 10
          }
        }, "connect", [true, false]));
        if (infinite) $(item).find(".noUi-value").last().html("+" + (max - 1));
        //get text
        if (item.dataset.text) {
          var rawText = item.dataset.text.split("#"),
              textContainer = item.nextElementSibling,
              text = [];
          rawText.forEach(function (textBit, index) {
            var bits = textBit.split("|");
            text[index] = { min: parseInt(bits[0]), max: parseInt(bits[1]), text: bits[2] };
          });
        }
        item.noUiSlider.on('update', function (values, handle, unencoded, tap, positions) {
          var targetEl = $(this.target);
          //paint numbers
          var paintUntil = values / step - min;
          targetEl.find('.noUi-pips .noUi-value').each(function (index, el) {
            $(el).removeClass('fill current');
            if (index <= paintUntil) $(el).addClass('fill');
            if (index === paintUntil) $(el).addClass('current');
          });
          //change text
          if (item.dataset.text) changeText(parseInt(values));
        });
        function changeText(point) {
          text.forEach(function (range) {
            if (range.min <= point && point <= range.max) textContainer.innerText = range.text;
          });
        }
      });
    },
    cleanInput: function cleanInput() {
      var field = $(this).parent();
      field.addClass("active");
      if (field.hasClass("error")) field.removeClass("error");
      if (field.hasClass("radioField") || field.hasClass("checkField")) field.parent().removeClass("error");
    },
    checkInput: function checkInput() {
      var input = this,
          container = input.parentElement,
          emptyError = container.dataset.errorempty,
          incorrectError = container.dataset.errorincorrect,
          regex = container.dataset.regex || "";
      if (input.value !== "") {
        // input filled, ok
        input.value.match("\\b" + regex + "\\b") ? (container.classList.add("filled"), container.classList.remove("error", "active") //passou no regex
        ) : (container.classList.add("error", "filled"), container.classList.remove("active"), container.querySelector("span").innerText = incorrectError); //não passou no regex
      } else {
        //input empty
        if (container.classList.contains("required")) {
          //required input
          container.classList.add("error");
          container.querySelector("span").innerText = emptyError;
        };
        container.classList.remove("active", "filled"); //optional input
      }
    },
    checkCheckboxes: function checkCheckboxes() {
      var container = $(this).parent().parent();
      var checkeds = container.find("input:checked").parent(),
          notCheckeds = container.find("input:not(:checked)").parent();
      checkeds.length >= container.data().max ? notCheckeds.addClass("disabled") : notCheckeds.removeClass("disabled");
    },
    adjustTextarea: function adjustTextarea(event) {
      event.target.style.height = ""; //reset height
      event.target.style.height = Math.min(event.target.scrollHeight + 1, 300) + "px";
    },
    toggleAccordion: function toggleAccordion() {
      $(this).next(".accordionBox").slideToggle(400);
      $(this).toggleClass("opened");
    },
    toggleDropList: function toggleDropList() {
      $(this).parent().toggleClass("listOpened").find(".listBox").slideToggle(400);
    },
    openDropList: function openDropList(e) {
      var view = this,
          target = e.currentTarget;
      if (e.keyCode == 13) {
        e.preventDefault();
        view.goToLink(null, target);
      }
      $(target).parent().addClass("listOpened").find(".listBox").slideDown(400);
    },
    selectDropdownOption: function selectDropdownOption() {
      if (this.classList.contains("notAnOption")) return;
      var list = $(this).parent(),
          element = list.parent();
      element.hasClass("dropdownField") ? list.slideToggle(400).parent().addClass("filled").removeClass("listOpened error").find(".dropdownTitle").html(this.innerText) : list.slideToggle(400).parent().addClass("filled").removeClass("listOpened error").find(".autocompleteInput").val(this.innerText);
      this.dataset.value ? element.hasClass("dropdownField") ? element.find(".dropdownTitle").attr("data-value", this.dataset.value) : element.find(".autocompleteInput").attr("data-value", this.dataset.value) : element.hasClass("dropdownField") ? element.find(".dropdownTitle").attr("data-value", "") : element.find(".autocompleteInput").attr("data-value", "");
    },
    closeAllDropdowns: function closeAllDropdowns(event) {
      var view = this;
      if (!event.target.matches('.dropdownLabel, .dropdownTitle, .autocompleteLabel, .autocompleteInput')) {
        for (var i = 0; i < view.dropdowns.length; i++) {
          if ($(view.dropdowns[i]).hasClass('listOpened')) {
            $(view.dropdowns[i]).removeClass('listOpened').find(".listBox").slideUp(400);
          }
        };
        for (var i = 0; i < view.autocompletes.length; i++) {
          if ($(view.autocompletes[i]).hasClass('listOpened')) {
            $(view.autocompletes[i]).removeClass('listOpened').find(".listBox").slideUp(400);
          }
        };
      }
    },

    toggleLockedField: function toggleLockedField(e) {
      var view = this,
          target = e.currentTarget;
      if (!target.parentElement.querySelector("[data-unlockfield]")) return;

      var lockedTargetName = target.dataset.unlockfield || target.parentElement.querySelector("[data-unlockfield]").dataset.unlockfield || undefined,
          lockedTarget = view.el.find("[data-name=\"" + lockedTargetName + "\"]");
      target.classList.contains("unlockField") ? lockedTarget.slideDown() : lockedTarget.slideUp();
    },
    goToLink: function goToLink(e) {
      var element = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : e.currentTarget.parentElement.querySelector("input");

      var inputValue = element.value,
          listBox = $(element).parent().find(".listBox");
      listBox.find("li").each(function (index, program) {
        if (inputValue === program.innerText) window.location.href = program.dataset.programlink;
      });
    },
    goToSearch: function goToSearch(event) {
      event.preventDefault();
      var view = this;
      var value = view.searchField.find("input").val();
      if ((event.keyCode == 13 || event.keyCode == undefined) && value.length > 0) window.location.href = location.origin + "/pt/pesquisa?s=" + encodeURIComponent(value);
    },
    selectSearchText: function selectSearchText() {
      var view = this;
      if (view.searchField.hasClass('filled')) view.searchField.find('input').select();
    },
    validateForm: function validateForm(event) {
      event.preventDefault();
      var view = this,
          allValidated = true,
          requireds = view.requireds.toArray(),
          //get all required fields
      filled = view.el.find(".filled").toArray(); //get all filled fields
      view.checkboxes.each(function (index, item) {
        if ($(item).find("input:checked").length) filled.push(item);
      });
      view.radios.each(function (index, item) {
        if ($(item).find("input:checked").length) filled.push(item);
      });
      view.sendList = [].concat(_toConsumableArray(requireds), _toConsumableArray(filled));
      view.resultList.length = 0;
      view.sendList.forEach(function (item, index) {
        if (item.style.display == 'none') return;
        if (item.classList.contains("textField")) view.resultList.push(view.validateText(item, item.dataset.regex));
        if (item.classList.contains("checkContainer")) view.resultList.push(view.validateCheckbox(item));
        if (item.classList.contains("radioContainer")) view.resultList.push(view.validateRadio(item));
        if (item.classList.contains("sliderField")) view.resultList.push(view.validateSlider(item));
        if (item.classList.contains("dropdownField")) view.resultList.push(view.validateDropdown(item));
        if (item.classList.contains("autocompleteField")) view.resultList.push(view.validateAutocomplete(item));
      });
      view.resultList.forEach(function (item) {
        if (!item.result) allValidated = false;
      });
      if ((!view.disclaimers.length || view.validateCheckbox(view.disclaimers.get(0)).result) && allValidated) view.sendData();else view.slideToError();
    },
    validateText: function validateText(item) {
      var regex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      //ok
      var result,
          emptyError = item.dataset.errorempty,
          incorrectError = item.dataset.errorincorrect,
          errorSpan = item.querySelector("span"),
          input = item.querySelector("input") || item.querySelector("textarea");
      if (item.dataset.name !== "hiddenfield") {
        if (input.value !== "") {
          input.value.match("\\b" + regex + "\\b") ? (item.classList.remove("error"), result = input.value) : errorSpan.innerText = incorrectError;
        } else if (item.classList.contains("required")) {
          item.classList.add("error");
          errorSpan.innerText = emptyError;
          result = 0;
        }
      } else result = true;
      return { name: item.dataset.name, type: "text", value: input.value, result: result };
    },
    validateCheckbox: function validateCheckbox(item) {
      //ok
      var min = item.dataset.min || 1,
          max = item.dataset.max || item.querySelectorAll("input").length,
          name = item.dataset.name,
          checkeds = $(item).find("input:checked");
      checkeds.length >= min && checkeds.length <= max ? item.classList.remove("error") : item.classList.add("error");
      var values = [];
      checkeds.each(function (index, item) {
        values.push(item.nextElementSibling.innerHTML);
      });
      return { name: item.dataset.name, type: "checkbox", value: values, result: checkeds.length };
    },
    validateRadio: function validateRadio(item) {
      //ok
      var result = item.getElementsByClassName("active").length,
          value;
      if (result) {
        item.classList.remove("error");
        value = item.querySelector("input:checked").parentElement.querySelector("label").innerText;
      } else {
        item.classList.add("error");
        value = null;
      }
      return { name: item.dataset.name, type: "radio", value: value, result: result };
    },
    validateSlider: function validateSlider(item) {
      //ok
      var min = item.dataset.min,
          max = item.dataset.max,
          value = parseInt(item.firstElementChild.noUiSlider.get()),
          result = value > min && value < max;
      result ? item.classList.remove("error") : item.classList.add("error");
      return { name: item.dataset.name, type: "slider", value: value, result: result };
    },
    validateDropdown: function validateDropdown(item) {
      //ok
      var result = item.classList.contains("filled"),
          text = item.querySelector(".dropdownTitle"),
          type = item.classList.contains("dropdownField") ? "dropdown" : "autocomplete",
          value = text.dataset.value ? text.dataset.value : text.innerHTML;
      result ? item.classList.remove("error") : item.classList.add("error");
      return { name: item.dataset.name, type: type, value: value, result: result };
    },
    validateAutocomplete: function validateAutocomplete(item) {
      //ok
      var input = item.getElementsByTagName("input")[0].value,
          result = input.length;
      return { name: item.dataset.name, type: "autocomplete", value: item.querySelector("input").value, result: result };
    },
    sendData: function sendData() {
      var view = this,
          url = window.location.origin + "/" + view.el.data("service-endpoint"),
          type = "POST",
          data = {};
      view.submitButton.off("click");
      data["url"] = window.location.href;
      data["documentNodeId"] = view.el.parent().data("modal");
      view.resultList.forEach(function (item) {
        return data[item.name] = item.value;
      });
      // for(let item of view.resultList) {
      //   data[item.name] = item.value;
      // };
      $.ajax({
        url: url,
        type: type,
        data: data,
        success: function success(result) {
          view.submitButton.on("click", view.validateForm.bind(view));
          //////////////////
          if (view.submitButton.data("datalayer") && !result) {
            var splitted = view.submitButton.data("datalayer").split(","),
                eventInfo = splitted[0],
                clickInfo = splitted[1];
            dataLayer.push({ 'event': eventInfo }, 'click', clickInfo);
          }
          ////////////////// 
          view.el.trigger("reset");
          LEGU.UTILS.closeModal();
          result ? LEGU.UTILS.openNotificationBar({ type: "warning", message: view.warningMessage }) : LEGU.UTILS.openNotificationBar({ type: "success", message: view.successMessage });
          view.initInputs();
        },
        error: function error(_error) {
          view.submitButton.on("click", view.validateForm.bind(view));
          LEGU.UTILS.openNotificationBar({ type: "error", message: view.errorMessage });
          view.initInputs();
        }
      });
    },
    slideToError: function slideToError() {
      var view = this,
          isModal = $(".modals").hasClass("opened"),
          firstErrorPos = view.el.find(".error").eq(0).position().top,
          container = isModal ? $(".modals") : $("html, body");
      isModal ? "" : firstErrorPos += view.el.offset().top;
      container.animate({
        scrollTop: firstErrorPos - 40
      }, 500);
    }
  };
};