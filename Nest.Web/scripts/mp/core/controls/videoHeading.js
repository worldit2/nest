'use strict';

var LEGU = LEGU || {};

LEGU.VIDEOHEADING = function () {
  return {
    init: function init(element, data) {
      var view = this;
      view.el = $(element);
      this.variables();
      this.events();
      this.headingHeight();
      this.injectVideo();
    },

    variables: function variables() {
      var view = this;
      view.nextBtn = view.el.next();
      view.Window = $(window), view.WindowHeight = view.Window.height();
      view.Menu = view.el.prev(), view.MenuHeight = view.Menu.height();
      view.heading = view.el, view.Body = $('body, html'), view.VideoContainer = view.el.find('.mediaContainer video'), view.Video = view.el.find('.mediaContainer video').get(0), view.VideoSource = $(view.Video).data('src');
    },

    events: function events() {
      var view = this;

      view.Window.resize(function () {
        view.variables();
        view.headingHeight();
        view.injectVideo();
      });
    },

    headingHeight: function headingHeight() {
      var view = this;
      view.heading.height(view.WindowHeight);
    },

    injectVideo: function injectVideo() {
      var view = this;
      if (view.VideoContainer.length) {
        $(window).width() > 768 ? view.Video.src = view.VideoSource : view.Video.src = '';
      }
    }
  };
};