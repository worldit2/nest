'use strict';

var LEGU = LEGU || {};

LEGU.FOOTERNAV = function () {

    return {
        init: function init(element, data) {
            var view = this;
            view.el = $(element);
            view.variables();
            //view.countingElements();
            view.verticalCenter();
            view.windowResize();
        },

        variables: function variables() {
            var view = this;
            view.footerLists = view.el.find('ul'), view.article = view.el.find('article'), view.logoImg = view.el.find('.logo');
        },

        verticalCenter: function verticalCenter() {
            var view = this;
            setTimeout(function () {
                view.footerLists.each(function (index, item) {

                    if (window.innerWidth >= 768) {
                        $(item).css({
                            'top': (view.article.height() - $(item).outerHeight(true)) / 2 + 'px'
                        });
                    } else {
                        $(item).css({
                            'top': 'inherit'
                        });
                    }
                });
            }, 100);

            view.logoImg.each(function (index, item) {

                if (window.innerWidth >= 768) {
                    $(item).css({
                        'top': (view.article.height() - $(item).height()) / 2 + 'px'
                    });
                } else {
                    $(item).css({
                        'top': 'inherit'
                    });
                }
            });
        },

        countingElements: function countingElements() {
            var view = this;
            view.articleWidth = view.el.find('article').width(), view.logoImg = view.el.find('.logo');

            if (view.article.children('ul').length == 1) {
                view.article.children().css('width', '100%');
            } else {
                view.footerLists.each(function (index, item) {
                    view.totalWidth = 0;
                    view.items = $(item).children();

                    for (var i = 0; i < view.items.length; i++) {
                        view.totalWidth += Math.round($(view.items[i]).outerWidth(true));
                    };

                    if (window.innerWidth >= 768) {
                        $(item).css({
                            'width': view.totalWidth + 10 + 'px',
                            'max-width': view.logoImg.length ? (view.articleWidth - view.logoImg.outerWidth(true)) / 2 + 'px' : view.totalWidth + 10 + 'px'
                        });
                    } else {
                        $(item).css({
                            'width': '100%',
                            'max-width': '100%'
                        });
                    }
                });
            }
        },

        windowResize: function windowResize() {
            var view = this;
            $(window).resize(function () {
                //view.countingElements();
                view.verticalCenter();
            });
        }
    };
};