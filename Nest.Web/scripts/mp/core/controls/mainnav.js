'use strict';

var LEGU = LEGU || {};

LEGU.MAINNAV = function () {

  return {
    init: function init(element, data) {
      var view = this;
      view.el = $(element);
      view.variables();
      view.events();
      //view.navWidth();
      //view.headingHeight();
      view.hideMenu();
      view.windowResize();
    },

    variables: function variables() {
      var view = this;
      view.menuMobile = view.el.find('.menuMobile'), view.navDesktop = view.el.find('.navDesktop'), view.imgLogo = view.el.find('.logo'), view.wrapContainer = view.el.find('.wrapper'), view.list = view.el.find('.menuItems'), view.navMobile = view.el.find('.navMobile');
      view.heading = view.el.next();
      view.highlightSlider = view.el.next('.highlightSlider');
      view.headingWrapper = $(view.heading).find('.wrapper'), view.lastScroll = 0;
      view.viewOffset = view.el.offset().top;
      view.viewHeight = view.el.outerHeight(true);
      view.Position = view.viewOffset + view.viewHeight;
    },
    events: function events() {
      var view = this;
      view.menuMobile.on('click', view.toggleMenu.bind(view));
      $(window).on("scroll", view.toggleSticky.bind(view));
    },
    hideMenu: function hideMenu() {
      var view = this;
      if (window.innerWidth > 1023) {
        view.navMobile.removeClass('open');
        view.menuMobile.removeClass('active');
      }
    },
    menuHeight: function menuHeight() {
      var view = this;

      if (view.el.data('dropmenu') == false && view.navMobile.outerHeight(true) < window.innerHeight && window.innerWidth < 1023) {
        view.navMobile.css('height', $(window).height() + 60 + 'px');
      } else {
        view.navMobile.css('height', '');
      }
    },
    headingHeight: function headingHeight() {
      var view = this;
      $(view.heading).data('fullscreen') == true ? $(view.heading).css('height', view.el.data('theme') == true ? window.innerHeight : window.innerHeight - view.el.outerHeight(true) + 'px') : $(view.heading).css('height', view.el.data('theme') == true ? $(view.heading).outerHeight(true) + view.el.outerHeight(true) : '' + 'px');
    },
    navWidth: function navWidth() {
      var view = this;

      if (!view.el[0].dataset.onlylogo) {
        view.navDesktop[0].style.width = view.imgLogo.length ? Math.floor($('.wrapper').width() - view.imgLogo.outerWidth(true)) + 'px' : $('.wrapper').width() + 'px';
        view.navDesktop[0].dataset.align == 'center' ? view.navDesktop[0].style.left = view.imgLogo.length ? 'calc(50% - ' + Math.floor(view.imgLogo.outerWidth(true)) + 'px)' : '50%' : '';
      }
    },
    toggleMenu: function toggleMenu() {
      var view = this;

      view.navMobile.toggleClass('open');
      view.menuMobile.toggleClass('active');

      if (view.el.data('dropmenu') == true) {
        view.el.addClass('open');
        view.navMobile.slideDown();
        view.navMobile.children().addClass('open');

        if (!view.navMobile.hasClass('open')) {
          view.navMobile.children().removeClass('open');
          view.navMobile.delay(50).slideUp();
          setTimeout(function () {
            view.el.removeClass('open');
          }, 400);
        }
      }

      if (view.el.data('theme') == true && view.el.data('dropmenu') == false) view.el.toggleClass('fixed');
    },
    closeMenuMobile: function closeMenuMobile() {
      var view = this;
      view.navMobile.slideUp();
      view.navMobile.children().removeClass('open');
      view.menuMobile.removeClass('active');
      view.navMobile.removeClass('open');
      setTimeout(function () {
        view.el.removeClass('open');
      }, 400);
    },
    toggleSticky: function toggleSticky() {
      var view = this,
          $window = $(window),
          st = $window.scrollTop(),
          element = view.el,
          elNext = element.next();

      if (element.data('fixed') == true) {
        if (st < view.viewHeight) {
          element.removeClass("stickyFixed");
        } else {
          element.addClass("stickyFixed");
          element.css({
            'box-shadow': element.data('theme') == true ? 'none' : '0px 40px 40px 0 rgba(35, 44, 53, 0.1)',
            transform: 'translateY(0px)'
          });
        }
      } else {
        element.removeClass("stickyFixed");
      }

      if (st > view.viewHeight && !element.hasClass("stickyFixed")) {
        element.addClass('sticky');
        element.css({
          'box-shadow': 'none',
          transform: 'translateY(-80px)'
        });
      } else {
        element.css({
          'box-shadow': element.data('theme') == true ? 'none' : '0px 40px 40px 0 rgba(35, 44, 53, 0.1)',
          transform: 'translateY(0px)'
        });
        if (st <= view.viewHeight) element.removeClass('sticky');
      }

      if (st > view.lastScroll) {
        if (element.data('dropmenu') == true) {
          view.closeMenuMobile();
        }

        if (view.navMobile.hasClass('open')) {
          view.el.css("transform", 'translateY(0)');
        }
      } else {
        element.css({
          'box-shadow': element.data('theme') == true ? 'none' : '0px 40px 40px 0 rgba(35, 44, 53, 0.1)',
          transform: 'translateY(0px)'
        });
        if (element.data('dropmenu') == true) {
          view.closeMenuMobile();
        }
      }
      view.lastScroll = st;
    },
    windowResize: function windowResize() {
      var view = this;
      $(window).resize(function () {
        view.menuHeight();
        view.hideMenu();
        view.navWidth();
      });
    }
  };
};