var LEGU = LEGU || {};

LEGU.MAIN = (function() {

    return {
        init: function() {
            LEGU["UTILS"].init(document); 
            $('[data-control]').each(function (index, elem) {
                var data = $(elem).data(),
                    control = data.control;

                if(!LEGU[control]) return;

                if(typeof LEGU[control] === 'function') {
                    var obj = new LEGU[control]; obj.init(elem, data);
                } else if(typeof LEGU[control] === 'object') {
                    LEGU[control].init(elem, data);
                }
            });
        }
    }
})();

$(document).ready(function () {
    LEGU.MAIN.init();
});
