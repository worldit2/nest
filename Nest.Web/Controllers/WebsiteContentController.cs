﻿using Nest.Web.Core.Domain;
using Nest.Web.Core.Managers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace Nest.Web.Controllers
{
    public class WebsiteContentController : UmbracoApiController
    {
        #region Call website information by route
        //[HttpPost]
        //public HttpResponseMessage GetWebsiteDataByRoute(PageInfo pageInfo)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, new ContentManager().GetElementByRoute(pageInfo.RoutePath));
        //}

        //public class PageInfo
        //{
        //    public string RoutePath { get; set; }
        //}
        #endregion

        [HttpGet]
        public HttpResponseMessage GetLanguages()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new ContentManager().GetWebsiteLanguages());
        }

        [HttpGet]
        public HttpResponseMessage GetWebsiteData(string isoLangCode = null)
        {
            return Request.CreateResponse(HttpStatusCode.OK, ((WebsiteDataFullResult)new ContentManager().GetWebsiteAllData(isoLangCode)).WebsiteData);
        }
    }
}
