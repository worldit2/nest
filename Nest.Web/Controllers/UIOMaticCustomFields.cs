﻿using Nest.Web.Core.Domain;
using Nest.Web.Core.Managers;
using System;
using System.Web.Http;
using Umbraco.Web.Editors;
using static Nest.Web.Core.Managers.UiomaticExport;

namespace Nest.Web.Controllers
{
    public class UIOMaticCustomActionsController : UmbracoAuthorizedJsonController
    {
        [HttpGet]
        public BaseServiceResult ValidateStartup(Guid Id)
        {
            var result = new StartupManager().SendStartupToUmbracoRepository(Id);
            return result;
        }

        [HttpGet]
        public ExportResult Export(string typeAlias)
        {
            return new UiomaticExport().CreateExcelDocBytes("Startup");
        }
    }
}