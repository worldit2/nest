﻿using Nest.Web.Core.Domain;
using Nest.Web.Core.Managers;
using System.Net;
using System.Net.Http;
using System.Web;
using Umbraco.Web.WebApi;

namespace Nest.Web.Controllers
{
    public class EventsController : UmbracoApiController
    {
        public HttpResponseMessage GetICSFile(int nodeId)
        {
            var result = new EventToCalenderManager().GenFile(nodeId);

            if (result.Success)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ContentType = "text/calendar";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"event.ics\"");
                HttpContext.Current.Response.Write(((EventICSFileResult)result).ICSXML);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }

            return Request.CreateResponse((HttpStatusCode)result.InternalStatusCode, result);
        }
    }
}