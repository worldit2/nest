﻿using LeGU.Web.MP.Core.Helpers;
using Nest.Web.Core.Domain;
using Nest.Web.Core.Managers;
using Nest.Web.Core.Objects.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace Nest.Web.Controllers
{
    public class StartupsController : UmbracoApiController
    {
        private readonly StartupManager _startupManager;
        public StartupsController()
        {
            _startupManager = new StartupManager();
        }

        [HttpPost]
        public HttpResponseMessage SubmitStartup()
        {
            BaseServiceResult result;
            try
            {
                List<string> otherCategories;
                string other;
                StartupInfoModel startupInfo = new StartupInfoModel()
                {
                    StartupName = HttpContext.Current.Request.Params["StartupName"],
                    WebsiteName = HttpContext.Current.Request.Params["WebsiteName"],
                    Email = HttpContext.Current.Request.Params["Email"],
                    ContactNumber = HttpContext.Current.Request.Params["ContactNumber"],
                    BusinessType = HttpContext.Current.Request.Params["BusinessType"],
                    Categories = HttpContext.Current.Request.Params["Categories"] ?? string.Empty,
                    StartupDescription = HttpContext.Current.Request.Params["StartupDescription"] ?? string.Empty,
                    IsStartupUpdate = HttpContext.Current.Request.Params["IsStartupUpdate"] != null ? bool.Parse(HttpContext.Current.Request.Params["IsStartupUpdate"]) : false,
                    ExistingStartupName = HttpContext.Current.Request.Params["ExistingStartupName"] ?? string.Empty,
                    Authorize = HttpContext.Current.Request.Params["Authorize"] != null ? bool.Parse(HttpContext.Current.Request.Params["Authorize"]) : false,
                    IsoCode = HttpContext.Current.Request.Params["IsoCode"] ?? "en-US"
                };
                other = HttpContext.Current.Request.Params["Other"] ?? string.Empty;
                if (!string.IsNullOrEmpty(other))
                {
                    otherCategories = new List<string>();
                    foreach (string categorie in other.Split(','))
                    {
                        if (!otherCategories.Contains(categorie))
                        {
                            otherCategories.Add(categorie);
                        }
                    }
                    if (otherCategories.Any())
                    {
                        startupInfo.OtherCategories = otherCategories;
                    }
                }
                //Save info
                result = _startupManager.RegisterStartup(startupInfo);
                if (result.Success)
                {
                    //Save file to filesystem
                    HttpFileCollection fileCollection = HttpContext.Current.Request.Files;
                    if (fileCollection.Count > 0)
                    {
                        HttpPostedFile file = fileCollection.Get("Image");
                        if (file != null)
                        {
                            string path = Path.GetFileName(string.Format("{0}.{1}", result.Message, file.FileName.Split('.').Last()));
                            string fullPath = Path.Combine(HttpContext.Current.Server.MapPath("~/StartupImages"), path ?? throw new InvalidOperationException());
                            file.SaveAs(fullPath);
                            _startupManager.RegisterStartupFile(Guid.Parse(result.Message), fullPath);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new BaseServiceResult { Success = false, Message = string.Format("Error submitting startup :: {0}", result.Message) });
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(Exception), "Error submitting startup", e);
                return Request.CreateResponse(HttpStatusCode.OK, new BaseServiceResult { Success = false, Message = "Error submitting startup." });

            }
            return Request.CreateResponse(HttpStatusCode.OK, new BaseServiceResult { Success = true, Message = "Startup info received and proceded with success." });
        }

        [HttpGet]
        public HttpResponseMessage StartupSearch(string lang)
        {
            var result = new StartupManager().GetStartups(lang);

            return Request.CreateResponse((HttpStatusCode)result.InternalStatusCode, result);
        }
    }
}